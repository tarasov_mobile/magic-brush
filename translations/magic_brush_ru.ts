<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AdvancedColorPicker</name>
    <message>
        <location filename="../qml/Popups/AdvancedColorPicker.qml" line="94"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../qml/Popups/AdvancedColorPicker.qml" line="103"/>
        <source>OK</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>BrushPreviewPage</name>
    <message>
        <source>Try %1%2</source>
        <translation type="vanished">Попробуйте кисть %1%2</translation>
    </message>
    <message>
        <source>Try %1%2:</source>
        <translation type="vanished">Попробуйте кисть %1%2:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/BrushPreviewPage.qml" line="72"/>
        <source>Check out %1%2:</source>
        <translation>Добавьте кисть %1%2:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/BrushPreviewPage.qml" line="96"/>
        <source>Get Premium to unlock additional brushes and remove watermark</source>
        <translation>Приобретите Premium, чтобы открыть все кисти и убрать водяной знак</translation>
    </message>
    <message>
        <location filename="../qml/Pages/BrushPreviewPage.qml" line="113"/>
        <source>Get Premium</source>
        <translation>Получить Premium</translation>
    </message>
    <message>
        <location filename="../qml/Pages/BrushPreviewPage.qml" line="120"/>
        <source>Something wrong with Google Play. Check if it is working.</source>
        <translation>Что-то не так с Google Play. Проверьте, работает ли он.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/BrushPreviewPage.qml" line="121"/>
        <source>App Store is not available right now. Try again later.</source>
        <translation>App Store сейчас недоступен. Повторите попытку позже.</translation>
    </message>
</context>
<context>
    <name>CanvasPage</name>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="22"/>
        <source>Discard all changes?</source>
        <translation>Удалить все изменения?</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="27"/>
        <source>Discard</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="162"/>
        <source>Clear the canvas?</source>
        <translation>Очистить область рисования?</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="168"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="338"/>
        <source>Mirror Ribbon</source>
        <translation>Зеркало Лента</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="344"/>
        <source>Mirror Web</source>
        <translation>Зеркало Паутина</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="340"/>
        <source>Symmetry Ribbon</source>
        <translation>Симметрия Лента</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="346"/>
        <source>Symmetry Web</source>
        <translation>Симметрия Паутина</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="348"/>
        <source>Symmetry Shaded</source>
        <translation>Симметрия Тень</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="342"/>
        <source>Symmetry Fur</source>
        <translation>Симметрия Мех</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="328"/>
        <source>Ribbon</source>
        <translation>Лента</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="334"/>
        <source>Fur</source>
        <translation>Мех</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="336"/>
        <source>Web</source>
        <translation>Паутина</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="332"/>
        <source>Shaded</source>
        <translation>Тень</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="330"/>
        <source>Simple</source>
        <translation>Линия</translation>
    </message>
    <message>
        <location filename="../qml/Pages/CanvasPage.qml" line="350"/>
        <source>Eraser</source>
        <translation>Ластик</translation>
    </message>
</context>
<context>
    <name>CanvasToolbar</name>
    <message>
        <location filename="../qml/Components/CanvasToolbar.qml" line="80"/>
        <source>Brush</source>
        <translation>Кисть</translation>
    </message>
    <message>
        <location filename="../qml/Components/CanvasToolbar.qml" line="90"/>
        <source>Width</source>
        <translatorcomment>Ширина кисти</translatorcomment>
        <translation>Толщина</translation>
    </message>
    <message>
        <location filename="../qml/Components/CanvasToolbar.qml" line="100"/>
        <source>Tone</source>
        <translation>Тон</translation>
    </message>
    <message>
        <location filename="../qml/Components/CanvasToolbar.qml" line="110"/>
        <source>Color</source>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="../qml/Components/CanvasToolbar.qml" line="124"/>
        <source>Base</source>
        <translation>Фон</translation>
    </message>
</context>
<context>
    <name>DialogManager</name>
    <message>
        <location filename="../qml/DialogManager.qml" line="38"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../qml/DialogManager.qml" line="50"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
</context>
<context>
    <name>HomeActionBar</name>
    <message>
        <source>by &lt;a href=&quot;https://app.appsflyer.com/com.tarasovmobile.gtd?pid=artbreak&amp;c=%1&quot;&gt;&lt;b&gt;Chaos Control&lt;/b&gt;&lt;/a&gt;</source>
        <translation type="vanished">от &lt;a href=&quot;https://app.appsflyer.com/com.tarasovmobile.gtd?pid=artbreak&amp;c=%1&quot;&gt;&lt;b&gt;Хаос-контроль&lt;/b&gt;&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Components/HomeActionBar.qml" line="77"/>
        <source>by &lt;a href=&quot;%1&quot;&gt;&lt;b&gt;Chaos Control&lt;/b&gt;&lt;/a&gt;</source>
        <translation>от &lt;a href=&quot;%1&quot;&gt;&lt;b&gt;Хаос-контроль&lt;/b&gt;&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../qml/Pages/HomePage.qml" line="76"/>
        <source>%1 selected</source>
        <translation>%1 выбрано</translation>
    </message>
    <message>
        <location filename="../qml/Pages/HomePage.qml" line="76"/>
        <source>Art Break</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Pages/HomePage.qml" line="110"/>
        <source>Remove selected pictures?</source>
        <translation>Удалить выбранные рисунки?</translation>
    </message>
    <message>
        <location filename="../qml/Pages/HomePage.qml" line="122"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../qml/Pages/HomePage.qml" line="141"/>
        <source>It seems like your image %1library is empty %1&lt;img width=&quot;%2&quot; height=&quot;%2&quot; src=&quot;image://emoji/open_mouth_face.svg&quot;&gt;</source>
        <translation>Похоже, ваша галерея %1пуста %1&lt;img width=&quot;%2&quot; height=&quot;%2&quot; src=&quot;image://emoji/open_mouth_face.svg&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/HomePage.qml" line="171"/>
        <source>Press &apos;+’ to create some %1magic art %2&lt;img width=&quot;%3&quot; height=&quot;%3&quot; src=&quot;image://emoji/art.svg&quot;&gt;</source>
        <translation>Нажмите на +,%1чтобы начать рисовать %2&lt;img width=&quot;%3&quot; height=&quot;%3&quot; src=&quot;image://emoji/art.svg&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/HomePage.qml" line="62"/>
        <location filename="../qml/Pages/HomePage.qml" line="381"/>
        <source>Something wrong with Google Play. Check if it is working.</source>
        <translation>Что-то не так с Google Play. Проверьте, работает ли он.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/HomePage.qml" line="63"/>
        <location filename="../qml/Pages/HomePage.qml" line="382"/>
        <source>App Store is not available right now. Try again later.</source>
        <translation>App Store сейчас недоступен. Повторите попытку позже.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/HomePage.qml" line="413"/>
        <source>Free drawings available:</source>
        <translation>Доступно бесплатных рисунков:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/HomePage.qml" line="463"/>
        <source>Get Premium for unlimited drawings</source>
        <translation>Получите Premium для безлимитного рисования</translation>
    </message>
    <message>
        <location filename="../qml/Pages/HomePage.qml" line="480"/>
        <source>Get Premium</source>
        <translation>Получить Premium</translation>
    </message>
    <message>
        <source>It seems like your image %1library is empty</source>
        <translation type="vanished">Похоже, ваша галерея %1пуста</translation>
    </message>
    <message>
        <source>Press &apos;+’ to create some %1magic art</source>
        <translation type="vanished">Нажмите на +,%1чтобы начать рисовать</translation>
    </message>
    <message>
        <source>It seems like your image %1library is empty %1&lt;img width=&quot;25&quot; height=&quot;25&quot; src=&quot;qrc:/icons/emojis/open_mouth_face.svg&quot;&gt;</source>
        <translation type="vanished">Похоже, ваша галерея %1пуста %1&lt;img width=&quot;25&quot; height=&quot;25&quot; src=&quot;qrc:/icons/emojis/open_mouth_face.svg&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/HomePage.qml" line="158"/>
        <source>Let’s create a new drawing!</source>
        <translation>Пополните галерею новыми рисунками!</translation>
    </message>
    <message>
        <source>Press &apos;+’ to create some %1magic art %2&lt;img width=&quot;25&quot; height=&quot;25&quot; src=&quot;qrc:/icons/emojis/art.svg&quot;&gt;</source>
        <translation type="vanished">Нажмите на +,%1чтобы начать рисовать %2&lt;img width=&quot;25&quot; height=&quot;25&quot; src=&quot;qrc:/icons/emojis/art.svg&quot;&gt;</translation>
    </message>
</context>
<context>
    <name>InfoPage</name>
    <message>
        <source>How to draw</source>
        <translation type="vanished">Как рисовать</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="37"/>
        <source>Info And Feedback</source>
        <translation>Инфо и контакты</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="54"/>
        <source>Art Break Webpage</source>
        <translation>Сайт Art Break</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="59"/>
        <source>Art Break Instagram</source>
        <translation>Art Break в Instagram</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="64"/>
        <source>Show Welcome Screens</source>
        <translation>Показать приветствие</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="69"/>
        <source>Review The App</source>
        <translation>Написать ревью</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="75"/>
        <source>Send Feedback</source>
        <translation>Связаться с нами</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="83"/>
        <source>Get Premium</source>
        <translation>Получить Premium</translation>
    </message>
    <message>
        <source>Remove Watermark</source>
        <translation type="vanished">Убрать знак Art Break</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="88"/>
        <source>Something wrong with Google Play. Check if it is working.</source>
        <translation>Что-то не так с Google Play. Проверьте, работает ли он.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="89"/>
        <source>App Store is not available right now. Try again later.</source>
        <translation>App Store сейчас недоступен. Повторите попытку позже.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="97"/>
        <source>Terms Of Use</source>
        <translation>Условия использования</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="102"/>
        <source>Privacy Policy</source>
        <translation>Политика конфиденциальности</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="245"/>
        <source>ART BREAK&lt;br&gt;by Chaos Control</source>
        <translation>ART BREAK&lt;br&gt;от Chaos Control</translation>
    </message>
    <message>
        <location filename="../qml/Pages/InfoPage.qml" line="253"/>
        <source>Version %1</source>
        <translation>Версия %1</translation>
    </message>
</context>
<context>
    <name>ManualPage</name>
    <message>
        <source>Sketching with Art Break is a great way&lt;br&gt; to reduce stress, improve focus&lt;br&gt;and boost your productivity during a&lt;br&gt;busy day. It’s like meditation but more&lt;br&gt;active and fun&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;+&lt;img src=&apos;image://emoji/phone.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;=&lt;img src=&apos;image://emoji/smile.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;</source>
        <translation type="vanished">5-10 минут рисования в приложении&lt;br&gt; Art Break помогают&lt;br&gt;восстановить силы и повысить&lt;br&gt;продуктивность в течение дня. Это&lt;br&gt;как медитация, только веселее&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;+&lt;img src=&apos;image://emoji/phone.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;=&lt;img src=&apos;image://emoji/smile.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;</translation>
    </message>
    <message>
        <source>You can create a stunning art&lt;br&gt;within seconds.&lt;br&gt;And what is a better therapy&lt;br&gt;than making something&lt;br&gt;beautiful&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/flexed bicep.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/fireworks.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;</source>
        <translation type="vanished">Инструменты приложения&lt;br&gt;позволяют в считанные секунды&lt;br&gt;создать небольшой шедевр и&lt;br&gt;получить заряд бодрости на весь&lt;br&gt;день&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/flexed bicep.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/fireworks.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;</translation>
    </message>
    <message>
        <source>Share your work with friends and fellow&lt;br&gt;artists using &lt;a href=&quot;https://www.instagram.com/explore/tags/artbreak&quot;&gt;#ArtBreak&lt;/a&gt;. And don’t forget&lt;br&gt;to follow our feed for inspiration and&lt;br&gt;productivity tips: &lt;a href=&quot;https://www.instagram.com/chaos.control.app&quot;&gt;@chaos.control.app&lt;/a&gt;&lt;img src=&apos;image://emoji/hands.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;</source>
        <translation type="vanished">Делитесь своими работами&lt;br&gt;с друзьями, используя хештег&lt;br&gt;&lt;a href=&quot;https://www.instagram.com/explore/tags/artbreak&quot;&gt;#ArtBreak&lt;/a&gt;. И присоединяйтесь к нам&lt;br&gt;в  Instagram: &lt;a href=&quot;https://www.instagram.com/chaos.control.app&quot;&gt;@chaos.control.app&lt;/a&gt;&lt;img src=&apos;image://emoji/hands.svg&apos; width=&apos;22&apos; height=&apos;22&apos;&gt;</translation>
    </message>
    <message>
        <source>Sketching with Art Break is a great way&lt;br&gt; to reduce stress, improve focus&lt;br&gt;and boost your productivity during a&lt;br&gt;busy day. It’s like meditation but more&lt;br&gt;active and fun&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;+&lt;img src=&apos;image://emoji/phone.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;=&lt;img src=&apos;image://emoji/smile.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</source>
        <translation type="vanished">5-10 минут рисования в приложении&lt;br&gt; Art Break помогают&lt;br&gt;восстановить силы и повысить&lt;br&gt;продуктивность в течение дня. Это&lt;br&gt;как медитация, только веселее&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;+&lt;img src=&apos;image://emoji/phone.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;=&lt;img src=&apos;image://emoji/smile.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</translation>
    </message>
    <message>
        <source>You can create a stunning art&lt;br&gt;within seconds.&lt;br&gt;And what is a better therapy&lt;br&gt;than making something&lt;br&gt;beautiful&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/flexed bicep.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/fireworks.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</source>
        <translation type="vanished">Инструменты приложения&lt;br&gt;позволяют в считанные секунды&lt;br&gt;создать небольшой шедевр и&lt;br&gt;получить заряд бодрости на весь&lt;br&gt;день&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/flexed bicep.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/fireworks.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</translation>
    </message>
    <message>
        <source>Share your work with friends and fellow&lt;br&gt;artists using &lt;a href=&quot;https://www.instagram.com/explore/tags/artbreakapp/&quot;&gt;#ArtBreakApp&lt;/a&gt;. And don’t&lt;br&gt;forget to follow our feed for inspiration&lt;br&gt;and productivity tips: &lt;a href=&quot;https://www.instagram.com/_u/art.break.app&quot;&gt;@art.break.app&lt;/a&gt;&lt;img src=&apos;image://emoji/hands.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</source>
        <translation type="vanished">Делитесь своими работами&lt;br&gt;с друзьями, используя хештег&lt;br&gt;&lt;a href=&quot;https://www.instagram.com/explore/tags/artbreakapp/&quot;&gt;#ArtBreakApp&lt;/a&gt;. И присоединяйтесь&lt;br&gt; к нам в  Instagram: &lt;a href=&quot;https://www.instagram.com/art.break.app&quot;&gt;@art.break.app&lt;/a&gt;&lt;img src=&apos;image://emoji/hands.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</translation>
    </message>
    <message>
        <source>Sketching with Art Break is a great way&lt;br&gt; to reduce stress, improve focus&lt;br&gt;and boost your productivity during a&lt;br&gt;busy day. Get the same benefits as meditation but it&apos;s more active and fun!&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;+&lt;img src=&apos;image://emoji/phone.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;=&lt;img src=&apos;image://emoji/smile.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</source>
        <translation type="vanished">5-10 минут рисования в&lt;br&gt;Art Break помогают&lt;br&gt;восстановить силы и повысить&lt;br&gt;продуктивность в течение дня. Это&lt;br&gt;как медитация, только веселее&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;+&lt;img src=&apos;image://emoji/phone.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;=&lt;img src=&apos;image://emoji/smile.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</translation>
    </message>
    <message>
        <source>You can produce stunning art&lt;br&gt;within seconds &lt;br&gt;and there is no better therapy&lt;br&gt;than creating something&lt;br&gt;beautiful&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/flexed bicep.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/fireworks.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</source>
        <translation type="vanished">Инструменты Art Break&lt;br&gt;позволяют в считанные секунды&lt;br&gt;создать небольшой шедевр и&lt;br&gt;получить заряд бодрости на весь&lt;br&gt;день&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/flexed bicep.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/fireworks.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</translation>
    </message>
    <message>
        <source>Sketching with Art Break is a great way&lt;br&gt; to reduce stress, improve focus&lt;br&gt;and boost your productivity during a&lt;br&gt;busy day. Get the same benefits as&lt;br&gt;meditation but it&apos;s more active and fun!&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;+&lt;img src=&apos;image://emoji/phone.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;=&lt;img src=&apos;image://emoji/smile.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</source>
        <translation type="vanished">5-10 минут рисования в&lt;br&gt;Art Break помогают&lt;br&gt;восстановить силы и повысить&lt;br&gt;продуктивность в течение дня. Это&lt;br&gt;как медитация, только веселее&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;+&lt;img src=&apos;image://emoji/phone.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;=&lt;img src=&apos;image://emoji/smile.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ManualPage.qml" line="35"/>
        <source>Sketching with Art Break is a great way&lt;br&gt; to reduce stress, improve focus&lt;br&gt;and boost your productivity during a&lt;br&gt;busy day. It’s as beneficial as&lt;br&gt;meditation but more fun!&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;+&lt;img src=&apos;image://emoji/phone.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;=&lt;img src=&apos;image://emoji/smile.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</source>
        <translation>5-10 минут рисования в&lt;br&gt;Art Break помогают&lt;br&gt;восстановить силы и повысить&lt;br&gt;продуктивность в течение дня. Это&lt;br&gt;как медитация, только веселее&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;+&lt;img src=&apos;image://emoji/phone.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;=&lt;img src=&apos;image://emoji/smile.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ManualPage.qml" line="40"/>
        <source>You can produce stunning art&lt;br&gt;within seconds&lt;br&gt;and there is no better therapy&lt;br&gt;than creating something&lt;br&gt;beautiful&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/flexed bicep.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/fireworks.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</source>
        <translation>Инструменты Art Break&lt;br&gt;позволяют в считанные секунды&lt;br&gt;создать небольшой шедевр и&lt;br&gt;получить заряд бодрости на весь&lt;br&gt;день&lt;br&gt;&lt;img src=&apos;image://emoji/art.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/flexed bicep.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;-&gt;&lt;img src=&apos;image://emoji/fireworks.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ManualPage.qml" line="44"/>
        <source>Share your work with friends and fellow&lt;br&gt;artists using &lt;a href=&quot;itag:artbreakapp&quot;&gt;#ArtBreakApp&lt;/a&gt;. And don’t&lt;br&gt;forget to follow our feed!&lt;br&gt;&lt;a href=&quot;iuser:art.break.app&quot;&gt;@art.break.app&lt;/a&gt;&lt;img src=&apos;image://emoji/hands.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</source>
        <translation>Делитесь своими работами&lt;br&gt;с друзьями, используя хештег&lt;br&gt;&lt;a href=&quot;itag:artbreakapp&quot;&gt;#ArtBreakApp&lt;/a&gt;. И присоединяйтесь&lt;br&gt; к нам в  Instagram: &lt;a href=&quot;iuser:art.break.app&quot;&gt;@art.break.app&lt;/a&gt;&lt;img src=&apos;image://emoji/hands.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</translation>
    </message>
    <message>
        <source>Share your work with friends and fellow&lt;br&gt;artists using &lt;a href=&quot;https://www.instagram.com/explore/tags/artbreakapp/&quot;&gt;#ArtBreakApp&lt;/a&gt;. And don’t&lt;br&gt;forget to follow our feed!&lt;br&gt;&lt;a href=&quot;https://www.instagram.com/_u/art.break.app&quot;&gt;@art.break.app&lt;/a&gt;&lt;img src=&apos;image://emoji/hands.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</source>
        <translation type="vanished">Делитесь своими работами&lt;br&gt;с друзьями, используя хештег&lt;br&gt;&lt;a href=&quot;https://www.instagram.com/explore/tags/artbreakapp/&quot;&gt;#ArtBreakApp&lt;/a&gt;. И присоединяйтесь&lt;br&gt; к нам в  Instagram: &lt;a href=&quot;https://www.instagram.com/_u/art.break.app&quot;&gt;@art.break.app&lt;/a&gt;&lt;img src=&apos;image://emoji/hands.svg&apos; width=&apos;%1&apos; height=&apos;%1&apos;&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ManualPage.qml" line="189"/>
        <source>Show at launch</source>
        <translation>Показывать на старте</translation>
    </message>
</context>
<context>
    <name>ManualToolbar</name>
    <message>
        <location filename="../qml/Components/ManualToolbar.qml" line="88"/>
        <source>Let&apos;s start!</source>
        <translation>Начать рисовать</translation>
    </message>
</context>
<context>
    <name>PurchasePopup</name>
    <message>
        <source>Remove Watermark</source>
        <translation type="vanished">Убрать знак Art Break</translation>
    </message>
    <message>
        <source>Get extra brushes and remove watermark</source>
        <translation type="vanished">Разблокировать все кисти и убрать водяной знак</translation>
    </message>
    <message>
        <location filename="../qml/Popups/PurchasePopup.qml" line="44"/>
        <source>Remove all limitations</source>
        <translation>Убрать все ограничения</translation>
    </message>
    <message>
        <location filename="../qml/Popups/PurchasePopup.qml" line="70"/>
        <source>For 1 month</source>
        <translation>На 1 месяц</translation>
    </message>
    <message>
        <location filename="../qml/Popups/PurchasePopup.qml" line="85"/>
        <source>For 1 year</source>
        <translation>На 1 год</translation>
    </message>
    <message>
        <location filename="../qml/Popups/PurchasePopup.qml" line="100"/>
        <source>Forever</source>
        <translation>Навсегда</translation>
    </message>
    <message>
        <location filename="../qml/Popups/PurchasePopup.qml" line="125"/>
        <source>“Forever” option is a one time payment</source>
        <translation>Стоимость “вечной” подписки снимается единоразово</translation>
    </message>
    <message>
        <source>Restore subscription</source>
        <translation type="vanished">Восстановить подписку</translation>
    </message>
    <message>
        <source>The 1-month and 1-year subscriptions renew automatically until you turn it off. “Forever” option is a one time payment</source>
        <translation type="vanished">Подписка на 1 месяц и 1 год обновляется автоматически пока не будет отключена. Стоимость “вечной” подписки снимается единоразово</translation>
    </message>
    <message>
        <location filename="../qml/Popups/PurchasePopup.qml" line="116"/>
        <source>Restore purchase</source>
        <translation>Восстановить покупку</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="vanished">Восстановить</translation>
    </message>
    <message>
        <source>Get Premium</source>
        <translation type="vanished">Получить Premium</translation>
    </message>
    <message>
        <source>This is a one time purchase. You will have Premium forever and without any extra charges.</source>
        <translation type="vanished">Это единоразовый платеж. Вы сможете использовать все текущие и будущие Premium-функции без необходимости дополнительной оплаты.</translation>
    </message>
    <message>
        <location filename="../qml/Popups/PurchasePopup.qml" line="192"/>
        <source>Check your internet connection</source>
        <translation>Проверьте интернет-соединение</translation>
    </message>
    <message>
        <location filename="../qml/Popups/PurchasePopup.qml" line="209"/>
        <source>Try again</source>
        <translation>Повторить</translation>
    </message>
    <message>
        <location filename="../qml/Popups/PurchasePopup.qml" line="235"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/managers/StorageManager.cpp" line="87"/>
        <source>Unable to create a subfolder within: %1</source>
        <translation>Невозможно создать подпапку в %1</translation>
    </message>
    <message>
        <location filename="../src/managers/StorageManager.cpp" line="92"/>
        <source>Unable to open the %1 folder</source>
        <translation>Невозможно открыть папку %1</translation>
    </message>
    <message>
        <location filename="../src/managers/StorageManager.cpp" line="142"/>
        <location filename="../src/managers/StorageManager.cpp" line="152"/>
        <source>Unable to save a picture: %1</source>
        <translation>Невозможно сохранить изображение %1</translation>
    </message>
    <message>
        <source>Unable to save image</source>
        <translation type="vanished">Невозможно сохранить изображение</translation>
    </message>
    <message>
        <source>Your picture saved into Gallery</source>
        <translation type="vanished">Ваш рисунок успешно сохранен в Галерею</translation>
    </message>
    <message>
        <source>Unable to create an album</source>
        <translation type="vanished">Невозможно создать новый альбом</translation>
    </message>
    <message>
        <source>You have no purchases</source>
        <translation type="vanished">У вас нет покупок</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Ошибка</translation>
    </message>
    <message>
        <source>You have denied access to your photo library. Check the privacy settings on your device.</source>
        <translation type="vanished">Вы запретили доступ к Фото. Проверьте настройки конфиденциальности на вашем устройстве.</translation>
    </message>
    <message>
        <source>Facebook app not found.</source>
        <translation type="vanished">Приложение Facebook не найдено.</translation>
    </message>
    <message>
        <source>Twitter app not found.</source>
        <translation type="vanished">Приложение Twitter не найдено.</translation>
    </message>
    <message>
        <source>Instagram app not found.</source>
        <translation type="vanished">Приложение Instagram не найдено.</translation>
    </message>
</context>
<context>
    <name>RatePopup</name>
    <message>
        <location filename="../qml/Popups/RatePopup.qml" line="67"/>
        <source>Good job!</source>
        <translation>Отличная работа!</translation>
    </message>
    <message>
        <source>We hope you&apos;re enjoying Art Break! Could you please review the app on the %1?</source>
        <translation type="vanished">Надеемся, вам нравится Art Break! Не могли бы вы поставить приложению оценку в %1?</translation>
    </message>
    <message>
        <source>Write review</source>
        <translation type="vanished">Написать ревью</translation>
    </message>
    <message>
        <location filename="../qml/Popups/RatePopup.qml" line="90"/>
        <source>We hope you’re enjoying Art Break! Could you please rate the app on the %1?</source>
        <translation>Надеемся, вам нравится Art Break! Не могли бы вы поставить приложению оценку в %1?</translation>
    </message>
    <message>
        <location filename="../qml/Popups/RatePopup.qml" line="107"/>
        <source>Rate the app</source>
        <translation>Поставить оценку</translation>
    </message>
    <message>
        <location filename="../qml/Popups/RatePopup.qml" line="130"/>
        <source>Remind me later</source>
        <translation>Напомнить позже</translation>
    </message>
    <message>
        <location filename="../qml/Popups/RatePopup.qml" line="153"/>
        <source>No, thanks</source>
        <translation>Нет, спасибо</translation>
    </message>
</context>
<context>
    <name>SharePage</name>
    <message>
        <location filename="../qml/Pages/SharePage.qml" line="46"/>
        <source>Share picture</source>
        <translation>Поделиться</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SharePage.qml" line="149"/>
        <source>Your picture saved into Gallery</source>
        <translation>Ваш рисунок успешно сохранен в Галерею</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SharePage.qml" line="198"/>
        <source>Remove Watermark</source>
        <translation>Убрать знак Art Break</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SharePage.qml" line="205"/>
        <source>Something wrong with Google Play. Check if it is working.</source>
        <translation>Что-то не так с Google Play. Проверьте, работает ли он.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SharePage.qml" line="206"/>
        <source>App Store is not available right now. Try again later.</source>
        <translation>App Store сейчас недоступен. Повторите попытку позже.</translation>
    </message>
</context>
<context>
    <name>StorageManager</name>
    <message>
        <location filename="../src/managers/StorageManager.cpp" line="240"/>
        <source>Unable to remove %1</source>
        <translation>Невозможно удалить %1</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Please press Back button again to exit</source>
        <translation type="vanished">Нажмите кнопку Назад еще раз, чтобы выйти</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="158"/>
        <source>Press again to exit</source>
        <translation>Нажмите еще раз, чтобы выйти</translation>
    </message>
</context>
</TS>
