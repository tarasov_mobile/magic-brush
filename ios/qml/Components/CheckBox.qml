import QtQuick 2.7
import QtQuick.Controls 2.1

CheckBox {
    id: root

    property int size: 20

    property color color: "#D9D9D9"

    checked: true

    indicator: Rectangle {
        implicitWidth: root.size
        implicitHeight: root.size
        x: root.leftPadding
        y: parent.height / 2 - height / 2
        radius: 7
        color: "transparent"
        border {
            width: 1
            color: root.color
        }

        Image {
            anchors.centerIn: parent
            sourceSize {
                width: parent.width * 0.7
                height: parent.height * 0.7
            }
            width: parent.width * 0.7
            height: parent.height * 0.7
            fillMode: Image.PreserveAspectFit
            visible: root.checked
            source: "qrc:/icons/ic_mark_gray.svg"
        }
    }

    contentItem: Text {
        text: root.text
        font.pixelSize: 14
        opacity: enabled ? 1.0 : 0.3
        color: root.color
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        leftPadding: root.indicator.width + root.spacing
    }
}
