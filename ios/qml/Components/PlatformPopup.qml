import QtQuick 2.7
import com.tarasovmobile.managers 1.0

Item {
    id: root

    default property alias data: dialogRect.data
    property int dialogHeight: StyleManager.styles.purchasePopup.height

    function close() {
        fogAnimation.to = 0.0;
        fogAnimation.start();
        dialogDisappearAnimation.start();
    }

    visible: false
    anchors.fill: parent
    z: 100

    onVisibleChanged: {
        if (visible) {
            fogAnimation.to = 0.4;
            fogAnimation.start();
        }
    }

    Rectangle {
        id: fogRect
        anchors.fill: parent
        color: "black"
        opacity: 0.0

        NumberAnimation {
            id: fogAnimation
            target: fogRect
            property: "opacity"
            duration: 300
            to: 0.4

            onStopped: {
                if (fogRect.opacity === 0.0) {
                    root.visible = false;
                    dialogRect.opacity = 1.0;
                    popupManager.hidePopup();
                }
            }
        }
    }

    MouseArea {
        anchors.fill: parent
    }

    Rectangle {
        id: dialogRect

        x: parent ? (parent.width - width) / 2 : 0
        y: parent ? (parent.height - height) / 2 : 0
        width: StyleManager.styles.purchasePopup.width
        height: root.dialogHeight

        radius: 13
        color: "#f9f9f9"
        scale: root.visible ? 1.0 : 1.1
        Behavior on scale {
            NumberAnimation { duration: 200 }
        }

        NumberAnimation {
            id: dialogDisappearAnimation
            target: dialogRect
            property: "opacity"
            duration: 100
            to: 0.0
        }
    }
}
