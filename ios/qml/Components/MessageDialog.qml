import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
    id: root

    property alias title: title.text
    property alias text: textLabel.text
    property var buttonModel: []

    signal accepted();

    function open() {
        visible = true;
        fogAnimation.to = 0.4;
        fogAnimation.start();
    }

    function hide() {
        fogAnimation.to = 0.0;
        fogAnimation.start();
        dialogDisappearAnimation.start();
    }

    visible: false
    anchors.fill: parent
    z: 100

    QtObject {
        id: internal

        readonly property color fogColor: "black"

        readonly property int dialogWidth: 270
        readonly property int dialogRectRadius: 13
        readonly property color dialogBg: "#f9f9f9"

        readonly property int verticalMargin: 20
        readonly property int contentWidth: dialogWidth * 0.9
        readonly property int contentColumnSpacing: 3

        readonly property int titleFontSize: 17
        readonly property int messageFontSize: 13
        readonly property color textColor: "black"

        readonly property int buttonHeight: 45
        readonly property int buttonFontSize: 17
        readonly property color defaultButtonColor: "#037aff"
        readonly property color destructiveButtonColor: "#ff0000"

        readonly property color separatorColor: "#d7dbdf"
    }

    Rectangle {
        id: fogRect
        anchors.fill: parent
        color: internal.fogColor
        opacity: 0.0

        NumberAnimation {
            id: fogAnimation
            target: fogRect
            property: "opacity"
            duration: 300
            to: 0.4

            onStopped: {
                if (fogRect.opacity === 0.0) {
                    root.visible = false;
                    dialogRect.opacity = 1.0;
                }
            }
        }
    }

    MouseArea {
        anchors.fill: parent
    }

    Rectangle {
        id: dialogRect

        anchors.centerIn: parent

        radius: internal.dialogRectRadius
        color: internal.dialogBg
        scale: root.visible ? 1.0 : 1.1
        Behavior on scale {
            NumberAnimation { duration: 200 }
        }

        implicitWidth: internal.dialogWidth
        implicitHeight: content.height + buttonRow.height

        NumberAnimation {
            id: dialogDisappearAnimation
            target: dialogRect
            property: "opacity"
            duration: 100
            to: 0.0
        }

        Item {
            id: content

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            height: contentColumn.height + internal.verticalMargin * 2

            Column {
                id: contentColumn

                anchors.centerIn: parent

                width: internal.contentWidth
                spacing: internal.contentColumnSpacing

                Label {
                    id: title

                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter

                    font {
                        pixelSize: internal.titleFontSize
                        bold: true
                    }

                    color: internal.textColor
                    wrapMode: Text.Wrap
                }

                Label {
                    id: textLabel

                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter

                    font.pixelSize: internal.messageFontSize

                    color: internal.textColor
                    wrapMode: Text.Wrap
                    visible: text.length
                }
            }
        }

        Rectangle {
            anchors {
                left: parent.left
                right: parent.right
                bottom: buttonRow.top
            }

            color: internal.separatorColor
            height: 1
        }

        Row {
            id: buttonRow

            anchors {
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }
            height: internal.buttonHeight

            Repeater {
                model: root.buttonModel

                Row {
                    height: parent.height

                    Button {
                        id: cancelButton
                        text: modelData.text
                        height: parent.height
                        width: dialogRect.width / root.buttonModel.length
                        style: ButtonStyle {
                            background: Item {
                                clip: true
                                Rectangle {
                                    anchors {
                                        fill: parent
                                        topMargin: -radius
                                        leftMargin: root.buttonModel.length > 1 && index == root.buttonModel.length - 1 ? -radius : 0
                                        rightMargin: root.buttonModel.length > 1 && index == 0 ? -radius : 0
                                    }

                                    color: internal.fogColor
                                    opacity: control.pressed ? 0.1 : 0.0
                                    radius: internal.dialogRectRadius
                                }
                            }
                            label: Label {
                                color: modelData.destructive ? internal.destructiveButtonColor : internal.defaultButtonColor
                                font.pixelSize: internal.buttonFontSize
                                font.bold: modelData.defaultButton ? modelData.defaultButton : false
                                text: control.text
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                            }
                        }

                        onClicked: {
                            root.hide();
                            if (modelData.acceptButton)
                                root.accepted();
                        }
                    }

                    Rectangle {
                        color: internal.separatorColor
                        width: 1
                        height: parent.height
                        visible: index != root.buttonModel.length - 1
                    }
                }
            }
        }
    }
}
