import QtQuick 2.7

Item {
    id: root

    Image {
        readonly property int size: 30

        anchors {
            right: parent.right
            bottom: parent.bottom
            margins: 10
        }

        width: size
        height: size
        sourceSize {
            width: size
            height: size
        }
        fillMode: Image.PreserveAspectFit
        source: "qrc:/icons/list_selector.svg"
    }
}
