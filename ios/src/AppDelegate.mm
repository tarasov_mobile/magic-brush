﻿#import "AppDelegate.h"

#import "UIKit/UIKit.h"
#import <objc/runtime.h>
#import <Social/Social.h>

#import "GAI.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#include <AppsFlyerTracker.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#include <QtCore>

#define VIEW_CONTROLLER [[[UIApplication sharedApplication] keyWindow] rootViewController]

NSUInteger ScreenOrientationLocker::ALLOWED_SCREEN_ORIENTATION = UIInterfaceOrientationMaskAll;

#pragma clang diagnostic ignored "-Wprotocol"
@implementation QIOSApplicationDelegate (AppDelegate)

-(id)init {
    return self;
}

- (NSString *) getCurrentAppVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
}

- (void) sendAnalyticsEventWithCategory:(NSString *)categoryName andAction:(NSString *)actionName
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:categoryName
                                                          action:actionName
                                                          label:nil
                                                          value:nil] build]];
}

-(void)applicationDidBecomeActive:(UIApplication *)application
{
    Q_UNUSED(application)
    // Track Installs, updates & sessions(app opens) (You must include this API to enable tracking)
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];

    [FBSDKAppEvents activateApp];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    //init Google Analytics
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-89808827-2"];
    [tracker set:kGAIAppVersion value:[self getCurrentAppVersion]];
    [tracker setAllowIDFACollection: YES];

    //init AppsFlyer
    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = @"qbjfvMkFezzW9fe5nmZty";
    [AppsFlyerTracker sharedTracker].appleAppID = @"1218293620";

    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url
                                                 sourceApplication:sourceApplication
                                                        annotation:annotation];
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    Q_UNUSED(application);
    Q_UNUSED(window);

    return ScreenOrientationLocker::ALLOWED_SCREEN_ORIENTATION;
}

- (void) forceActivateOrientation:(NSUInteger) orientation
{
    NSNumber *orientationValue = [NSNumber numberWithInt:orientation];
    [[UIDevice currentDevice] setValue:orientationValue forKey:@"orientation"];
}

- (bool) isTablet
{
    UIDevice *device = [UIDevice currentDevice];
    UIUserInterfaceIdiom idiom = device.userInterfaceIdiom;
    return idiom == UIUserInterfaceIdiomPad;
}

- (void) showNotificationMessage:(NSString *)messageText
{
    dispatch_async(dispatch_get_main_queue(), ^{
       UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                      message:messageText
                                                               preferredStyle:UIAlertControllerStyleAlert];

       [VIEW_CONTROLLER presentViewController:alert animated:YES completion:nil];

       int duration = 1; // duration in seconds

       dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
           [alert dismissViewControllerAnimated:YES completion:nil];
       });
    });
}

- (PHAssetCollection *) getAlbumByName:(NSString *)name
{
    PHFetchOptions *albumFetchOption = [[PHFetchOptions alloc]init];
    albumFetchOption.predicate = [NSPredicate predicateWithFormat:@"title == %@",name];
    PHFetchResult *albumResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:albumFetchOption];
    return albumResult.firstObject;
}

- (void) saveImage:(NSString *)imagePath toAlbum:(PHAssetCollection *)assetCollection completion:(void (^)(void))completion
{
    UIImage *image = [UIImage imageNamed:imagePath];

    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];

        PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:assetCollection];
        [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
    } completionHandler:^(BOOL success, NSError *error) {
        if (!success) {
            [self showAlertWithTitle:QObject::tr("Unable to save image").toNSString() withMessage: error.localizedDescription];
        } else {
            [self showNotificationMessage:QObject::tr("Your picture saved into Gallery").toNSString()];
            if (completion)
               completion();
        }
    }];
}

- (void) saveToPhotoLibrary:(NSString *) shareImagePath completion:(void (^)(void))completion
{
    [self requestPhotoAuthorization:^(BOOL granted){
        if (granted == YES) {
            // check if such album already exists
            NSString *albumName = @"ArtBreak";
            PHAssetCollection *assetCollection = [self getAlbumByName:albumName];
            if (!assetCollection) {
                // create a new album
                __block PHObjectPlaceholder *albumPlaceholder;
                [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                    PHAssetCollectionChangeRequest *changeRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:albumName];
                    albumPlaceholder = changeRequest.placeholderForCreatedAssetCollection;
                } completionHandler:^(BOOL success, NSError *error) {
                    if (success) {
                        [self saveImage:shareImagePath toAlbum:[self getAlbumByName:@"ArtBreak"] completion:completion];
                    } else {
                        [self showAlertWithTitle:QObject::tr("Unable to create an album").toNSString() withMessage: error.localizedDescription];
                    }
                }];
            } else {
                [self saveImage:shareImagePath toAlbum:assetCollection completion:completion];
            }
        } else {
            [self showAlertWithTitle:QObject::tr("Error").toNSString()
              withMessage: QObject::tr("You have denied access to your photo library. Check the privacy settings on your device.").toNSString()];
        }
    }];
}

- (void) shareToFacebook:(NSString *) shareImagePath completion:(void (^)(void))completion
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            controller.completionHandler = ^(SLComposeViewControllerResult result) {
                if (completion)
                   completion();
            };
            [controller addImage:[UIImage imageNamed:shareImagePath]];

            [VIEW_CONTROLLER presentViewController:controller animated:YES completion:nil];
        });
    } else {
        [self showAlertWithTitle:QObject::tr("Facebook app not found.").toNSString() withMessage:nil];
    }
}

- (void) shareToTwitter:(NSString *) shareImagePath completion:(void (^)(void))completion
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            controller.completionHandler = ^(SLComposeViewControllerResult result) {
                if (completion)
                    completion();
            };

            [controller addImage:[UIImage imageNamed:shareImagePath]];

            [VIEW_CONTROLLER presentViewController:controller animated:YES completion:nil];
        });
    } else {
        [self showAlertWithTitle:QObject::tr("Twitter app not found.").toNSString() withMessage:nil];
    }
}

- (void) shareToInstagram:(NSString *) shareImagePath
{
    [self requestPhotoAuthorization:^(BOOL granted) {
        if (granted == YES) {
            UIImage *image = [UIImage imageNamed:shareImagePath];

            [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                PHAssetChangeRequest * assetReq = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
                NSString* localId = [[assetReq placeholderForCreatedAsset] localIdentifier];
                NSString *escapedString = [localId stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
                NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@", escapedString]];

                if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
                    [[UIApplication sharedApplication] openURL:instagramURL];
                } else {
                    [self showAlertWithTitle:QObject::tr("Instagram app not found.").toNSString() withMessage:nil];
                }


            } completionHandler:^(BOOL success, NSError *error) {
                if (!success){
                    [self showAlertWithTitle:QObject::tr("Error").toNSString() withMessage:error.localizedDescription];
                }
            }];
        } else {
            [self showAlertWithTitle:QObject::tr("Error").toNSString()
                    withMessage: QObject::tr("You have denied access to your photo library. Check the privacy settings on your device.").toNSString()];
        }
    }];
}

- (void)requestPhotoAuthorization:(void (^)(BOOL granted))granted
{
    __block void (^handler)(PHAuthorizationStatus status);
    handler = ^(PHAuthorizationStatus status)
    {
        if (status == PHAuthorizationStatusAuthorized) granted(YES);
        else if (status == PHAuthorizationStatusNotDetermined) [PHPhotoLibrary requestAuthorization:handler];
        else granted(NO);
    };
    handler([PHPhotoLibrary authorizationStatus]);
}

- (void) showShareDialog:(NSString *)shareImagePath completion:(void (^)(void))completion
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImage *image = [UIImage imageNamed:shareImagePath];
        NSArray *activityItems = @[image];
        UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        activityViewControntroller.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            if (completion)
              completion();
        };
        activityViewControntroller.excludedActivityTypes = @[];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            activityViewControntroller.popoverPresentationController.sourceView = VIEW_CONTROLLER.view;
            activityViewControntroller.popoverPresentationController.sourceRect = CGRectMake(VIEW_CONTROLLER.view.bounds.size.width/2, VIEW_CONTROLLER.view.bounds.size.height/4, 0, 0);
        }
        [VIEW_CONTROLLER presentViewController:activityViewControntroller animated:true completion:nil];
    });
}

- (void) showAlertWithTitle:(NSString *)titleText withMessage:(NSString *)messageText
{
    dispatch_async(dispatch_get_main_queue(), ^{
       UIAlertController * alert = [UIAlertController
       alertControllerWithTitle:titleText
       message:messageText
       preferredStyle:UIAlertControllerStyleAlert];

       UIAlertAction* ok = [UIAlertAction
       actionWithTitle:@"OK"
       style:UIAlertActionStyleDefault
       handler:^(UIAlertAction * action) {
           Q_UNUSED(action);
           [alert dismissViewControllerAnimated:YES completion:nil];
       }];

       [alert addAction:ok];

       [VIEW_CONTROLLER presentViewController:alert animated:YES completion:nil];
    });
}

@end
