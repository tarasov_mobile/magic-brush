#ifndef APPDELEGATE_H
#define APPDELEGATE_H

#import "UIKit/UIKit.h"
#import <Photos/Photos.h>

#include "../../src/managers/PurchaseManager_p.h"

class ScreenOrientationLocker {
public:
    static NSUInteger ALLOWED_SCREEN_ORIENTATION;
};

@interface QIOSApplicationDelegate
@end

@interface QIOSApplicationDelegate(AppDelegate)
- (NSString *) getCurrentAppVersion;

- (void) sendAnalyticsEventWithCategory:(NSString *)categoryName andAction:(NSString *)actionName;

- (void) forceActivateOrientation:(NSUInteger)orientation;
- (bool) isTablet;

- (void) showNotificationMessage:(NSString *)messageText;
- (void) showAlertWithTitle:(NSString *)titleText withMessage:(NSString *)messageText;

- (PHAssetCollection *) getAlbumByName:(NSString *)name;
- (void) saveImage:(NSString *)imagePath toAlbum:(PHAssetCollection *)assetCollection completion:(void (^)(void))completion;
- (void) saveToPhotoLibrary:(NSString *) shareImagePath completion:(void (^)(void))completion;
- (void) shareToFacebook:(NSString *) shareImagePath completion:(void (^)(void))completion;
- (void) shareToInstagram:(NSString *) shareImagePath;
- (void) shareToTwitter:(NSString *) shareImagePath completion:(void (^)(void))completion;
- (void) requestPhotoAuthorization:(void (^)(BOOL granted))granted;
- (void) showShareDialog:(NSString *)shareImagePath completion:(void (^)(void))completion;
@end

#endif // APPDELEGATE_H
