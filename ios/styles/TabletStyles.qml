import QtQuick 2.7
import QtQuick.Window 2.2

QtObject {
    id: root

    readonly property bool isSw800dp: Screen.width >= 800 && Screen.height >= 800
    readonly property double tabletRatio: isSw800dp ? 1.5 : 1.3

    property var manualPage: QtObject {
        readonly property int textBottomMargin: 20 * root.tabletRatio
        readonly property int sideMargin: 30 * root.tabletRatio
        readonly property int lineHeight: 20 * root.tabletRatio
        readonly property int fontSize: 16 * root.tabletRatio
        readonly property int checkBoxSize: 20 * root.tabletRatio
        readonly property int checkBoxBottomMargin: 10 * root.tabletRatio
        readonly property int emojiSize: 22 * root.tabletRatio
    }

    property var manualToolbar: QtObject {
        readonly property int totalSize: 52 * root.tabletRatio
        readonly property int barSize: 43 * root.tabletRatio
        readonly property int buttonFontSize: 20 * root.tabletRatio
        readonly property int pageIndicatorSize: 8 * root.tabletRatio
    }

    property var homePage: QtObject {
        readonly property int fontSize: 16 * root.tabletRatio
        readonly property int emojiSize: 25 * root.tabletRatio

        readonly property int gridFooterHeight: 360
        readonly property int footerTitleFontSize: 20
        readonly property int footerCountFontSize: 40
        readonly property color footerTextColor: "#B1B1B1"

        readonly property int premiumButtonWidth: 210
        readonly property int premiumButtonHeight: 45
        readonly property color premiumButtonBgColor: "#EFEFF4"
        readonly property int premiumButtonFontSize: 18
        readonly property int premiumButtonTopMargin: 35

        readonly property int footerTitleTopMargin: 50
        readonly property int footerCountTopMargin: 10
        readonly property int footerPremiumTitleTopMargin: 70
    }

    property var actionBar: QtObject {
        readonly property int height: 54
        readonly property int iconSize: 30

        readonly property int portraitTitleBarFontSize: 24
        readonly property int landscapeTitleBarFontSize: 22

        readonly property int sideMargin: 14
    }

    property var roundIconButton: QtObject {
        readonly property int buttonSize: 54
        readonly property int iconSize: 24
    }

    property var floatingButton: QtObject {
        readonly property int size: 70
        readonly property int fontSize: 32
        readonly property color bgColor: DefaultAppColor
        readonly property color fontColor: "white"
    }

    property var infoPage: QtObject {
        readonly property int appIconSize: 50
        readonly property int listItemHeight: 57
        readonly property int listIconSize: 43
        readonly property int itemRowSpacing: 15
        readonly property int sideMargin: 15
        readonly property int listBottomMargin: 13
    }

    property var purchasePopup: QtObject {
        readonly property int width: 280 * root.tabletRatio
        readonly property int height: 240 * root.tabletRatio

        readonly property int headerTopMargin: 15 * root.tabletRatio
        readonly property int headerFontSize: 16 * root.tabletRatio

        readonly property int buttonColumnSpacing: 15 * root.tabletRatio
        readonly property int productButtonWidth: 210 * root.tabletRatio
        readonly property int productButtonHeight: 40 * root.tabletRatio

        readonly property int errorLabelFontSize: 14 * root.tabletRatio
        readonly property int tryAgainButtonWidth: 100 * root.tabletRatio
        readonly property int tryAgainButtonHeight: 30 * root.tabletRatio

        readonly property int buttonFontSize: 16

        readonly property int subscriptionsTextFontSize: 10 * root.tabletRatio

        readonly property int buttonRadius: 13
    }

    property var sharePage: QtObject {
        readonly property int removeWatermarkButtonWidth: 250 * root.tabletRatio
        readonly property int removeWatermarkButtonHeight: 40 * root.tabletRatio
        readonly property int removeWatermarkButtonFontSize: 18
    }

    property var brushPreviewPage: QtObject {
        readonly property int headerFontSize: root.isSw800dp ? 32 : 28
        readonly property int descriptionFontSize: root.isSw800dp ? 34 : 30
        readonly property int columnSpacing: Screen.primaryOrientation === Qt.PortraitOrientation ? 50 : (root.isSw800dp ? 40 : 25)
        readonly property double canvasWidthFactor: Screen.primaryOrientation === Qt.PortraitOrientation ? 0.6 : 0.8
        readonly property int premiumButtonWidth: 300
        readonly property int premiumButtonHeight: 60
        readonly property int premiumButtonFontSize: 20
    }

    property var ratePopup: QtObject {
        readonly property int height: 340 * root.tabletRatio

        readonly property int contentSpacing: 20 * root.tabletRatio
        readonly property int titleRowSpacing: 10 * root.tabletRatio

        readonly property int appIconSize: 50 * root.tabletRatio
        readonly property int titleIconSize: 18 * root.tabletRatio
        readonly property int titleFontSize: 20 * root.tabletRatio
        readonly property int descriptionFontSize: 15 * root.tabletRatio
        readonly property int writeReviewButtonWidth: 200 * root.tabletRatio
        readonly property int writeReviewButtonHeight: 40 * root.tabletRatio

        readonly property int buttonRadius: 13
        readonly property int buttonFontSize: 18
        readonly property int cancelFontCapitalization: Font.MixedCase
    }
}
