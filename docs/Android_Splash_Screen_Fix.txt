In order to have splash screen working fluently on Android we have to make some changes in Qt's source code.

1) Get rid of black flicker between splash and QML content (https://bugreports.qt.io/browse/QTBUG-43558)
- Open /home/USER_NAME/Qt5.8.0_Release/5.8/Src/qtbase/src/android/android.pro in Qt Creator
- open QtActivityDelegate.java:965 where you can find creation of QtLayout - "m_layout = new QtLayout(m_activity, startApplication);"
- after that line add - "m_layout.addView(new QtSurface(m_activity, 0, false, 0));"

2) Make splash screen to be cropped on irregular screen sizes
- replace QtActivityDelegate.java:974(or 975 if you added the line from the previous item) with "m_splashScreen.setScaleType(ImageView.ScaleType.CENTER_CROP);"

3) Fade out splash screen (this works only if we use "android.app.splash_screen_sticky" option from our AndroidManifest.xml)

Find the hideSplashScreen() method in QtActivityDelegate.java and replace lines

        m_layout.removeView(m_splashScreen);
        m_splashScreen = null;

with the following:

        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(m_splashScreen, "alpha",  1f, 0.0f);
        fadeOut.setDuration(300);
        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(fadeOut);

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                m_layout.removeView(m_splashScreen);
                m_splashScreen = null;
            }
        });

        animatorSet.start();
