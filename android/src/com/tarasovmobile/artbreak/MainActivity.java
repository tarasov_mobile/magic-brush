package com.tarasovmobile.artbreak;

import android.util.Log;
import android.content.Intent;
import android.net.Uri;
import java.io.File;
import android.content.res.Configuration;
import android.content.pm.ActivityInfo;
import android.app.ActivityManager.TaskDescription;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.content.pm.PackageManager;
import java.util.List;
import java.util.ArrayList;
import android.content.pm.ResolveInfo;
import java.lang.Runnable;
import java.util.Map;
import java.util.HashMap;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.HitBuilders;

import com.appsflyer.AppsFlyerLib;
import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;

import android.hardware.SensorEventListener;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;

import android.view.Surface;

import android.content.pm.PackageManager.NameNotFoundException;

import com.tarasovmobile.artbreak.util.IabHelper;
import com.tarasovmobile.artbreak.util.IabResult;
import com.tarasovmobile.artbreak.util.Inventory;
import com.tarasovmobile.artbreak.util.Purchase;
import com.tarasovmobile.artbreak.util.SkuDetails;
import com.tarasovmobile.artbreak.util.IabHelper.IabAsyncInProgressException;

import android.support.v4.content.FileProvider;

public class MainActivity extends org.qtproject.qt5.android.bindings.QtActivity implements SensorEventListener
{
    private static final String TAG = "ARTBREAK";

    private static final int PURCHASE_REQUEST_CODE = 10001;

    private static final String ONE_MONTH_SKU = "ab1subscription";
    private static final String ONE_YEAR_SKU = "absub1year";
    private static final String FOREVER_SKU = "abfull";

    private static native void updateItemPrice(String itemId, String price);
    private static native void updateHasPurchase(boolean hasPurchase);
    private static native void purchaseCompleted();
    private static native void onDataUpdateError();
    private static native void onBillingStatusChanged(boolean initialized);

    private Tracker mTracker;
    private SensorManager mSensorManager;
    private IabHelper mHelper;
    private Inventory mInventory;
    private IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase)
        {
            if (result.isFailure()) {
                MainActivity.this.sendExceptionEvent("Purchase error: " + result, false);
                return;
            }

            String productSku = purchase.getSku();
            SkuDetails details = mInventory.getSkuDetails(productSku);

            Map<String, Object> eventValue = new HashMap<String, Object>();
            eventValue.put(AFInAppEventParameterName.REVENUE, details.getPrice());
            eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, purchase.getItemType() == "ITEM_TYPE_SUBS" ? "subscription" : "forever");
            eventValue.put(AFInAppEventParameterName.CONTENT_ID, productSku);
            eventValue.put(AFInAppEventParameterName.CURRENCY, details.getPriceCurrencyCode());
            AppsFlyerLib.getInstance().trackEvent(MainActivity.this, AFInAppEventType.PURCHASE, eventValue);

            MainActivity.purchaseCompleted();
        }
    };

    private IabHelper.QueryInventoryFinishedListener mUpdatePurchasesListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory)
        {
            if (result.isFailure()) {
                MainActivity.this.sendExceptionEvent("Unable to get local inventory: " + result, false);
                return;
            }

            // Update Qt's purchase manager
            boolean hasPurchase = inventory.hasPurchase(ONE_MONTH_SKU) || inventory.hasPurchase(ONE_YEAR_SKU) || inventory.hasPurchase(FOREVER_SKU);
            MainActivity.updateHasPurchase(hasPurchase);
        }
    };

    private IabHelper.QueryInventoryFinishedListener mUpdateDataListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory)
        {
            if (result.isFailure()) {
                onDataUpdateError();

                try {
                    mHelper.queryInventoryAsync(mUpdatePurchasesListener);
                } catch (IabAsyncInProgressException e) {
                    Log.e(TAG, "Error querying inventory. Another async operation in progress.");
                }

                return;
            }

            mInventory = inventory;
            // Update Qt's purchase manager
            MainActivity.updateItemPrice(ONE_MONTH_SKU, inventory.getSkuDetails(ONE_MONTH_SKU).getPrice());
            MainActivity.updateItemPrice(ONE_YEAR_SKU, inventory.getSkuDetails(ONE_YEAR_SKU).getPrice());
            MainActivity.updateItemPrice(FOREVER_SKU, inventory.getSkuDetails(FOREVER_SKU).getPrice());

            boolean hasPurchase = inventory.hasPurchase(ONE_MONTH_SKU) || inventory.hasPurchase(ONE_YEAR_SKU) || inventory.hasPurchase(FOREVER_SKU);
//            try {
//                mHelper.consumeAsync(inventory.getPurchase(FOREVER_SKU), null);
//            } catch (IabAsyncInProgressException e) {
//                Log.e(TAG, "Error querying inventory. Another async operation in progress.");
//            }
            MainActivity.updateHasPurchase(hasPurchase);
        }
    };

    public MainActivity()
    {

    }

    // this is needed to support transitions between Landscape/InvertedLandscape
    // and Portrait/InvertedPortrait orientations in Qt apps
    // this won't work without changes in QtActivityDelegate.java and QtActivity.java (for Qt 5.8)
    // take a look at the /docs directory where you'll find a file with description
    // on how to patch QtActivityDelegate and QtActivity properly
    private void registerSensorListener()
    {
        if (mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).size() != 0) {
            Sensor sensor = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
            mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    public void initInAppBilling()
    {
        // init In-App Billing
        mHelper = new IabHelper(this, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAit4rBwbiX+0xjl3JVDxqF6eI+Cu8F8CBF3OrxgweAINduSpOLKb" +
                "4+hCcs4qdyMYu+f7ONEl4NboZH4oGLNuV+0VbEAzpkEJNEEDYpLUWl+0hs9LXkd++t81P7bX+gkiFcMiqz8ZSSMxmlYt5fTHB0fgHCIB9IMDYOzC04YTYtkv1" +
                "j6sZ3dLfIY48GgmzYIr9WBzb+iQLwJmYtGYRcLcKG/ho6bSHrUv4m5E38SbKWJiWHbPmcviekN3EUy6sL0SdeBgAh+uIwvVnAZ5+CbQRE+8F9h77r7cNYhBFY" +
                "y36C9nnNu00POMEa1NDyHNyFX6vHCb1pzWL/WX4uSxjWSS8kQIDAQAB");

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                onBillingStatusChanged(result.isSuccess());

                if (!result.isSuccess()) {
                    MainActivity.this.sendExceptionEvent("Problem setting up In-app Billing: " + result, false);
                    return;
                }

                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        MainActivity.this.requestUpdateData();
                    }
                });
            }
        });
    }

    public void requestUpdateData()
    {
        //request data from the server
        List<String> itemSkus = new ArrayList<String>();
        itemSkus.add(FOREVER_SKU);
        List<String> subsSkus = new ArrayList<String>();
        subsSkus.add(ONE_MONTH_SKU);
        subsSkus.add(ONE_YEAR_SKU);

        try {
            mHelper.queryInventoryAsync(true, itemSkus, subsSkus, mUpdateDataListener);
        } catch (IabAsyncInProgressException e) {
            Log.e(TAG, "Error querying inventory. Another async operation in progress.");
        }
    }

    public void purchaseItem(String id)
    {
        try {
            mHelper.launchPurchaseFlow(this, id, PURCHASE_REQUEST_CODE, mPurchaseFinishedListener);
        } catch (IabAsyncInProgressException e) {
            Log.e(TAG, "Error querying inventory. Another async operation in progress.");
        }
    }

    public void purchaseSubscription(String id)
    {
        try {
            mHelper.launchSubscriptionPurchaseFlow(this, id, PURCHASE_REQUEST_CODE, mPurchaseFinishedListener);
        } catch (IabAsyncInProgressException e) {
            Log.e(TAG, "Error querying inventory. Another async operation in progress.");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // init Google Analytics
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        mTracker = analytics.newTracker("UA-89808827-1");
        mTracker.enableExceptionReporting(true);

        //init AppsFlyer
        AppsFlyerLib.getInstance().startTracking(this.getApplication(), "qbjfvMkFezzW9fe5nmZty");

        // init Sensors to detect inverted screen orientation
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        registerSensorListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mHelper != null) {
            mHelper.disposeWhenFinished();
            mHelper = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper == null) return;

        mHelper.handleActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        super.onSensorChanged(event);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume()
    {
        super.onResume();

        //set upper bar color to purple on Android 5.0+
        //user can see it when calls Recent apps
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setTaskDescription(new TaskDescription(null, null, Color.parseColor("#7E57C2")));
        }

        registerSensorListener();
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        mSensorManager.unregisterListener(this);
    }

    public void sendAnalyticsEvent(String category, String eventName)
    {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(eventName)
                .build());
    }


    public void sendExceptionEvent(String description, boolean fatal)
    {
        mTracker.send(new HitBuilders.ExceptionBuilder()
                .setDescription (description)
                .setFatal(fatal)
                .build());
    }

    public String getCurrentAppVersion()
    {
        String version = "";
        try{
            version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (NameNotFoundException ex) {}

        return version;
    }

    public boolean isTablet()
    {
        return getResources().getBoolean(R.bool.isTablet);
    }

    public void updateImageInGallery(String filePath)
    {
        Intent intent =
                new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.fromFile(new File(filePath)));
        sendBroadcast(intent);
    }

    public void shareImage(String appId, String filePath)
    {
        File newFile = new File(filePath);
        Uri contentUri = FileProvider.getUriForFile(this, "com.tarasovmobile.artbreak.fileprovider", newFile);

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        share.putExtra(Intent.EXTRA_STREAM, contentUri);
        if (!appId.isEmpty())
            share.setPackage(appId);
        startActivity(Intent.createChooser(share, ""));
    }

    public boolean openInstagramApp(String url)
    {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setPackage("com.instagram.android");

        final PackageManager packageManager = getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean intentAvailable = list.size() > 0;
        if (intentAvailable) {
            startActivity(intent);
            return true;
        }

        return false;
    }

    public void requestScreenOrientation(int orientation)
    {
        setRequestedOrientation(orientation);
    }

    public void enableScreenOrientationChange()
    {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    public void disableScreenOrientationChange()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        } else {
            int rotation = getWindowManager().getDefaultDisplay().getRotation();

            switch (rotation) {
                case Surface.ROTATION_0: //0
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    break;
                case Surface.ROTATION_90: //1
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    break;
                case Surface.ROTATION_180: //2
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                    break;
                case Surface.ROTATION_270: //3
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                    break;
            }
        }
    }
}
