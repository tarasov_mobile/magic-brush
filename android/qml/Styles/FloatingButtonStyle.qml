import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import com.tarasovmobile.managers 1.0

ButtonStyle {
    id: root

    background: Rectangle {
        implicitWidth: StyleManager.styles.floatingButton.size
        implicitHeight: StyleManager.styles.floatingButton.size
        color: StyleManager.styles.floatingButton.bgColor
        radius: width / 2
        opacity: control.pressed ? 0.75 : 1.0
        layer.enabled: true
        layer.effect: DropShadow {
            transparentBorder: true
            verticalOffset: 2
            horizontalOffset: 1
            color: "#7c7b7b"
            samples: 41
            radius: 20
        }
    }

    label: Label {
        anchors.fill: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: "white"
        font.pixelSize: StyleManager.styles.floatingButton.fontSize
        text: control.text
    }
}
