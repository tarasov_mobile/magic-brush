import QtQuick 2.7

Rectangle {
    id: root

    anchors.fill: parent
    color: DefaultAppColor
    opacity: 0.5
}
