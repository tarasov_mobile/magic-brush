import QtQuick 2.7
import QtQuick.Controls 2.1

CheckBox {
    id: root

    property int size: 20

    property color color: "#B288FB"

    checked: true

    indicator: Rectangle {
        implicitWidth: root.size
        implicitHeight: root.size
        x: root.leftPadding
        y: parent.height / 2 - height / 2
        radius: 3
        color: root.checked ? root.color : "transparent"
        border {
            width: !root.checked ? 2 : 0
            color: root.color
        }

        Image {
            anchors.centerIn: parent
            sourceSize {
                width: parent.width * 0.7
                height: parent.height * 0.7
            }
            width: parent.width * 0.7
            height: parent.height * 0.7
            fillMode: Image.PreserveAspectFit
            visible: root.checked
            source: "qrc:/icons/active_color_white.svg"
        }
    }

    contentItem: Text {
        text: root.text
        font.pixelSize: 16
        opacity: enabled ? 1.0 : 0.3
        color: root.color
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        leftPadding: root.indicator.width + root.spacing
    }
}
