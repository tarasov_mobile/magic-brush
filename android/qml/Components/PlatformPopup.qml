import QtQuick 2.7
import QtGraphicalEffects 1.0
import com.tarasovmobile.managers 1.0

Rectangle {
    id: root

    property int dialogHeight: StyleManager.styles.purchasePopup.height

    function close() {
        popupManager.hidePopup();
    }

    x: parent ? (parent.width - width) / 2 : 0
    y: parent ? (parent.height - height) / 2 : 0
    width: StyleManager.styles.purchasePopup.width
    height: root.dialogHeight

    color: "white"

    layer.enabled: true
    layer.effect: DropShadow {
        transparentBorder: true
        verticalOffset: 2
        horizontalOffset: 0
        color: "#7c7b7b"
        samples: 61
        radius: 30
        spread: 0.2
    }

    MouseArea {
        anchors.fill: parent
    }
}
