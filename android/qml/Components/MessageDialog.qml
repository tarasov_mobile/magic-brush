import QtQuick 2.7
import QtQuick.Controls 2.1

Dialog {
    id: root

    property alias text: label.text
    property var buttonModel: []

    focus: true
    x: (parent.width - this.width) / 2
    y: (parent.height - this.height) / 2
    width: 280
    header: null

    Label {
        id: label
        anchors.horizontalCenter: parent.horizontalCenter
        width: 250

        font.pixelSize: 18
        color: "#757575"
        wrapMode: Text.Wrap
    }

    footer: DialogButtonBox {
        id: buttonBox

        Repeater {
            model: root.buttonModel

            Button {
                text: modelData.text
                flat: true

                DialogButtonBox.buttonRole: modelData.defaultButton ? DialogButtonBox.RejectRole : DialogButtonBox.InvalidRole

                onClicked: {
                    if (modelData.acceptButton)
                        buttonBox.accepted();
                    else
                        buttonBox.rejected()
                }

                Component.onCompleted: contentItem.color = DefaultAppColor
            }
        }
    }
}
