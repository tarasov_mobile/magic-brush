import QtQuick 2.7
import QtQuick.Window 2.2

QtObject {
    id: root

    property var manualPage: QtObject {
        readonly property int textBottomMargin: 20
        readonly property int sideMargin: 30
        readonly property int lineHeight: 20
        readonly property int fontSize: 16
        readonly property int checkBoxSize: 20
        readonly property int checkBoxBottomMargin: 10
        readonly property int emojiSize: 22
    }

    property var manualToolbar: QtObject {
        readonly property int totalSize: 60
        readonly property int barSize: 51
        readonly property int buttonFontSize: 22
        readonly property int pageIndicatorSize: 10
    }

    property var homePage: QtObject {
        readonly property int fontSize: 16
        readonly property int emojiSize: 25

        readonly property int gridFooterHeight: 310
        readonly property int footerTitleFontSize: 16
        readonly property int footerCountFontSize: 36
        readonly property color footerTextColor: "#B1B1B1"

        readonly property int premiumButtonWidth: 180
        readonly property int premiumButtonHeight: 40
        readonly property color premiumButtonBgColor: "#EFEFF4"
        readonly property int premiumButtonFontSize: 14
        readonly property int premiumButtonTopMargin: 30

        readonly property int footerTitleTopMargin: 30
        readonly property int footerCountTopMargin: 5
        readonly property int footerPremiumTitleTopMargin: 30
    }

    property var actionBar: QtObject {
        readonly property int height: 40
        readonly property int iconSize: 24

        readonly property int portraitTitleBarFontSize: 22
        readonly property int landscapeTitleBarFontSize: 20

        readonly property int sideMargin: 16
    }

    property var roundIconButton: QtObject {
        readonly property int buttonSize: 45
        readonly property int iconSize: 20
    }

    property var floatingButton: QtObject {
        readonly property int size: 56
        readonly property int fontSize: 24
        readonly property color bgColor: DefaultAppColor
        readonly property color fontColor: "white"
    }

    property var infoPage: QtObject {
        readonly property int appIconSize: 50
        readonly property int listItemHeight: 50
        readonly property int listIconSize: 24
        readonly property int itemRowSpacing: 30
        readonly property int sideMargin: 30
        readonly property int listBottomMargin: 13
    }

    property var purchasePopup: QtObject {
        readonly property int width: 280
        readonly property int height: 260

        readonly property int headerTopMargin: 15
        readonly property int headerFontSize: 16

        readonly property int buttonColumnSpacing: 15
        readonly property int productButtonWidth: 210
        readonly property int productButtonHeight: 40

        readonly property int errorLabelFontSize: 14
        readonly property int tryAgainButtonWidth: 100
        readonly property int tryAgainButtonHeight: 30

        readonly property int buttonFontSize: 12

        readonly property int subscriptionsTextFontSize: 10

        readonly property int buttonRadius: 5
    }

    property var sharePage: QtObject {
        readonly property int removeWatermarkButtonWidth: 250
        readonly property int removeWatermarkButtonHeight: 40
        readonly property int removeWatermarkButtonFontSize: 14
    }

    property var brushPreviewPage: QtObject {
        readonly property int headerFontSize: 16
        readonly property int descriptionFontSize: 18
        readonly property int columnSpacing: Screen.primaryOrientation === Qt.PortraitOrientation ? 25 : 15
        readonly property double canvasWidthFactor: 0.8
        readonly property int premiumButtonWidth: 250
        readonly property int premiumButtonHeight: 40
        readonly property int premiumButtonFontSize: 14
    }

    property var ratePopup: QtObject {
        readonly property int height: 340

        readonly property int contentSpacing: 20
        readonly property int titleRowSpacing: 10

        readonly property int appIconSize: 50
        readonly property int titleIconSize: 18
        readonly property int titleFontSize: 20
        readonly property int descriptionFontSize: 15
        readonly property int writeReviewButtonWidth: 200
        readonly property int writeReviewButtonHeight: 40

        readonly property int buttonRadius: 5
        readonly property int buttonFontSize: 14
        readonly property int cancelFontCapitalization: Font.MixedCase
    }
}
