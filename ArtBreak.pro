TEMPLATE = app

QT += qml quick widgets concurrent svg quickcontrols2

android {
    QT += androidextras
    HEADERS += src/helpers/AndroidHelper.h
    SOURCES += src/helpers/AndroidHelper.cpp \
               src/managers/PurchaseManager_android.cpp
    RESOURCES += images.qrc android/styles.qrc android/icons_android.qrc android/qml_android.qrc

    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

    DISTFILES += \
        android/src/com/tarasovmobile/artbreak/MainActivity.java \
        android/AndroidManifest.xml \
        android/gradle/wrapper/gradle-wrapper.jar \
        android/gradlew \
        android/res/values/libs.xml \
        android/res/values/screen.xml \
        android/res/values-sw600dp/screen.xml \
        android/build.gradle \
        android/gradle/wrapper/gradle-wrapper.properties \
        android/gradlew.bat \
        android/google-services.json \
        android/res/mipmap-hdpi/ic_app.png \
        android/res/mipmap-mdpi/ic_app.png \
        android/res/mipmap-xhdpi/ic_app.png \
        android/res/mipmap-xxhdpi/ic_app.png \
        android/res/mipmap-xxxhdpi/ic_app.png \
        android/res/values/strings.xml \
        android/res/drawable-hdpi/splash_screen.png \
        android/res/drawable-land-hdpi/splash_screen.png \
        android/res/drawable-land-mdpi/splash_screen.png \
        android/res/drawable-land-xhdpi/splash_screen.png \
        android/res/drawable-land-xxhdpi/splash_screen.png \
        android/res/drawable-land-xxxhdpi/splash_screen.png \
        android/res/drawable-mdpi/splash_screen.png \
        android/res/drawable-xhdpi/splash_screen.png \
        android/res/drawable-xxhdpi/splash_screen.png \
        android/res/drawable-xxxhdpi/splash_screen.png \
        android/src/com/tarasovmobile/artbreak/util/IabBroadcastReceiver.java \
        android/src/com/tarasovmobile/artbreak/util/IabException.java \
        android/src/com/tarasovmobile/artbreak/util/IabHelper.java \
        android/src/com/tarasovmobile/artbreak/util/IabResult.java \
        android/src/com/tarasovmobile/artbreak/util/Inventory.java \
        android/src/com/tarasovmobile/artbreak/util/Purchase.java \
        android/src/com/tarasovmobile/artbreak/util/Security.java \
        android/src/com/tarasovmobile/artbreak/util/SkuDetails.java
}

ios {
    CONFIG -= bitcode
    HEADERS += src/helpers/AiosHelper.h \
               ios/src/AppDelegate.h \
               ios/src/IAPHelper/IAPHelper.h \
               ios/src/IAPHelper/IAPShare.h \
               ios/src/IAPHelper/NSString+Base64.h \
               ios/src/IAPHelper/SFHFKeychainUtils.h

    OBJECTIVE_SOURCES += src/helpers/AiosHelper.mm \
                         ios/src/AppDelegate.mm \
                         src/managers/PurchaseManager_ios.mm \
                         ios/src/IAPHelper/IAPHelper.m \
                         ios/src/IAPHelper/IAPShare.m \
                         ios/src/IAPHelper/NSString+Base64.m \
                         ios/src/IAPHelper/SFHFKeychainUtils.m

    RESOURCES += ios/images.qrc ios/styles.qrc ios/icons_ios.qrc ios/qml_ios.qrc
    INCLUDEPATH += ios/GoogleAnalytics/include ios/AppsFlyer/include
    LIBS += -framework Social -framework Photos \
            -L$$PWD/ios/GoogleAnalytics/lib -lGoogleAnalyticsServices -lAdIdAccess \
            -framework SystemConfiguration -framework CoreData -framework AdSupport -lsqlite3.0 \
            -L$$PWD/ios/AppsFlyer/lib -lAppsFlyerLib \
            -framework StoreKit

    # add FacebookSDK
    LIBS += -F$$PWD/ios/FacebookSDK -framework Bolts \
            -framework FBSDKCoreKit \
            -framework FBSDKLoginKit \
            -framework FBSDKShareKit \

    QMAKE_IOS_DEPLOYMENT_TARGET = 9.0
    QMAKE_INFO_PLIST = $$PWD/ios/info.plist

    # Needed for IAPHelper
    QMAKE_OBJECTIVE_CFLAGS += -fobjc-arc

    # Add our asset catalog
    QMAKE_ASSET_CATALOGS = $$PWD/ios/Images.xcassets

    # QMAKE_ASSET_CATALOGS_LAUNCH_IMAGE is only available on 5.9 dev. Work around until then.
    asset_catalog_launchimages.name = "ASSETCATALOG_COMPILER_LAUNCHIMAGE_NAME"
    asset_catalog_launchimages.value = "LaunchImage" # Put the launch image name here (i.e. "LaunchImage")
    QMAKE_MAC_XCODE_SETTINGS += asset_catalog_launchimages
}

CONFIG += c++11

SOURCES += src/main.cpp \
    src/drawing/surfaces/Canvas.cpp \
    src/drawing/brushes/AbstractBrush.cpp \
    src/drawing/brushes/FurBrush.cpp \
    src/drawing/brushes/LongFurBrush.cpp \
    src/drawing/brushes/WebBrush.cpp \
    src/drawing/brushes/SquaresBrush.cpp \
    src/settings/GlobalBrushSettings.cpp \
    src/settings/BrushSettings.cpp \
    src/core/BrushFactory.cpp \
    src/drawing/brushes/SketchyBrush.cpp \
    src/drawing/brushes/ShadedBrush.cpp \
    src/drawing/brushes/ChromeBrush.cpp \
    src/drawing/brushes/Eraser.cpp \
    src/drawing/brushes/GridBrush.cpp \
    src/drawing/brushes/RibbonBrush.cpp \
    src/drawing/brushes/MirrorBrush.cpp \
    src/managers/ShareManager.cpp \
    src/helpers/ColorHelper.cpp \
    src/settings/AppSettings.cpp \
    src/drawing/brushes/AbstractProxyBrush.cpp \
    src/drawing/brushes/SymmetryBrush.cpp \
    src/drawing/brushes/SimpleBrush.cpp \
    src/managers/AnalyticsManager.cpp \
    src/managers/StorageManager.cpp \
    src/models/StorageModel.cpp \
    src/helpers/ImageHelper.cpp \
    src/helpers/ScreenHelper.cpp \
    src/helpers/UrlHelper.cpp \
    src/managers/PurchaseManager.cpp \
    src/managers/AppEventManager.cpp


RESOURCES += qml.qrc \
    icons.qrc \
    translations.qrc

TRANSLATIONS = translations/magic_brush_ru.ts

lupdate_only {
SOURCES = src/qml/*.qml \
          src/qml/Components/*.qml \
          src/qml/Pages/*.qml \
          src/qml/Popups/*.qml \
          src/managers/*.cpp \
          src/managers/*.mm \
          ios/src/*.mm
}

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    src/drawing/surfaces/Canvas.h \
    src/drawing/brushes/AbstractBrush.h \
    src/drawing/brushes/FurBrush.h \
    src/drawing/brushes/LongFurBrush.h \
    src/drawing/brushes/WebBrush.h \
    src/drawing/brushes/SquaresBrush.h \
    src/settings/GlobalBrushSettings.h \
    src/settings/BrushSettings.h \
    src/core/BrushFactory.h \
    src/drawing/brushes/SketchyBrush.h \
    src/drawing/brushes/ShadedBrush.h \
    src/drawing/brushes/ChromeBrush.h \
    src/drawing/brushes/Eraser.h \
    src/drawing/brushes/GridBrush.h \
    src/drawing/brushes/RibbonBrush.h \
    src/drawing/brushes/MirrorBrush.h \
    src/managers/ShareManager.h \
    src/helpers/ColorHelper.h \
    src/settings/AppSettings.h \
    src/drawing/brushes/AbstractProxyBrush.h \
    src/drawing/brushes/SymmetryBrush.h \
    src/drawing/brushes/SimpleBrush.h \
    src/managers/AnalyticsManager.h \
    src/managers/StorageManager.h \
    src/models/StorageModel.h \
    src/helpers/ImageHelper.h \
    src/helpers/ScreenHelper.h \
    src/helpers/UrlHelper.h \
    src/managers/PurchaseManager.h \
    src/managers/PurchaseManager_p.h \
    src/common/Constants.h \
    src/managers/AppEventManager.h
