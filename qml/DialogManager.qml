pragma Singleton
import QtQuick 2.7
import QtQuick.Window 2.2

QtObject {
    id: root

    property var activeDialog: undefined

    property Component messageDialogComponent: Qt.createComponent("qrc:/qml/Components/MessageDialog.qml")

    property var currentWindow: undefined

    function createDialog(titleText, messageText) {
        var dialog = messageDialogComponent.createObject(currentWindow.contentItem);
        dialog.title = titleText;
        dialog.text = messageText;

        dialog.visibleChanged.connect(function() {
            if (dialog.visible) {
                root.activeDialog = dialog;
            } else {
                dialog.destroy();
            }
        });

        return dialog;
    }

    function showQuestionDialog(questionText, acceptText, acceptCallback) {
        var dialog;
        if (Qt.platform.os == "ios")
            dialog = createDialog(questionText, "");
        else if (Qt.platform.os == "android")
            dialog = createDialog("", questionText);

        dialog.buttonModel = [
                    { "text": qsTr("Cancel"), "defaultButton": true },
                    { "text": acceptText, "destructive": true, "acceptButton": true },
                ];

        dialog.accepted.connect(acceptCallback);

        dialog.open();
    }

    function showErrorDialog(errorText) {
        var dialog;
        if (Qt.platform.os == "ios")
            dialog = createDialog(qsTr("Error"), errorText);
        else if (Qt.platform.os == "android")
            dialog = createDialog("", errorText);

        dialog.buttonModel = [
                    { "text": "OK" }
                ];

        dialog.open();
    }
}
