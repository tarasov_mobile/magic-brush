import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import com.tarasovmobile.utils 1.0
import com.tarasovmobile.managers 1.0
import "./Components" as Components
import "./Pages"

ApplicationWindow {
    id: mainWindow

    property bool canQuit: false //this is used for Android

    color: "#FAFAFA"
    visible: true

    width: 640
    height: 360

    Component.onCompleted: {
        DialogManager.currentWindow = this;
        StorageManager.drawWatermark = Qt.binding(function() {
            return !PurchaseManager.hasPurchase;
        });
    }

    Connections {
        target: AppEventManager

        onAppDidBecomeActive: {
            if (Qt.platform.os === "ios") {
                if (AiosHelper.isAppleReviewDialogSupported()) {
                    if (AppSettings.getCustomProperty("user_shared_picture")) {
                        AppSettings.setCustomProperty("user_shared_picture", false);
                        Utils.delayCall(50, function() {
                            AiosHelper.requestReviewDialog();
                        });
                    }

                    return;
                }
            }

            if (!AppSettings.getCustomProperty("rate_popup_disabled")) {
                if (AppSettings.getCustomProperty("user_shared_picture")) {
                    var lastShowTime = AppSettings.getCustomProperty("last_share_rate_show_time");
                    var currentDateTime = new Date();
                    if (lastShowTime !== undefined) {
                        var diff = currentDateTime.valueOf() - lastShowTime.valueOf();
                        var diffInHours = Math.round(diff / 1000 / 60 / 60);
                        // show popup again only after 24 hours
                        if (diffInHours < 24)
                            return;
                    }

                    AppSettings.setCustomProperty("last_share_rate_show_time", new Date());

                    var popup = Qt.createComponent("qrc:/qml/Popups/RatePopup.qml").createObject();
                    popup.remindLaterCallback = function() {
                        AppSettings.setCustomProperty("user_shared_picture", false);
                    };

                    popupManager.showPopup(popup, false);
                }
            }
        }

        onPictureAddedToGallery: {
            if (Qt.platform.os === "ios") {
                if (AiosHelper.isAppleReviewDialogSupported()) {
                    var count = AppSettings.getCustomProperty("saved_picture_count");
                    if (count === undefined)
                        count = 0;

                    AppSettings.setCustomProperty("saved_picture_count", ++count);
                    if (count >= 3) {
                        Utils.delayCall(50, function() {
                            AiosHelper.requestReviewDialog();
                        });
                    }

                    return;
                }
            }

            if (!AppSettings.getCustomProperty("rate_popup_disabled")) {
                var count = AppSettings.getCustomProperty("saved_picture_count");
                if (count === undefined)
                    count = 0;

                AppSettings.setCustomProperty("saved_picture_count", ++count);
                if (count >= 3) {
                    var lastShowTime = AppSettings.getCustomProperty("last_gallery_rate_show_time");
                    var currentDateTime = new Date();
                    if (lastShowTime !== undefined) {
                        var diff = currentDateTime.valueOf() - lastShowTime.valueOf();
                        var diffInHours = Math.round(diff / 1000 / 60 / 60);
                        // show popup again only after 24 hours
                        if (diffInHours < 24)
                            return;
                    }

                    AppSettings.setCustomProperty("last_gallery_rate_show_time", new Date());

                    var popup = Qt.createComponent("qrc:/qml/Popups/RatePopup.qml").createObject();
                    popup.remindLaterCallback = function() {
                        AppSettings.setCustomProperty("saved_picture_count", 0);
                    };

                    popupManager.showPopup(popup, false);
                }
            }
        }
    }

    //hide popups when screen orientation changed in order to avoid recalculations of x and y coors
    Screen.onPrimaryOrientationChanged: {
        if (popupManager.currentPopup && popupManager.currentPopup.callDefaultActionOnClose) {
            popupManager.currentPopup.callDefaultActionOnClose();
        }

        popupManager.hidePopup();
    }

    onClosing: {
        if (Qt.platform.os == "android") {
            if (DialogManager.activeDialog) {
                DialogManager.activeDialog.close();

                close.accepted = false;
                return;
            }

            if (popupManager.currentPopup) {
                //if the event is not handled by the popup then just hide it
                if (!popupManager.currentPopup.handleBackButton || !popupManager.currentPopup.handleBackButton())
                    popupManager.hidePopup();

                close.accepted = false;
                return;
            }

            if (pageView.currentItem.handleBackButton) {
                pageView.currentItem.handleBackButton();
                close.accepted = false;
                return;
            } else if (pageView.depth > 1) {
                pageView.pop();
                close.accepted = false;
                return;
            }

            if (!canQuit) {
                close.accepted = false;
                canQuit = true;

                AndroidHelper.showToast(qsTr("Press again to exit"));

                var timer = Qt.createQmlObject('import QtQuick 2.7; Timer { interval: 3000 }',
                                               mainWindow,
                                               "");
                timer.triggered.connect(function() { mainWindow.canQuit = false; timer.destroy() })
                timer.start();
            } else {
                close.accepted = true;
            }
        }
    }

    StackView {
        id: pageView

        anchors.fill: parent

        initialItem: Qt.resolvedUrl("Pages/%1.qml".arg(AppSettings.showManualAtStart ? "ManualPage" : "HomePage"))
    }

    QtObject {
        id: popupManager

        property var currentPopup: undefined

        function showPopup(popup, hideOnClickOutside) {
            if (currentPopup)
                hidePopup();

            currentPopup = popup;
            currentPopup.parent = popupFog;
            popupFog.hideOnClickOutside = hideOnClickOutside === undefined ? true : hideOnClickOutside;
            popupFog.visible = currentPopup.visible = true;
        }

        function hidePopup() {
            if (currentPopup) {
                popupFog.visible = currentPopup.visible = false;
                currentPopup.destroy();
            }
        }
    }

    Connections {
        target: StorageManager

        onError: DialogManager.showErrorDialog(errorText);
    }

    Item {
        id: popupFog

        property bool hideOnClickOutside: true

        anchors {
            fill: parent
        }

        visible: false
        MouseArea {
            anchors.fill: parent
            onPressed: {
                if (popupFog.hideOnClickOutside)
                    popupManager.hidePopup();
            }
        }
    }
}
