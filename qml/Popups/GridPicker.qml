import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import com.tarasovmobile.utils 1.0

RoundedGridPopup {
    id: root

    signal itemChanged(string itemId)
    signal lockedItemPicked(int index)

    QtObject {
        id: internal

        readonly property int thumbnailSize: DeviceIsTablet ? 42 : 36
        readonly property int lockedIconSize: DeviceIsTablet ? 17 : 12
    }

    cellSize: DeviceIsTablet ? 88 : 80
    delegate: Item {
        width: root.cellSize
        height: root.cellSize

        Loader {
            id: lockedIconLoader

            anchors {
                right: parent.right
                top: parent.top
                margins: 4
            }

            active: modelData.locked !== undefined && modelData.locked
            sourceComponent: Image {
                width: internal.lockedIconSize
                height: internal.lockedIconSize
                sourceSize {
                    width: internal.lockedIconSize
                    height: internal.lockedIconSize
                }
                source: "qrc:/icons/ic_inapp_locked.svg"
                fillMode: Image.PreserveAspectFit
            }
        }

        Column {
            anchors.centerIn: parent
            spacing: 8

            Image {
                anchors.horizontalCenter: parent.horizontalCenter
                width: internal.thumbnailSize
                height: internal.thumbnailSize
                sourceSize {
                    width: internal.thumbnailSize
                    height: internal.thumbnailSize
                }
                fillMode: Image.PreserveAspectFit
                source: root.currentIndex == index ? modelData.activeIcon : modelData.inactiveIcon
            }

            Label {
                width: root.cellSize * 0.8
                anchors.horizontalCenter: parent.horizontalCenter
                font {
                    pixelSize: DeviceIsTablet ? 14 : 11
                    capitalization: Font.AllLowercase
                }

                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                color: "#828283"
                text: modelData.name
                wrapMode: Text.WordWrap
            }
        }

        MouseArea {
            anchors.fill: parent

            onClicked: {
                if (modelData.locked !== undefined && modelData.locked) {
                    root.lockedItemPicked(index);
                } else {
                    root.currentIndex = index;
                    Utils.delayCall(150, function() {
                        root.itemChanged(root.model[index].id);
                    });
                }
            }
        }

        Rectangle {
            anchors.right: parent.right
            width: 1
            height: parent.height
            color: "#DFDFDF"
            visible: (index + 1) % root.columnCount !== 0
        }

        Rectangle {
            anchors.bottom: parent.bottom
            width: parent.width
            height: 1
            color: "#DFDFDF"
            visible: index / root.columnCount + 1 < root.model.length / root.columnCount
        }
    }
}
