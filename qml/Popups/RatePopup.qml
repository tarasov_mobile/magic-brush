import QtQuick 2.7
import QtQuick.Controls 2.1
import com.tarasovmobile.managers 1.0

import "../Components" as Components

Components.PlatformPopup {
    id: root

    property var remindLaterCallback: undefined

    // this function is used under Android
    // simulate Remind Later button when hardware Back button clicked
    function handleBackButton() {
        remindLaterButton.clicked();

        return false;
    }

    // this function is used when popup is about to be destroyed
    // after screen orientation change
    function callDefaultActionOnClose() {
        if (root.remindLaterCallback)
            root.remindLaterCallback();
    }

    dialogHeight: StyleManager.styles.ratePopup.height

    QtObject {
        id: internal

        property int appIconSize: StyleManager.styles.ratePopup.appIconSize
        property int titleIconSize: StyleManager.styles.ratePopup.titleIconSize
    }

    Column {
        width: parent.width
        anchors.verticalCenter: parent.verticalCenter
        spacing: StyleManager.styles.ratePopup.contentSpacing

        Image {
            id: iconImage

            anchors.horizontalCenter: parent.horizontalCenter

            width: internal.appIconSize
            height: internal.appIconSize

            sourceSize {
                width: internal.appIconSize
                height: internal.appIconSize
            }
            fillMode: Image.PreserveAspectFit
            source: Qt.platform.os === "ios" ? "qrc:/icons/ic_art_break.svg" : "qrc:/icons/ic_art_break_logo.svg"
        }

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: StyleManager.styles.ratePopup.titleRowSpacing
            height: Math.max(titleLabel.paintedHeight, internal.titleIconSize)

            Label {
                id: titleLabel

                anchors.verticalCenter: parent.verticalCenter

                text: qsTr("Good job!")
                font.pixelSize: StyleManager.styles.ratePopup.titleFontSize
                color: DefaultAppColor
                textFormat: Text.StyledText
            }

            Image {
                anchors.verticalCenter: parent.verticalCenter

                width: internal.titleIconSize
                height: internal.titleIconSize

                sourceSize {
                    width: internal.titleIconSize
                    height: internal.titleIconSize
                }
                fillMode: Image.PreserveAspectFit
                source: "qrc:/icons/emojis/applause.svg"
            }
        }

        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("We hope you’re enjoying Art Break! Could you please rate the app on the %1?").arg(Qt.platform.os === "ios" ? "App Store" : "Google Play")
            width: parent.width * 0.8
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.Wrap
            font.pixelSize: StyleManager.styles.ratePopup.descriptionFontSize
            color: DefaultAppColor
        }

        Components.TextButton {
            id: writeReviewButton

            anchors.horizontalCenter: parent.horizontalCenter
            width: StyleManager.styles.ratePopup.writeReviewButtonWidth
            height: StyleManager.styles.ratePopup.writeReviewButtonHeight
            color: DefaultAppColor
            border.width: 0
            radius: StyleManager.styles.ratePopup.buttonRadius
            text: qsTr("Rate the app")
            textColor: "white"
            fontSize: StyleManager.styles.ratePopup.buttonFontSize

            onClicked: {
                AnalyticsManager.sendEvent("RATE", "WRITE_REVIEW");

                AppSettings.setCustomProperty("rate_popup_disabled", true);
                var link = Qt.platform.os === "android" ? "market://details?id=com.tarasovmobile.artbreak" : "itms-apps://itunes.apple.com/app/id1218293620?action=write-review";
                Qt.openUrlExternally(link);
                root.close();
            }
        }

        Components.TextButton {
            id: remindLaterButton

            anchors.horizontalCenter: parent.horizontalCenter
            mouseAreaMargin: -10
            width: StyleManager.styles.ratePopup.writeReviewButtonWidth
            height: fontSize
            color: "transparent"
            border.width: 0
            text: qsTr("Remind me later")
            fontSize: StyleManager.styles.ratePopup.buttonFontSize
            fontCapitalization: StyleManager.styles.ratePopup.cancelFontCapitalization

            onClicked: {
                AnalyticsManager.sendEvent("RATE", "REMIND_LATER");

                if (root.remindLaterCallback)
                    root.remindLaterCallback();

                root.close();
            }
        }

        Components.TextButton {
            id: cancelButton

            anchors.horizontalCenter: parent.horizontalCenter
            mouseAreaMargin: -10
            width: StyleManager.styles.ratePopup.writeReviewButtonWidth
            height: fontSize
            color: "transparent"
            border.width: 0
            text: qsTr("No, thanks")
            fontSize: StyleManager.styles.ratePopup.buttonFontSize
            fontCapitalization: StyleManager.styles.ratePopup.cancelFontCapitalization

            onClicked: {
                AnalyticsManager.sendEvent("RATE", "DISABLE_RATE_POPUP");

                AppSettings.setCustomProperty("rate_popup_disabled", true);
                root.close();
            }
        }
    }
}
