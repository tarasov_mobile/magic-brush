import QtQuick 2.7
import QtQuick.Window 2.2
import "../Components"

RoundedGridPopup {
    id: root

    property color currentColor: "black"
    property string settingsArrayName: ""
    property int rowCount: {
        var maxWidth = 14 * cellSize;
        var landscapeRowCount = Math.max(Math.ceil(gridModel.count / 14), 2);
        var landscapeAlternativeRowCount = Math.max(Math.ceil(gridModel.count / 7), 4);

        if ((Screen.desktopAvailableWidth - maxWidth) < 100)
            return landscapeAlternativeRowCount;

        return landscapeRowCount;
    }

    signal colorChanged(var newColor)
    signal addColorClicked()

    //this function is used under Android
    //returns true if the event is handled
    function handleBackButton() {
        if (internal.editMode) {
            internal.editMode = false;
            return true;
        }

        return false;
    }

    QtObject {
        id: internal

        property bool editMode: false
        property bool hasRemovedColors: false

        function fillColorListFromSettings() {
            gridModel.clear();
            var colors = AppSettings[root.settingsArrayName];
            for (var i = 0; i < colors.length; ++i) {
                gridModel.append({ "colorName": ColorHelper.colorToName(colors[i]) })
            }
        }

        function saveColorList() {
            if (hasRemovedColors)
                AnalyticsManager.sendEvent("DRAW", "COLOR_%1_DELETE".arg(root.settingsArrayName.indexOf("base") !== -1 ? "BASE" : "BRUSH"));

            var colors = [];
            for (var i = 0; i < gridModel.count; ++i) {
                colors.push(gridModel.get(i).colorName)
            }

            AppSettings[root.settingsArrayName] = colors;
        }
    }

    ListModel {
        id: gridModel
    }

    onVisibleChanged: {
        if (!visible)
            internal.editMode = false;
        else
            internal.fillColorListFromSettings();
    }

    cellSize: DeviceIsTablet ? 45 : 37
    columnCount: {
        var maxHeight = 14 * cellSize;
        var portraitColumnCount = Math.max(Math.ceil(gridModel.count / 14), 2);
        var portraitAlternativeColumnCount = Math.max(Math.ceil(gridModel.count / 7), 4);

        if ((Screen.height - maxHeight) < 100)
            return portraitAlternativeColumnCount;

        return portraitColumnCount;
    }
    height: root.model ? root.gridHeight + buttonBox.height : 0
    gridWidth: {
        if (Screen.primaryOrientation === Qt.PortraitOrientation) {
            return root.cellSize * root.columnCount;
        } else {
            var maxWidth = 14 * cellSize;
            var averageWidth = 7 * cellSize;

            if ((Screen.width - maxWidth) < 100)
                return averageWidth;

            return maxWidth;
        }
    }

    gridHeight: {
        if (Screen.primaryOrientation === Qt.PortraitOrientation) {
            var maxHeight = 14 * cellSize;
            var averageHeight = 7 * cellSize;

            if ((Screen.height - maxHeight) < 100)
                return averageHeight;

            return maxHeight;
        } else {
            return root.cellSize * root.rowCount;
        }
    }
    gridFlow: Screen.primaryOrientation === Qt.PortraitOrientation ? GridView.FlowTopToBottom : GridView.FlowLeftToRight
    removeTransition: Transition {
        ParallelAnimation {
            NumberAnimation { property: "opacity"; to: 0; duration: 200 }
            NumberAnimation { properties: "height"; to: 0; duration: 200 }
        }
    }
    removeDisplacedTransition: Transition {
        NumberAnimation { properties: "x,y"; duration: 200 }
    }
    model: gridModel
    delegate: Rectangle {
        id: delegateRect

        width: root.cellSize
        height: root.cellSize
        color: colorName

        Loader {
            anchors.centerIn: parent
            sourceComponent: root.currentColor == modelData && !internal.editMode ? activeColorComponent : undefined
            onLoaded: item.color = delegateRect.color
        }

        Loader {
            property int itemIndex: index

            anchors.centerIn: parent
            z: 1
            sourceComponent: internal.editMode ? removeColorComponent : undefined
            onLoaded: item.color = delegateRect.color
        }

        MouseArea {
            anchors.fill: parent

            onClicked: {
                if (internal.editMode)
                    return;

                root.currentColor = modelData;
                root.colorChanged(modelData)
            }
        }
    }

    Component {
        id: activeColorComponent

        Image {
            property color color: "white"

            anchors.centerIn: parent
            width: 25
            height: 25
            sourceSize {
                width: 25
                height: 25
            }
            fillMode: Image.PreserveAspectFit
            source: ColorHelper.getSaturationFromColor(color) < 0.05 && ColorHelper.getBrightnessFromColor(color) > 0.9 ? "qrc:/icons/active_color.svg"
                                                                                                                        : "qrc:/icons/active_color_white.svg"
        }
    }

    Component {
        id: removeColorComponent

        IconButton {
            id: removeButton

            property color color: "white"

            anchors.centerIn: parent
            size: 17
            iconSource: ColorHelper.getSaturationFromColor(color) < 0.05 && ColorHelper.getBrightnessFromColor(color) > 0.9 ? "qrc:/icons/delete_grey.svg"
                                                                                                                              : "qrc:/icons/delete_white.svg"

            transform: Rotation {
                id: rotator

                origin {
                    x: removeButton.size / 2
                    y: removeButton.size / 2
                }
                angle: 0
            }

            SequentialAnimation {
                id: shakeAnimation

                running: true

                SequentialAnimation {
                    PropertyAnimation { easing.type: Easing.InQuad; duration: 80; target: rotator; property: "angle"; to: 3 }
                    PropertyAnimation { easing.type: Easing.InQuad; duration: 80; target: rotator; property: "angle"; to: -3 }
                    loops: 3
                }
                PropertyAnimation { easing.type: Easing.InQuad; duration: 80; target: rotator; property: "angle"; to: 0 }
            }

            onClicked: {
                internal.hasRemovedColors = true;
                gridModel.remove(parent.itemIndex, 1);
            }
        }
    }

    Rectangle {
        id: upperButtonBoxLine

        anchors {
            bottom: buttonBox.top
        }

        width: parent.width
        height: 1
        color: "#DFDFDF"
        visible: Screen.primaryOrientation === Qt.LandscapeOrientation
    }
    Row {
        id: buttonBox

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: root.cellSize

        TextButton {
            border.width: 0
            color: "white"
            width: parent.width / 2
            height: parent.height
            fontSize: 22
            textColor: DefaultAppColor
            text: "+"
            visible: !internal.editMode

            onClicked: root.addColorClicked()
        }

        Rectangle {
            width: 1
            height: parent.height
            color: "#DFDFDF"
            visible: !internal.editMode
        }

        TextButton {
            border.width: 0
            color: "white"
            width: parent.width / 2
            height: parent.height
            fontSize: 22
            textColor: DefaultAppColor
            text: "–"
            visible: !internal.editMode

            onClicked: internal.editMode = true
        }

        TextButton {
            width: parent.width
            height: parent.height
            visible: internal.editMode
            text: "OK"

            onClicked: {
                internal.saveColorList();
                internal.editMode = false;
            }
        }
    }
}
