import QtQuick 2.7
import QtGraphicalEffects 1.0

Rectangle {
    id: root

    visible: false

    layer.enabled: true
    layer.effect: OpacityMask {
        maskSource: Rectangle {
            width: root.width
            height: root.height
            radius: 9
        }
    }

    Rectangle {
        id: borderRect

        anchors.fill: parent
        z: 1
        color: "transparent"
        border {
            width: 1
            color: "#DFDFDF"
        }
        radius: 8
    }
}
