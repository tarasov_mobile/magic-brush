import QtQuick 2.7
import "../Components"

RoundedPopup {
    id: root

    property alias currentColor: colorPickItem.currentColor

    function initWithColor(color) {
        currentColorRect.color = color;
        hueSlider.value = ColorHelper.getHueFromColor(color);
        colorPickItem.pickColor(color);
    }

    //this function is used under Android
    //returns true if the event is handled
    function handleBackButton() {
        root.rejected();
        return true;
    }

    signal rejected()
    signal accepted()

    width: DeviceIsTablet ? 320 : 300
    height: DeviceIsTablet ? 320 : 300

    Row {
        id: colorsRow

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        height: 50

        Rectangle {
            id: currentColorRect

            width: parent.width / 2
            height: parent.height
            color: "black"
        }

        Rectangle {
            id: newColorRect

            width: parent.width / 2
            height: parent.height
            color: colorPickItem.currentColor
        }
    }

    ColorPickItem {
        id: colorPickItem

        hueValue: hueSlider.value

        anchors {
            top: colorsRow.bottom
            bottom: hueSlider.top
            left: parent.left
            right: parent.right
        }
    }

    HueSlider {
        id: hueSlider

        anchors {
            bottom: buttonsRow.top
            left: parent.left
            right: parent.right
        }
    }

    Row {
        id: buttonsRow

        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        height: 40

        TextButton {
            border.width: 0
            color: "white"
            width: parent.width / 2
            height: parent.height
            text: qsTr("Cancel")
            onClicked: root.rejected()
        }

        TextButton {
            border.width: 0
            color: "white"
            width: parent.width / 2
            height: parent.height
            text: qsTr("OK")
            onClicked: {
                if (Qt.colorEqual(currentColorRect.color, newColorRect.color))
                    root.rejected();
                else
                    root.accepted();
            }
        }
    }
}
