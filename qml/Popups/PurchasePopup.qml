import QtQuick 2.7
import QtQuick.Controls 2.1
import com.tarasovmobile.managers 1.0

import "../Components" as Components

Components.PlatformPopup {
    id: root

    QtObject {
        id: internal

        property bool updateErrorOccured: false
        property bool pricesLoaded: PurchaseManager.monthPrice !== "" && PurchaseManager.yearPrice !== "" && PurchaseManager.foreverPrice !== ""
    }

    Connections {
        target: PurchaseManager

        onPurchaseCompleted: root.close()
        onDataUpdateError: internal.updateErrorOccured = true
    }

    MouseArea {
        anchors.fill: parent
    }

    Component.onCompleted: {
        if (!internal.pricesLoaded)
            PurchaseManager.requestUpdateData();
    }

    Label {
        id: headerLabel

        anchors {
            top: parent.top
            topMargin: StyleManager.styles.purchasePopup.headerTopMargin
            horizontalCenter: parent.horizontalCenter
        }

        width: parent.width * 0.8
        horizontalAlignment: Text.AlignHCenter
        text: qsTr("Remove all limitations")
        wrapMode: Text.Wrap
        font.pixelSize: StyleManager.styles.purchasePopup.headerFontSize
        color: DefaultAppColor
    }

    Column {
        id: buttonsColumn

        anchors {
            horizontalCenter: parent.horizontalCenter
            top: headerLabel.bottom
            topMargin: StyleManager.styles.purchasePopup.buttonColumnSpacing
        }

        spacing: StyleManager.styles.purchasePopup.buttonColumnSpacing
        visible: loader.status === Loader.Null

        Components.TextButton {
            id: monthPurchase

            width: StyleManager.styles.purchasePopup.productButtonWidth
            height: StyleManager.styles.purchasePopup.productButtonHeight
            color: DefaultAppColor
            border.width: 0
            radius: StyleManager.styles.purchasePopup.buttonRadius
            text: qsTr("For 1 month") + " - " + PurchaseManager.monthPrice
            textColor: "white"
            visible: false

            onClicked: PurchaseManager.purchaseMonthSubscription()
        }

        Components.TextButton {
            id: yearPurchase

            width: StyleManager.styles.purchasePopup.productButtonWidth
            height: StyleManager.styles.purchasePopup.productButtonHeight
            color: DefaultAppColor
            border.width: 0
            radius: StyleManager.styles.purchasePopup.buttonRadius
            text: qsTr("For 1 year") + " - " + PurchaseManager.yearPrice
            textColor: "white"
            visible: false

            onClicked: PurchaseManager.purchaseYearSubscription()
        }

        Components.TextButton {
            id: foreverPurchase

            width: StyleManager.styles.purchasePopup.productButtonWidth
            height: StyleManager.styles.purchasePopup.productButtonHeight
            color: DefaultAppColor
            border.width: 0
            radius: StyleManager.styles.purchasePopup.buttonRadius
            text: qsTr("Forever") + " - " + PurchaseManager.foreverPrice
            textColor: "white"
            fontSize: StyleManager.styles.purchasePopup.buttonFontSize

            onClicked: PurchaseManager.purchaseAppForever()
        }

        Components.TextButton {
            id: restorePurchase

            visible: Qt.platform.os === "ios"
            width: StyleManager.styles.purchasePopup.productButtonWidth
            height: fontSize
            color: "transparent"
            border.width: 0
            radius: StyleManager.styles.purchasePopup.buttonRadius
            text: qsTr("Restore purchase")
            textColor: DefaultAppColor
            fontSize: StyleManager.styles.purchasePopup.buttonFontSize

            onClicked: PurchaseManager.restoreProducts()
        }

        Label {
            width: StyleManager.styles.purchasePopup.productButtonWidth
            text: qsTr("“Forever” option is a one time payment")
            font.pixelSize: StyleManager.styles.purchasePopup.subscriptionsTextFontSize
            horizontalAlignment: Text.AlignHCenter
            color: DefaultAppColor
            wrapMode: Text.Wrap
        }
    }

    Loader {
        id: loader

        anchors.centerIn: parent

        sourceComponent: internal.updateErrorOccured ? errorComponent : (!internal.pricesLoaded || PurchaseManager.state === PurchaseManagerEnums.Active ? busyIndicatorComponent : null)
    }

    Component {
        id: busyIndicatorComponent

        BusyIndicator {
            id: control

            contentItem: Item {
                implicitWidth: 50
                implicitHeight: 50

                Image {
                    id: image
                    anchors.fill: parent

                    opacity: control.running ? 1 : 0

                    Behavior on opacity {
                        OpacityAnimator {
                            duration: 250
                        }
                    }

                    sourceSize {
                        width: 50
                        height: 50
                    }
                    source: "qrc:/icons/busy_indicator.svg"

                    RotationAnimator {
                        target: image
                        running: control.visible && control.running
                        from: 0
                        to: 360
                        loops: Animation.Infinite
                        duration: 1000
                    }
                }
            }
        }
    }

    Component {
        id: errorComponent

        Column {
            width: StyleManager.styles.purchasePopup.productButtonWidth
            spacing: StyleManager.styles.purchasePopup.buttonColumnSpacing

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                text: qsTr("Check your internet connection")
                horizontalAlignment: Text.AlignHCenter
                color: DefaultAppColor
                font.pixelSize: StyleManager.styles.purchasePopup.errorLabelFontSize
                wrapMode: Text.Wrap
            }

            Components.TextButton {
                id: tryAgainButton

                anchors.horizontalCenter: parent.horizontalCenter

                width: StyleManager.styles.purchasePopup.tryAgainButtonWidth
                height: StyleManager.styles.purchasePopup.tryAgainButtonHeight
                color: DefaultAppColor
                border.width: 0
                radius: StyleManager.styles.purchasePopup.buttonRadius
                text: qsTr("Try again")
                textColor: "white"

                onClicked: {
                    internal.updateErrorOccured = false;
                    PurchaseManager.requestUpdateData();
                }
            }
        }
    }

    Components.TextButton {
        id: cancelButton

        anchors {
            right: parent.right
            bottom: parent.bottom
            margins: 20
        }

        mouseAreaMargin: -10
        width: textPaintedWidth
        fontSize: StyleManager.styles.purchasePopup.buttonFontSize
        height: fontSize
        color: "transparent"
        border.width: 0
        text: qsTr("Cancel")
        textColor: DefaultAppColor

        onClicked: root.close()
    }
}
