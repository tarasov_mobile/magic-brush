import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: root

    property alias value: slider.value
    property alias minValue: slider.minimumValue
    property alias maxValue: slider.maximumValue
    property alias stepSize: slider.stepSize

    signal valueChangedByUser(double value)
    signal closed()

    Component.onDestruction: root.closed()

    border {
        width: 1
        color: "#DFDFDF"
    }
    visible: false
    width: DeviceIsTablet ? 64 : 55
    height: DeviceIsTablet ? 228 : 220
    radius: 8

    Column {
        width: parent.width
        anchors.centerIn: parent
        spacing: 7

        Label {
            id: valueLabel

            anchors.horizontalCenter: parent.horizontalCenter

            color: DefaultAppColor
            font.pixelSize: DeviceIsTablet ? 14 : 11
            text: root.value.toFixed(2)
        }

        Slider {
            id: slider

            anchors.horizontalCenter: parent.horizontalCenter

            orientation: Qt.Vertical
            height: DeviceIsTablet ? 188 : 180

            onValueChanged: root.valueChangedByUser(value)

            style: SliderStyle {
                groove: Rectangle {
                    implicitHeight: DeviceIsTablet ? 12 : 8
                    implicitWidth: DeviceIsTablet ? 188 : 180
                    radius: 8
                    border {
                        width: 1
                        color: "#DFDFDF"
                    }
                    color: DefaultAppColor

                    Rectangle {
                        height: parent.height
                        width: styleData.handlePosition + 6
                        implicitHeight: 8
                        implicitWidth: 180
                        radius: 8
                        border {
                            width: 1
                            color: "#DFDFDF"
                        }
                        color: "white"
                    }
                }
                handle: Rectangle {
                    anchors.centerIn: parent
                    color: DefaultAppColor
                    implicitWidth: 8
                    implicitHeight: 20
                    radius: 8
                }
            }
        }
    }
}
