import QtQuick 2.7

RoundedPopup {
    id: root

    property int cellSize: 0
    property int columnCount: 0
    property alias model: grid.model
    property alias delegate: grid.delegate
    property alias currentIndex: grid.currentIndex
    property alias gridWidth: grid.width
    property alias gridHeight: grid.height
    property alias gridFlow: grid.flow
    property alias removeTransition: grid.remove
    property alias removeDisplacedTransition: grid.removeDisplaced

    width: grid.width
    height: grid.height

    GridView {
        id: grid

        anchors.top: parent.top
        width: root.cellSize * root.columnCount
        height: root.model ? root.cellSize * Math.ceil(root.model.length / root.columnCount) : 0
        cellWidth: root.cellSize; cellHeight: root.cellSize
        interactive: false
    }
}
