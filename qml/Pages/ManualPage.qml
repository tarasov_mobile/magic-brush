import QtQuick 2.7
import QtQuick.Controls 1.4
import com.tarasovmobile.utils 1.0
import com.tarasovmobile.managers 1.0
import "../Components" as Components

Components.Page {
    id: root

    property var handleBackButton: disableStartButton ? handleBackButtonFunc : undefined

    property bool disableStartButton: false

    customStatusBarColor: "#FAFAFA"
    bottomDummyBarColor: "white"

    //this function is for Android
    function handleBackButtonFunc() {
        internal.closePage();
    }

    QtObject {
        id: internal

        readonly property int textBottomMargin: StyleManager.styles.manualPage.textBottomMargin
        readonly property int sideMargin: StyleManager.styles.manualPage.sideMargin
        readonly property int descriptionLineCount: 6
        readonly property int lineHeight: StyleManager.styles.manualPage.lineHeight
        readonly property int fontSize: StyleManager.styles.manualPage.fontSize
        readonly property int checkBoxSize: StyleManager.styles.manualPage.checkBoxSize
        readonly property int checkBoxBottomMargin: StyleManager.styles.manualPage.checkBoxBottomMargin
        readonly property int emojiSize: StyleManager.styles.manualPage.emojiSize

        property var manualsModel: [
            { "text": qsTr("Sketching with Art Break is a great way<br> to reduce stress, " +
                           "improve focus<br>and boost your productivity during a<br>busy day. It’s as beneficial as<br>meditation but more fun!" +
                           "<br><img src='image://emoji/art.svg' width='%1' height='%1'>+<img src='image://emoji/phone.svg' width='%1' height='%1'>" +
                           "=<img src='image://emoji/smile.svg' width='%1' height='%1'>").arg(StyleManager.styles.manualPage.emojiSize),
                "gif": Qt.resolvedUrl("qrc:/images/pic_1%1.gif".arg(DeviceIsTablet && Qt.platform.os == "ios" ? "_ipad" : "")) },
            { "text": qsTr("You can produce stunning art<br>within seconds<br>and there is no better therapy<br>than creating something<br>beautiful" +
                           "<br><img src='image://emoji/art.svg' width='%1' height='%1'>-><img src='image://emoji/flexed bicep.svg' width='%1' height='%1'>" +
                           "-><img src='image://emoji/fireworks.svg' width='%1' height='%1'>").arg(StyleManager.styles.manualPage.emojiSize),
                "gif": Qt.resolvedUrl("qrc:/images/pic_2%1.gif".arg(DeviceIsTablet && Qt.platform.os == "ios" ? "_ipad" : "")) },
            { "text": qsTr("Share your work with friends and fellow<br>artists using <a href=\"itag:artbreakapp\">#ArtBreakApp</a>. " +
                           "And don’t<br>forget to follow our feed!<br>" +
                           "<a href=\"iuser:art.break.app\">@art.break.app</a>" +
                           "<img src='image://emoji/hands.svg' width='%1' height='%1'>").arg(StyleManager.styles.manualPage.emojiSize),
                "image": Qt.resolvedUrl("qrc:/images/pic_3%1.jpg".arg(DeviceIsTablet && Qt.platform.os == "ios" ? "_ipad" : "")) }
        ]

        function closePage() {
            if (root.disableStartButton)
                root.Stack.view.pop();
            else
                root.Stack.view.push({ item: "qrc:/qml/Pages/HomePage.qml", replace: true })
        }
    }

    Loader {
        id: iosCloseButton

        anchors {
            top: parent.top
            right: parent.right
            rightMargin: StyleManager.styles.actionBar.sideMargin
        }
        z: 1
        width: StyleManager.styles.actionBar.iconSize
        height: StyleManager.styles.actionBar.height
        active: Qt.platform.os === "ios" && root.disableStartButton
        sourceComponent: Components.IconButton {
            size: StyleManager.styles.actionBar.iconSize

            anchors.verticalCenter: parent.verticalCenter
            iconSource: "qrc:/icons/ic_info_close.svg"

            onClicked: root.Stack.view.pop()
        }
    }

    ListView {
        id: swipeView

        anchors {
            top: parent.top
            bottom: manualToolbar.top
            left: parent.left
            right: parent.right
        }

        onContentXChanged: {
            if (!root.disableStartButton) {
                manualToolbar.showStartButton = (contentX >= swipeView.width * 1.5);
            }
        }

        onMovementEnded: currentIndex = indexAt(contentX, 0)

        clip: true
        cacheBuffer: swipeView.width * 3
        boundsBehavior: ListView.StopAtBounds
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        model: internal.manualsModel
        currentIndex: 0
        maximumFlickVelocity: 5000
        delegate: Item {
            id: page

            width: swipeView.width
            height: swipeView.height

            Image {
                id: fakeImage
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    margins: internal.sideMargin
                }
                width: root.width * 0.75
                height: swipeView.height - internal.sideMargin * 2
                        - (internal.lineHeight * internal.descriptionLineCount) - internal.textBottomMargin
                fillMode: Image.PreserveAspectFit
                source: internal.manualsModel[index].gif ? internal.manualsModel[index].gif : internal.manualsModel[index].image
            }

            AnimatedImage {
                id: image

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    margins: internal.sideMargin
                }
                visible: internal.manualsModel[index].gif !== undefined
                source: page.ListView.isCurrentItem && internal.manualsModel[index].gif ? internal.manualsModel[index].gif : ""

                width: root.width * 0.75
                height: swipeView.height - internal.sideMargin * 2
                        - (internal.lineHeight * internal.descriptionLineCount) - internal.textBottomMargin
                fillMode: Image.PreserveAspectFit
                paused: page.ListView.view.moving
                asynchronous: true
            }

            Label {
                id: label
                anchors {
                    left: parent.left
                    right: parent.right
                    top: image.bottom
                    margins: internal.sideMargin
                    leftMargin: 8
                    rightMargin: 8
                }
                height: lineCount * lineHeight
                lineHeightMode: Text.FixedHeight
                lineHeight: internal.lineHeight
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: internal.fontSize
                color: DefaultAppColor
                text: internal.manualsModel[index].text
                wrapMode: Text.Wrap
                textFormat: Text.StyledText
                linkColor: color

                onLinkActivated: {
                    var id = link.split(":")[1];
                    if (link.indexOf("iuser") !== -1) {
                        AnalyticsManager.sendEvent("MANUAL", "OPEN_INSTAGRAM_APP");
                        UrlHelper.openInstagramUserPage(id);
                    } else if (link.indexOf("itag") !== -1) {
                        AnalyticsManager.sendEvent("MANUAL", "OPEN_#art.break.app");
                        UrlHelper.openInstagramTagPage(id);
                    }
                }
            }

            Components.CheckBox {
                id: checkBox

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    bottom: parent.bottom
                    margins: internal.checkBoxBottomMargin
                }
                size: internal.checkBoxSize
                visible: index === swipeView.count - 1
                text: qsTr("Show at launch")
                checked: AppSettings.showManualAtStart

                onClicked: {
                    AnalyticsManager.sendEvent("MANUAL", checked ? "SHOW_MANUAL" : "DONT_SHOW_MANUAL");
                    AppSettings.showManualAtStart = checked;
                }
            }
        }
    }

    Components.ManualToolbar {
        id: manualToolbar

        showStartButton: false
        currentIndex: swipeView.currentIndex
        pageCount: internal.manualsModel.length

        onButtonClicked: internal.closePage()
    }

    Component.onCompleted: {
        if (!DeviceIsTablet)
            ScreenHelper.requestScreenOrientation(Qt.PortraitOrientation);
    }
    Component.onDestruction: ScreenHelper.enableScreenOrientationChange()
}
