import QtQuick 2.7
import QtQml 2.2
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import com.tarasovmobile.utils 1.0
import com.tarasovmobile.managers 1.0
import "../Components" as Components

Components.Page {
    id: root

    //this var is used when user goes to this page from the Home page
    property int pictureOrientation: -1

    property url pictureFilePath: undefined

    property var pixmapToShare: undefined
    onPixmapToShareChanged: {
        if (pixmapToShare) {
            root.pictureFilePath = StorageManager.savePixmapTemporary(pixmapToShare);
            if (root.pictureFilePath.toString() == "") {
                Utils.delayCall(0, function() {
                    root.Stack.view.pop();
                });
            }
        }
    }

    customStatusBarColor: "#FAFAFA"

    QtObject {
        id: internal

        property bool isSW360DPLandscape: Screen.primaryOrientation === Qt.LandscapeOrientation && Screen.height <= 360
        property bool isSW540DPLandscape: Screen.primaryOrientation === Qt.LandscapeOrientation && Screen.height <= 540

        function sharePictureTo(serviceName) {
            AnalyticsManager.sendEvent("SHARE", "SAVE_%1".arg(serviceName.length ? serviceName.toUpperCase() : "OTHER"));
            ShareManager.shareImage(root.pictureFilePath, serviceName);
        }
    }

    Components.SimpleActionBar {
        id: actionBar

        titleText: qsTr("Share picture")
        leftButtonIcon: "qrc:/icons/ic_arrow_back_home.svg"
        rightButtonIcon: root.pictureOrientation === -1 ? "qrc:/icons/ic_home.svg" : ""
        rightButtonEnabled: root.pictureOrientation === -1
        color: "#FAFAFA"
        buttonsExtraMargin: 30

        onLeftButtonClicked: {
            AnalyticsManager.sendEvent("SHARE", "BACK_DRAW_SCREEN");
            root.Stack.view.pop();
        }
        onRightButtonClicked: {
            AnalyticsManager.sendEvent("SHARE", "DONE_SHARE");
            //save only when user opened the picture from the Canvas page
            if (root.pictureOrientation === -1) {
                //save the pixmap to show it on Home page
                StorageManager.savePixmap(pixmapToShare);

                var imagesCount = AppSettings.getCustomProperty("saved_images_count")
                if (imagesCount === undefined)
                    imagesCount = 0

                imagesCount++
                AppSettings.setCustomProperty("saved_images_count", imagesCount)
            }
            root.Stack.view.pop({item:null, immediate: true});
        }
    }

    Rectangle {
        id: imageRect

        anchors {
            top: actionBar.bottom
            topMargin: {
                if (Qt.platform.os === "ios") {
                    if (AiosHelper.isCurrentDeviceIsIPhoneX() && Screen.primaryOrientation === Qt.LandscapeOrientation)
                        return -(StyleManager.styles.actionBar.iconSize + ((actionBar.height - StyleManager.styles.actionBar.iconSize) / 2));
                }

                if (internal.isSW360DPLandscape) {
                    return -(StyleManager.styles.actionBar.iconSize + ((actionBar.height - StyleManager.styles.actionBar.iconSize) / 2));
                }

                if (internal.isSW540DPLandscape) {
                    return 0;
                }

                return 20;
            }
            horizontalCenter: parent.horizontalCenter
        }

        border {
            width: 1
            color: "#DFDFDF"
        }
        width: image.implicitWidth / Screen.devicePixelRatio * (Screen.primaryOrientation === Qt.PortraitOrientation ? 0.7 : 0.6)
        height: image.implicitHeight / Screen.devicePixelRatio * (Screen.primaryOrientation === Qt.PortraitOrientation ? 0.7 : 0.6)

        Image {
            id: image

            anchors {
                fill: parent
                margins: 1
            }

            source: root.pictureFilePath

            Connections {
                target: StorageManager

                onUnlockCompleted: {
                    image.source = "";
                    image.source = root.pictureFilePath;
                }
            }
        }
    }


    Row {
        id: shareRow

        anchors {
            top: imageRect.bottom
            topMargin: Screen.primaryOrientation === Qt.PortraitOrientation ? 40 : 15
            horizontalCenter: parent.horizontalCenter
        }
        spacing: 20
        enabled: !blockButtonsTimer.running
        Repeater {
            id: repeater

            property var rowModel: [
                { "callback": function() {
                    AnalyticsManager.sendEvent("SHARE", "SAVE_GALLERY");

                    if (Qt.platform.os == "android") {
                        var fileUrl = StorageManager.copyPictureToExternalStorage(root.pictureFilePath);
                        if (fileUrl.toString() != "") {
                            AndroidHelper.updateImageInGallery(fileUrl);
                            AndroidHelper.showToast(qsTr("Your picture saved into Gallery"));
                        }
                    } else if (Qt.platform.os == "ios") {
                        internal.sharePictureTo("photo_library");
                    }
                }, "icon": "qrc:/icons/ic_save_photo.svg" },
                { "callback": function() { AppSettings.setCustomProperty("user_shared_picture", true); internal.sharePictureTo("instagram") }, "icon": "qrc:/icons/instagram.svg" },
                { "callback": function() { AppSettings.setCustomProperty("user_shared_picture", true); internal.sharePictureTo("facebook") }, "icon": "qrc:/icons/facebook.svg" },
                { "callback": function() { AppSettings.setCustomProperty("user_shared_picture", true); internal.sharePictureTo(Qt.locale().name === "ru_RU" && Qt.platform.os !== "ios" ? "vk" : "twitter") },
                    "icon": Qt.locale().name === "ru_RU" && Qt.platform.os !== "ios" ? "qrc:/icons/vk.svg" : "qrc:/icons/twitter.svg" },
                { "callback": function() { AppSettings.setCustomProperty("user_shared_picture", true); internal.sharePictureTo("") }, "icon": "qrc:/icons/ic_share.svg" }
            ]

            model: rowModel

            Components.IconButton {
                size: DeviceIsTablet ? 56 : 40
                iconSource: modelData.icon

                onClicked: {
                    repeater.rowModel[index]["callback"]();
                    blockButtonsTimer.start();
                }
            }
        }
    }

    Timer {
        id: blockButtonsTimer

        interval: 1500
    }

    Components.TextButton {
        id: purchaseButton

        anchors {
            top: shareRow.bottom
            topMargin: Screen.primaryOrientation === Qt.PortraitOrientation ? 40 : 12
            horizontalCenter: parent.horizontalCenter
        }

        visible: !PurchaseManager.hasPurchase
        color: DefaultAppColor
        radius: 5
        width: StyleManager.styles.sharePage.removeWatermarkButtonWidth
        height: StyleManager.styles.sharePage.removeWatermarkButtonHeight
        fontSize: StyleManager.styles.sharePage.removeWatermarkButtonFontSize
        textColor: "white"
        text: qsTr("Remove Watermark")

        onClicked: {
            AnalyticsManager.sendEvent("SHARE", "REMOVE_WATERMARK");

            if (!PurchaseManager.billingInitialized) {
                DialogManager.showErrorDialog(Qt.platform.os == "android" ?
                            qsTr("Something wrong with Google Play. Check if it is working.")
                                                : qsTr("App Store is not available right now. Try again later."));
            } else {
                var popup = Qt.createComponent("qrc:/qml/Popups/PurchasePopup.qml").createObject();
                popupManager.showPopup(popup, false);
            }
        }
    }

    Component.onCompleted: {
        if (root.pictureOrientation === -1
                || (root.pictureOrientation === Qt.LandscapeOrientation && Screen.orientation === Qt.InvertedLandscapeOrientation)
                || (root.pictureOrientation === Qt.PortraitOrientation && Screen.orientation === Qt.InvertedPortraitOrientation)) {
            ScreenHelper.disableScreenOrientationChange();
        } else {
            ScreenHelper.requestScreenOrientation(root.pictureOrientation);
        }
    }
    Component.onDestruction: ScreenHelper.enableScreenOrientationChange()
}
