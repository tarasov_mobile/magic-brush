﻿import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import com.tarasovmobile.managers 1.0
import com.tarasovmobile.utils 1.0
import "../Components" as Components

Components.Page {
    id: root

    property string brushId: ""
    property string brushName: ""

    customStatusBarColor: "white"

    //this function is for Android
    function handleBackButton() {
        internal.closePage();
    }

    Rectangle { anchors.fill: parent }

    QtObject {
        id: internal

        property color oldBrushColor: DefaultAppColor

        function closePage() {
            root.Stack.view.pop({ immediate: true });
        }
    }

    Connections {
        target: PurchaseManager

        onPurchaseCompleted: {
            //HACK: avoid UI freeze after purchase
            Utils.delayCall(300, function() {
                internal.closePage();
            });
        }
    }

    Components.IconButton {
        id: closeButton

        anchors {
            top: parent.top
            right: parent.right
            margins: StyleManager.styles.actionBar.sideMargin
        }

        z: 1
        size: StyleManager.styles.actionBar.iconSize

        iconSource: "qrc:/icons/ic_info_close.svg"

        onClicked: internal.closePage()
    }

    Column {
        id: contentColumn

        anchors.centerIn: parent
        spacing: StyleManager.styles.brushPreviewPage.columnSpacing

        width: parent.width * 0.8

        Label {
            width: parent.width
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("Check out %1%2:").arg(Screen.primaryOrientation === Qt.PortraitOrientation ? "\n" : "").arg(brushName)
            wrapMode: Text.Wrap
            font.capitalization: Font.AllUppercase
            font.pixelSize: StyleManager.styles.brushPreviewPage.headerFontSize
            color: DefaultAppColor
        }

        AnimatedImage {
            id: animatedImage

            property int canvasWidth: Math.min(Screen.width, Screen.height) * StyleManager.styles.brushPreviewPage.canvasWidthFactor
            property int canvasHeight: Screen.primaryOrientation === Qt.PortraitOrientation ? canvasWidth : Math.min(Screen.width, Screen.height) * 0.5

            anchors.horizontalCenter: parent.horizontalCenter
            source: Qt.resolvedUrl("qrc:/images/%1%2.gif".arg(root.brushId).arg(DeviceIsTablet && Qt.platform.os == "ios" ? "_ipad" : ""))

            width: canvasWidth
            height: canvasHeight
            fillMode: Image.PreserveAspectFit
        }

        Label {
            width: parent.width
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("Get Premium to unlock additional brushes and remove watermark")
            wrapMode: Text.Wrap
            font.pixelSize: StyleManager.styles.brushPreviewPage.descriptionFontSize
            color: DefaultAppColor
        }

        Components.TextButton {
            id: purchaseButton

            anchors.horizontalCenter: parent.horizontalCenter

            color: DefaultAppColor
            radius: 5
            width: StyleManager.styles.brushPreviewPage.premiumButtonWidth
            height: StyleManager.styles.brushPreviewPage.premiumButtonHeight
            fontSize: StyleManager.styles.brushPreviewPage.premiumButtonFontSize
            textColor: "white"
            text: qsTr("Get Premium")

            onClicked: {
                AnalyticsManager.sendEvent("BRUSH_PREVIEW", "REMOVE_WATERMARK");

                if (!PurchaseManager.billingInitialized) {
                    DialogManager.showErrorDialog(Qt.platform.os == "android" ?
                                qsTr("Something wrong with Google Play. Check if it is working.")
                                                    : qsTr("App Store is not available right now. Try again later."));
                } else {
                    var popup = Qt.createComponent("qrc:/qml/Popups/PurchasePopup.qml").createObject();
                    popupManager.showPopup(popup, false);
                }
            }
        }
    }

    Component.onCompleted: {
        ScreenHelper.disableScreenOrientationChange();

        internal.oldBrushColor = GlobalBrushSettings.brushColor;
        GlobalBrushSettings.brushColor = "white";
    }

    Component.onDestruction: {
        GlobalBrushSettings.brushColor = internal.oldBrushColor;

        ScreenHelper.enableScreenOrientationChange();
    }
}
