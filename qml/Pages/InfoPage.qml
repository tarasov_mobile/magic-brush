import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import "../Components" as Components
import com.tarasovmobile.managers 1.0

Components.Page {
    id: root

    customStatusBarColor: DefaultAppColor
    bottomDummyBarColor: "#FAFAFA"

    //this function is for Android
    function handleBackButton() {
        internal.closePage();
    }

    QtObject {
        id: internal

        property int appIconSize: StyleManager.styles.infoPage.appIconSize
        property int listItemHeight: StyleManager.styles.infoPage.listItemHeight
        property int itemIconSize: StyleManager.styles.infoPage.listIconSize
        property int itemRowSpacing: StyleManager.styles.infoPage.itemRowSpacing
        property int sideMargin: StyleManager.styles.infoPage.sideMargin
        property int listBottomMargin: StyleManager.styles.infoPage.listBottomMargin

        function closePage() {
            root.Stack.view.pop({ immediate: true });
        }
    }

    Components.HomeActionBar {
        id: actionBar

        barColor: DefaultAppColor
        titleText: qsTr("Info And Feedback")
        titleFontBold: false
        showExtraTitle: false
        showExtraBar: false
        leftButtonIcon: "qrc:/icons/ic_arrow_back_white.svg"
        buttonsExtraMargin: listView.anchors.leftMargin
        z: 1
        onLeftButtonClicked: internal.closePage()
    }

    ListView {
        id: listView

        property var itemModel: []

        function reloadModel() {
            itemModel = [
                        { "icon": "qrc:/icons/ic_art_break.svg", "text": qsTr("Art Break Webpage"), "callback": function() {
                            AnalyticsManager.sendEvent("INFO", "VISIT_WEBSITE");
                            Qt.openUrlExternally("http://chaos-control.mobi/artbreak")
                        }
                        },
                        { "icon": "qrc:/icons/instagram.svg", "text": qsTr("Art Break Instagram"), "callback": function() {
                            AnalyticsManager.sendEvent("INFO", "VISIT_INSTAGRAM");
                            UrlHelper.openInstagramUserPage("art.break.app");
                        }
                        },
                        { "icon": "qrc:/icons/ic_welcome.svg", "text": qsTr("Show Welcome Screens"), "callback": function() {
                            AnalyticsManager.sendEvent("INFO", "OPEN_WELCOME");
                            root.Stack.view.push({ item: "qrc:/qml/Pages/ManualPage.qml", properties: { "disableStartButton": true } })
                        }
                        },
                        { "icon": "qrc:/icons/ic_rate.svg", "text": qsTr("Review The App"), "callback": function() {
                            AnalyticsManager.sendEvent("INFO", "WRITE_REVIEW");
                            var link = Qt.platform.os === "android" ? "market://details?id=com.tarasovmobile.artbreak" : "itms-apps://itunes.apple.com/app/id1218293620?action=write-review";
                            Qt.openUrlExternally(link);
                        }
                        },
                        { "icon": "qrc:/icons/ic_mail.svg", "text": qsTr("Send Feedback"), "callback": function() {
                            AnalyticsManager.sendEvent("INFO", "SEND_FEEDBACK");
                            Qt.openUrlExternally("mailto:artbreak@tarasov-mobile.com?subject=%1".arg("Chaos Control: Art Break for %1 v%2"
                                                                                                     .arg(Qt.platform.os === "ios" ? "iOS" : "Android")
                                                                                                     .arg(Qt.application.version)));
                        } }
                    ];
            if (!PurchaseManager.hasPurchase) {
                itemModel.push({ "icon": "qrc:/icons/ic_store.svg", "text": qsTr("Get Premium"), "callback": function() {
                    AnalyticsManager.sendEvent("INFO", "REMOVE_WATERMARK");

                    if (!PurchaseManager.billingInitialized) {
                        DialogManager.showErrorDialog(Qt.platform.os == "android" ?
                                                          qsTr("Something wrong with Google Play. Check if it is working.")
                                                        : qsTr("App Store is not available right now. Try again later."));
                    } else {
                        var popup = Qt.createComponent("qrc:/qml/Popups/PurchasePopup.qml").createObject();
                        popupManager.showPopup(popup, false);
                    }
                } });
            }

            itemModel.push({ "icon": "", "text": qsTr("Terms Of Use"), "callback": function() {
                AnalyticsManager.sendEvent("INFO", "TERMS_OF_USE");
                Qt.openUrlExternally("http://chaos-control.mobi/art_break/%1/terms_of_use.html".arg(Qt.locale().name === "ru_RU" ? "rus" : "en"))
            } });

            itemModel.push({ "icon": "", "text": qsTr("Privacy Policy"), "callback": function() {
                AnalyticsManager.sendEvent("INFO", "PRIVACY_POLICY");
                Qt.openUrlExternally("http://chaos-control.mobi/art_break/%1/privacy.html".arg(Qt.locale().name === "ru_RU" ? "rus" : "en"))
            } });

            listView.model = itemModel;
        }

        Connections {
            target: PurchaseManager

            onPurchaseCompleted: listView.reloadModel()
        }

        Component.onCompleted: reloadModel()

        anchors {
            top: actionBar.bottom
            left: parent.left
            right: parent.right
            bottom: iconRow.top
            bottomMargin: internal.listBottomMargin
            leftMargin: {
                if (Qt.platform.os === "ios") {
                    if (AiosHelper.isCurrentDeviceIsIPhoneX() && Screen.primaryOrientation === Qt.LandscapeOrientation) {
                        return 30;
                    }
                }

                return 0;
            }
        }
        header: Item { width: listView.width; height: 9 }
        clip: true
        boundsBehavior: Flickable.StopAtBounds

        delegate: Rectangle {
            width: parent.width
            height: internal.listItemHeight
            color: mouseArea.pressed ? Qt.rgba(0.49, 0.34, 0.76, 0.2) : "transparent" // DefaultAppColor with opacity 0.2

            MouseArea {
                id: mouseArea
                anchors.fill: parent

                onClicked: listView.itemModel[index]["callback"]()
            }

            Row {
                anchors {
                    fill: parent
                    margins: internal.sideMargin
                }

                spacing: internal.itemRowSpacing

                Image {
                    id: icon

                    anchors.verticalCenter: parent.verticalCenter

                    width: internal.itemIconSize
                    height: internal.itemIconSize

                    sourceSize {
                        width: internal.itemIconSize
                        height: internal.itemIconSize
                    }
                    fillMode: Image.PreserveAspectFit
                    source: modelData.icon
                    visible: source != ""
                }

                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: DeviceIsTablet ? 20 : 14
                    color: DefaultAppColor
                    text: modelData.text
                }
            }
        }
    }

    Row {
        id: iconRow

        spacing: 10

        anchors {
            bottom: parent.bottom
            left: parent.left
            margins: internal.sideMargin
            leftMargin: {
                if (Qt.platform.os === "ios") {
                    if (AiosHelper.isCurrentDeviceIsIPhoneX() && Screen.primaryOrientation === Qt.LandscapeOrientation) {
                        return internal.sideMargin + listView.anchors.leftMargin;
                    }
                }

                return internal.sideMargin;
            }
        }

        height: internal.appIconSize

        Image {
            id: iconImage

            width: internal.appIconSize
            height: internal.appIconSize

            sourceSize {
                width: internal.appIconSize
                height: internal.appIconSize
            }
            fillMode: Image.PreserveAspectFit
            source: "qrc:/icons/ic_art_break_logo.svg"

            // Easter Egg: improve quality of drawing on Android by turning on Antialiasing :)
            MouseArea {
                anchors.fill: parent

                onPressAndHold: {
                    if (Qt.platform.os === "android") {
                        var currentValue = AppSettings.getCustomProperty("use_antialiasing");
                        if (currentValue === undefined)
                            currentValue = false;

                        AppSettings.setCustomProperty("use_antialiasing", !currentValue);
                    }
                }
            }
        }

        Column {
            anchors.verticalCenter: parent.verticalCenter
            spacing: 3

            Label {
                font.bold: true
                font.pixelSize: 14
                color: DefaultAppColor
                textFormat: Text.StyledText
                text: qsTr("ART BREAK<br>by Chaos Control")
            }

            Label {
                id: versionLabel

                font.pixelSize: 12
                color: DefaultAppColor
                text: qsTr("Version %1").arg(Qt.application.version)
            }
        }
    }
}
