﻿import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import com.tarasovmobile.managers 1.0
import "../Components" as Components
import "../Popups" as Popups

Components.Page {
    id: root

    customStatusBarColor: "white"
    bottomDummyBarColor: "white"
    useMarginsOnlyInPortrait: false

    //this function is for Android
    function handleBackButton() {
        if (!canvas.canUndo) {
            root.Stack.view.pop();
            return;
        }

        var messageText = qsTr("Discard all changes?");
        var acceptCallback = function() {
            root.Stack.view.pop();
        };

        DialogManager.showQuestionDialog(messageText, qsTr("Discard"), acceptCallback);
    }

    Component.onDestruction: GlobalBrushSettings.unsetBrushColor()

    QtObject {
        id: internal

        property var portrait: QtObject {
            readonly property int toolbarButtonCount: 5
            readonly property int toolbarButtonWidth: 45
            readonly property int toolbarButtonsSpacing: 20
            readonly property int toolbarFirstButtonCenterX: (root.width - (toolbarButtonWidth * toolbarButtonCount
                                                                            + toolbarButtonsSpacing * (toolbarButtonCount - 1))) / 2
                                                             + toolbarButtonWidth / 2
        }

        property var landscape: QtObject {
            readonly property int toolbarButtonCount: 5
            readonly property int toolbarButtonHeight: 62
            readonly property int toolbarButtonLabelHeight: 17
            readonly property int toolbarButtonsSpacing: 8
            readonly property int toolbarFirstButtonCenterY: (root.height - (toolbarButtonHeight * toolbarButtonCount
                                                                             + toolbarButtonsSpacing * (toolbarButtonCount - 1))) / 2
                                                             + toolbarButtonHeight / 2
        }

        function calcLandscapePopupX(itemWidth) {
            return root.width - toolBar.width - itemWidth - 10;
        }

        function calcPopupY(itemHeight) {
            return root.height - (Screen.primaryOrientation === Qt.PortraitOrientation ? toolBar.height : toolBar.width) - itemHeight + 9;
        }

        function showColorPicker(settingsArrayName, currentColor, buttonIndex, colorChangedCallback) {
            var colorPicker = colorPickerComponent.createObject();
            colorPicker.colorChanged.connect(function(selectedColor) {
                colorChangedCallback(selectedColor);

                popupManager.hidePopup();
            });
            colorPicker.addColorClicked.connect(function() {
                popupManager.hidePopup();
                showAdvancedColorPicker(settingsArrayName, currentColor, buttonIndex, colorChangedCallback);
            });

            colorPicker.buttonIndex = buttonIndex;
            colorPicker.settingsArrayName = settingsArrayName;
            colorPicker.currentColor = currentColor;
            popupManager.showPopup(colorPicker);
        }

        function showAdvancedColorPicker(settingsArrayName, currentColor, buttonIndex, colorChangedCallback) {
            var colorPicker = advancedColorPickerComponent.createObject();
            colorPicker.rejected.connect(function() {
                popupManager.hidePopup();
                showColorPicker(settingsArrayName, currentColor, buttonIndex, colorChangedCallback);
            });
            colorPicker.accepted.connect(function() {
                var backgroundColorChanged = settingsArrayName.indexOf("base") !== -1;
                AnalyticsManager.sendEvent("DRAW", "COLOR_%1_ADD_NEW".arg(backgroundColorChanged ? "BASE" : "BRUSH"));

                var colorList = AppSettings[settingsArrayName];
                colorList.push(colorPicker.currentColor);
                AppSettings[settingsArrayName] = colorList;
                popupManager.hidePopup();

                //set newly added color as current
                if (backgroundColorChanged)
                    canvas.backgroundColor = colorPicker.currentColor;
                else
                    GlobalBrushSettings.brushColor = colorPicker.currentColor;

                showColorPicker(settingsArrayName, currentColor, buttonIndex, colorChangedCallback);
            });

            colorPicker.initWithColor(currentColor);
            popupManager.showPopup(colorPicker);
        }
    }

    Components.Canvas {
        id: canvas

        anchors {
            top: actionBar.bottom
            left: parent.left
            right: parent.right
            bottom: toolBar.top
            bottomMargin: -toolBar.borderSize
        }

        brushName: "ribbon"

        onDrawingActiveChanged: {
            if (drawingActive)
                ScreenHelper.disableScreenOrientationChange();
            else
                ScreenHelper.enableScreenOrientationChange();
        }

        states: [
            State {
                name: "landscape"
                when: Screen.primaryOrientation === Qt.LandscapeOrientation

                AnchorChanges {
                    target: canvas

                    anchors {
                        top: parent.top
                        left: actionBar.right
                        right: toolBar.left
                        bottom: parent.bottom
                    }
                }

                PropertyChanges {
                    target: canvas
                    anchors.bottomMargin: 0
                    anchors.rightMargin: -toolBar.borderSize
                }
            }
        ]
    }

    Components.ActionBar {
        id: actionBar

        function updateIconRows() {
            leftRowModel = [
                        { "callback": root.handleBackButton, "icon": "qrc:/icons/ic_arrow_back_home.svg" },
                        { "enabled": canvas.canUndo, "callback": function() {
                            if (canvas.canUndo || canvas.canRedo) {
                                var messageText = qsTr("Clear the canvas?");
                                var acceptCallback = function() {
                                    AnalyticsManager.sendEvent("DRAW", "CLEAR_CANVAS");
                                    canvas.clear();
                                };

                                DialogManager.showQuestionDialog(messageText, qsTr("Clear"), acceptCallback);
                            }
                        }, "icon": canvas.canUndo ? "qrc:/icons/ic_delete.svg" : "qrc:/icons/ic_delete_unactive.svg" },
                        { "enabled": canvas.canUndo, "callback": function() {
                            if (canvas.canUndo) {
                                AnalyticsManager.sendEvent("DRAW", "UNDO");
                                canvas.undo();
                            }
                        },
                            "icon": canvas.canUndo ? "qrc:/icons/ic_back.svg" : "qrc:/icons/ic_back_unactive.svg" },
                        { "enabled": canvas.canRedo, "callback": function() {
                            if (canvas.canRedo) {
                                AnalyticsManager.sendEvent("DRAW", "REDO");
                                canvas.redo();
                            }
                        },
                            "icon": canvas.canRedo ? "qrc:/icons/ic_forward.svg" : "qrc:/icons/ic_forward_unactive.svg" }
                    ];
            rightRowModel = [
                        { "enabled": canvas.canUndo, "callback": function() {
                            root.Stack.view.push({ item: "qrc:/qml/Pages/SharePage.qml", properties:{ pixmapToShare: canvas.pixmap } })
                        },
                            "icon": canvas.canUndo ? "qrc:/icons/ic_save_draw.svg" : "qrc:/icons/ic_save_draw_unactive.svg" }
                    ]
        }

        Connections {
            target: canvas

            onCanUndoChanged: actionBar.updateIconRows()
            onCanRedoChanged: actionBar.updateIconRows()
        }

        Component.onCompleted: updateIconRows()
    }

    Components.CanvasToolbar {
        id: toolBar

        parent: root.Stack.view && root.Stack.view.currentItem === root && popupManager.currentPopup ? popupFog : canvas.parent

        onBrushButtonClicked: {
            var brushPicker = brushPickerComponent.createObject();
            brushPicker.selectBrush(canvas.brushName);
            popupManager.showPopup(brushPicker);
        }

        onWidthButtonClicked: {
            var popup = sliderPopupComponent.createObject();
            popup.buttonIndex = 1;
            popup.minValue = GlobalBrushSettings.minBrushWidth;
            popup.maxValue = GlobalBrushSettings.maxBrushWidth;
            popup.stepSize = Qt.binding(function() { return canvas.brushName != "eraser" ? 0.05 : 1.0; })
            popup.value = GlobalBrushSettings.brushWidth;
            var oldValue = popup.value;
            popup.valueChangedByUser.connect(function(newValue) {
                GlobalBrushSettings.brushWidth = newValue;
                AppSettings.setCustomProperty("%1_width".arg(canvas.brushName), newValue);
            });
            popup.closed.connect(function() {
                if (oldValue.toFixed(2) !== GlobalBrushSettings.brushWidth.toFixed(2))
                    AnalyticsManager.sendEvent("DRAW", "CHANGE_WIDTH_" + canvas.brushName.toUpperCase())
            });
            popupManager.showPopup(popup);
        }


        onToneButtonClicked: {
            var popup = sliderPopupComponent.createObject();
            popup.buttonIndex = 2;
            popup.minValue = GlobalBrushSettings.minBrushTransparency;
            popup.maxValue = GlobalBrushSettings.maxBrushTransparency;
            popup.stepSize = 0.01;
            popup.value = GlobalBrushSettings.brushTransparency;
            var oldValue = popup.value;
            popup.valueChangedByUser.connect(function(newValue) {
                GlobalBrushSettings.brushTransparency = newValue;
                AppSettings.setCustomProperty("%1_transparency".arg(canvas.brushName), newValue);
            });
            popup.closed.connect(function() {
                if (oldValue.toFixed(2) !== GlobalBrushSettings.brushTransparency.toFixed(2))
                    AnalyticsManager.sendEvent("DRAW", "CHANGE_TONE_" + canvas.brushName.toUpperCase())
            });
            popupManager.showPopup(popup);
        }

        onBrushColorClicked: {
            internal.showColorPicker("brushColorList", GlobalBrushSettings.brushColor, 3, function(newColor) {
                AnalyticsManager.sendEvent("DRAW", "CHANGE_COLOR_BRUSH");
                GlobalBrushSettings.brushColor = newColor;
            });
        }

        onBaseColorClicked: {
            internal.showColorPicker("baseColorList", canvas.backgroundColor, 4, function(newColor) {
                AnalyticsManager.sendEvent("DRAW", "CHANGE_COLOR_BASE");
                canvas.backgroundColor = newColor;
            });
        }
    }

    Component {
        id: colorPickerComponent

        Popups.ColorPicker {
            property int buttonIndex: 0

            x: {
                if (Screen.primaryOrientation === Qt.LandscapeOrientation)
                    return internal.calcLandscapePopupX(this.width);

                var xCoor = internal.portrait.toolbarFirstButtonCenterX +
                        (internal.portrait.toolbarButtonWidth + internal.portrait.toolbarButtonsSpacing) * buttonIndex
                        - this.width / 2;

                if ((xCoor + this.width) > Screen.desktopAvailableWidth)
                    xCoor = Screen.desktopAvailableWidth - this.width - 10;

                return xCoor;
            }
            y: Screen.primaryOrientation === Qt.PortraitOrientation ?
                   internal.calcPopupY(this.height)
                 : internal.landscape.toolbarFirstButtonCenterY
                   + (internal.landscape.toolbarButtonHeight + internal.landscape.toolbarButtonsSpacing) * 5
                   - internal.landscape.toolbarButtonHeight / 2 - this.height - internal.landscape.toolbarButtonLabelHeight
        }
    }

    Component {
        id: advancedColorPickerComponent

        Popups.AdvancedColorPicker {
            property int buttonIndex: 0

            x: (root.width - this.width) / 2
            y: (root.height - this.height) / 2
        }
    }

    Component {
        id: brushPickerComponent

        Popups.GridPicker {
            function selectBrush(brushId) {
                for (var i = 0; i < model.length; ++i) {
                    if (brushId === model[i].id) {
                        this.currentIndex = i;
                        break;
                    }
                }
            }

            x: Screen.primaryOrientation === Qt.PortraitOrientation ?
                   internal.portrait.toolbarFirstButtonCenterX - internal.portrait.toolbarButtonWidth / 2
                 : internal.calcLandscapePopupX(this.width)
            y: Screen.primaryOrientation === Qt.PortraitOrientation ?
                   internal.calcPopupY(this.height)
                 : internal.landscape.toolbarFirstButtonCenterY - internal.landscape.toolbarButtonHeight / 2
            columnCount: 3
            model: [
                { "name": qsTr("Ribbon"), "inactiveIcon": "qrc:/icons/brushes/ribbon.svg",
                    "activeIcon": "qrc:/icons/brushes/ribbon_active.svg", "id": "ribbon" },
                { "name": qsTr("Simple"), "inactiveIcon": "qrc:/icons/brushes/simple.svg",
                    "activeIcon": "qrc:/icons/brushes/simple_active.svg", "id": "simple" },
                { "name": qsTr("Shaded"), "inactiveIcon": "qrc:/icons/brushes/shaded.svg",
                    "activeIcon": "qrc:/icons/brushes/shaded_active.svg", "id": "shaded" },
                { "name": qsTr("Fur"), "inactiveIcon": "qrc:/icons/brushes/fur.svg",
                    "activeIcon": "qrc:/icons/brushes/fur_active.svg", "id": "fur" },
                { "name": qsTr("Web"), "inactiveIcon": "qrc:/icons/brushes/web.svg",
                    "activeIcon": "qrc:/icons/brushes/web_active.svg", "id": "web" },
                { "name": qsTr("Mirror Ribbon"), "inactiveIcon": "qrc:/icons/brushes/mirror_ribbon.svg",
                    "activeIcon": "qrc:/icons/brushes/mirror_ribbon_active.svg", "id": "mirror_ribbon_vertical" },
                { "name": qsTr("Symmetry Ribbon"), "inactiveIcon": "qrc:/icons/brushes/symmetry_ribbon.svg",
                    "activeIcon": "qrc:/icons/brushes/symmetry_ribbon_active.svg", "id": "symmetry_ribbon" },
                { "name": qsTr("Symmetry Fur"), "inactiveIcon": "qrc:/icons/brushes/symmetry_fur.svg",
                    "activeIcon": "qrc:/icons/brushes/symmetry_fur_active.svg", "id": "symmetry_fur" },
                { "name": qsTr("Mirror Web"), "inactiveIcon": "qrc:/icons/brushes/mirror_web.svg",
                    "activeIcon": "qrc:/icons/brushes/mirror_web_active.svg", "id": "mirror_web_vertical", "locked": !PurchaseManager.hasPurchase },
                { "name": qsTr("Symmetry Web"), "inactiveIcon": "qrc:/icons/brushes/symmetry_web.svg",
                    "activeIcon": "qrc:/icons/brushes/symmetry_web_active.svg", "id": "symmetry_web", "locked": !PurchaseManager.hasPurchase },
                { "name": qsTr("Symmetry Shaded"), "inactiveIcon": "qrc:/icons/brushes/symmetry_shaded.svg",
                    "activeIcon": "qrc:/icons/brushes/symmetry_shaded_active.svg", "id": "symmetry_shaded", "locked": !PurchaseManager.hasPurchase },
                { "name": qsTr("Eraser"), "inactiveIcon": "qrc:/icons/brushes/eraser.svg",
                    "activeIcon": "qrc:/icons/brushes/eraser_active.svg", "id": "eraser" },
            ]

            onItemChanged: {
                AnalyticsManager.sendEvent("DRAW", "USE_BRUSH_" + itemId.toUpperCase());
                canvas.brushName = itemId;
                popupManager.hidePopup();
            }

            onLockedItemPicked: {
                var selectedBrushId = model[index].id;
                var selectedBrushName = model[index].name;

                AnalyticsManager.sendEvent("DRAW", "TRY_BRUSH_" + selectedBrushId.toUpperCase());
                popupManager.hidePopup();
                root.Stack.view.push({ item: "qrc:/qml/Pages/BrushPreviewPage.qml",
                                         immediate: true, properties: { brushId: selectedBrushId, brushName: selectedBrushName } });
            }
        }
    }

    Component {
        id: sliderPopupComponent

        Popups.SliderPopup {
            property int buttonIndex: 0

            x: Screen.primaryOrientation === Qt.PortraitOrientation ?
                   internal.portrait.toolbarFirstButtonCenterX +
                   (internal.portrait.toolbarButtonWidth + internal.portrait.toolbarButtonsSpacing) * buttonIndex
                   - this.width / 2
                 : internal.calcLandscapePopupX(this.width)
            y: Screen.primaryOrientation === Qt.PortraitOrientation ?
                   internal.calcPopupY(this.height)
                 : internal.landscape.toolbarFirstButtonCenterY
                   + (internal.landscape.toolbarButtonHeight + internal.landscape.toolbarButtonsSpacing)
                   - internal.landscape.toolbarButtonHeight / 2
        }
    }
}
