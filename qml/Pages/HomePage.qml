import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import com.tarasovmobile.utils 1.0
import com.tarasovmobile.managers 1.0
import "../Components" as Components

Components.Page {
    id: root

    property var handleBackButton: internal.editMode ? handleBackButtonFunc : undefined

    customStatusBarColor: DefaultAppColor

    //this function is for Android
    function handleBackButtonFunc() {
        if (internal.editMode)
            internal.editMode = false;
    }

    QtObject {
        id: internal

        property bool applyLimitations: AppSettings.getCustomProperty("apply_limitations") === undefined
                                        ? true : AppSettings.getCustomProperty("apply_limitations")

        property bool pageActive: root.Stack.status === Stack.Active
        onPageActiveChanged: {
            //remove temporary files when this page appears
            if (pageActive)
                StorageManager.removeTempPictures();
        }

        property bool editMode: false
        onEditModeChanged: {
            if (!editMode) {
                selectedPictures = [];
                selectedPicturesCount = 0;
            }
        }

        property var selectedPictures: []
        property int selectedPicturesCount: 0
        property int cellSize: Screen.primaryOrientation === Qt.PortraitOrientation ? grid.width / 2 : grid.width / 3

        readonly property int fontSize: StyleManager.styles.homePage.fontSize
        readonly property int emojiSize: StyleManager.styles.homePage.emojiSize
        readonly property int portraitTitleBarFontSize: StyleManager.styles.actionBar.portraitTitleBarFontSize
        readonly property int landscapeTitleBarFontSize: StyleManager.styles.actionBar.landscapeTitleBarFontSize


        function updateSelectedPicturesCount()
        {
            selectedPicturesCount = selectedPictures.length;
        }

        function showPurchaseDialog() {
            AnalyticsManager.sendEvent("INFO", "REMOVE_WATERMARK");

            if (!PurchaseManager.billingInitialized) {
                DialogManager.showErrorDialog(Qt.platform.os == "android" ?
                                                  qsTr("Something wrong with Google Play. Check if it is working.")
                                                : qsTr("App Store is not available right now. Try again later."));
            } else {
                var popup = Qt.createComponent("qrc:/qml/Popups/PurchasePopup.qml").createObject();
                popupManager.showPopup(popup, false);
            }
        }
    }

    Components.HomeActionBar {
        id: actionBar

        z: 1
        barColor: DefaultAppColor
        titleText: internal.editMode ? qsTr("%1 selected").arg(internal.selectedPicturesCount) : qsTr("Art Break")
        titleFontBold: !internal.editMode
        titleFontPixelSize: Screen.primaryOrientation === Qt.PortraitOrientation ?
                                (internal.editMode ? internal.portraitTitleBarFontSize * 0.9 : internal.portraitTitleBarFontSize)
                              : (internal.editMode ? internal.landscapeTitleBarFontSize * 0.9 : internal.landscapeTitleBarFontSize)
        showExtraTitle: !internal.editMode
        leftButtonIcon: internal.editMode ? "qrc:/icons/ic_arrow_back_white.svg" : "qrc:/icons/ic_info.svg"
        rightButtonIcon: internal.editMode ? (rightButtonEnabled ? "qrc:/icons/ic_delete_white.svg" : "qrc:/icons/ic_delete_unactive.svg")
                                           : (rightButtonEnabled ? "qrc:/icons/ic_edit.svg" : "qrc:/icons/ic_edit_unactive.svg")
        rightButtonEnabled: (!internal.editMode && grid.count) || (internal.editMode && internal.selectedPicturesCount)
        buttonsExtraMargin: {
            if (Qt.platform.os === "ios") {
                if (AiosHelper.isCurrentDeviceIsIPhoneX() && Screen.primaryOrientation === Qt.LandscapeOrientation) {
                    return 31;
                }
            }

            return 0;
        }

        onLeftButtonClicked: {
            if (internal.editMode) {
                internal.editMode = false;
            } else {
                AnalyticsManager.sendEvent("MAIN", "OPEN_INFO");
                root.Stack.view.push({ item: "qrc:/qml/Pages/InfoPage.qml",
                                         immediate: true })
            }
        }

        onRightButtonClicked: {
            if (!internal.editMode) {
                internal.editMode = true;
            } else {
                var messageText = qsTr("Remove selected pictures?");
                var acceptCallback = function() {
                    for (var i = 0; i < internal.selectedPicturesCount; ++i) {
                        AnalyticsManager.sendEvent("MAIN", "DELETE_PIC");
                        StorageManager.removePicture(internal.selectedPictures[i]);

                        if (Qt.platform.os === "android")
                            AndroidHelper.updateImageInGallery(internal.selectedPictures[i]);
                    }
                    internal.editMode = false;
                };

                DialogManager.showQuestionDialog(messageText, qsTr("Remove"), acceptCallback);
            }
        }
    }

    Item {
        id: infoItem

        anchors.centerIn: parent
        width: parent.width * 0.7
        height: childrenRect.height

        Label {
            id: firstLabel

            width: parent.width
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: internal.fontSize
            color: DefaultAppColor
            text: qsTr("It seems like your image %1library is empty %1<img width=\"%2\" height=\"%2\" src=\"image://emoji/open_mouth_face.svg\">")
            .arg(Screen.primaryOrientation === Qt.PortraitOrientation ? "<br>" : "&nbsp;").arg(internal.emojiSize)
            wrapMode: Text.Wrap
        }

        Label {
            id: secondLabel

            anchors {
                top: firstLabel.bottom
                topMargin: Screen.primaryOrientation === Qt.PortraitOrientation ? 15 : 30
            }

            width: parent.width
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: internal.fontSize
            color: DefaultAppColor
            text: qsTr("Let’s create a new drawing!")
        }

        Label {
            anchors {
                top: secondLabel.bottom
                topMargin: 15
            }

            width: parent.width
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: internal.fontSize
            color: DefaultAppColor
            text: qsTr("Press '+’ to create some %1magic art %2<img width=\"%3\" height=\"%3\" src=\"image://emoji/art.svg\">")
                                    .arg(Screen.primaryOrientation === Qt.PortraitOrientation ? "<br>" : "")
                                    .arg(Screen.primaryOrientation === Qt.PortraitOrientation ? "<br>" : "&nbsp;")
                                    .arg(internal.emojiSize)
        }
    }

    GridView {
        id: grid

        function reloadModel() {
            grid.model = 0;
            grid.model = StorageManager.storageModel;
        }

        anchors {
            top: actionBar.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: 2
            topMargin: -9
            leftMargin: {
                if (Qt.platform.os === "ios") {
                    if (AiosHelper.isCurrentDeviceIsIPhoneX() && Screen.primaryOrientation === Qt.LandscapeOrientation) {
                        return 35;
                    }
                }

                return 2;
            }
            rightMargin: {
                if (Qt.platform.os === "ios") {
                    if (AiosHelper.isCurrentDeviceIsIPhoneX() && Screen.primaryOrientation === Qt.LandscapeOrientation) {
                        return 35;
                    }
                }

                return 2;
            }
        }

        onCountChanged: {
            if (count == 0) {
                var imagesCount = AppSettings.getCustomProperty("saved_images_count")
                if (imagesCount === undefined) {
                    imagesCount = 0
                }

                var availableDrawings = 10 - imagesCount
                if (!internal.applyLimitations || PurchaseManager.hasPurchase || availableDrawings > 0) {
                    infoItem.visible = true
                    limitationLoader.active = false
                } else if (availableDrawings === 0) {
                    limitationLoader.active = true
                    infoItem.visible = false
                }
            } else {
                infoItem.visible = false
                limitationLoader.active = false
            }
        }

        clip: true
        cellWidth: internal.cellSize
        cellHeight: internal.cellSize
        header: Item { width: grid.width; height: 9 }
        footer: !PurchaseManager.hasPurchase && internal.applyLimitations && grid.count !== 0 ? gridFooterComponent : null

        Connections {
            target: StorageManager

            onUnlockCompleted: grid.reloadModel()
        }

        Component.onCompleted: {
            grid.reloadModel()

            if (!PurchaseManager.hasPurchase) {
                if (AppSettings.getCustomProperty("apply_limitations") === undefined) {
                    AppSettings.setCustomProperty("apply_limitations", grid.count == 0)
                    internal.applyLimitations = grid.count == 0
                }
            }
        }

        delegate: Item {
            id: delegateItem

            property bool selected: internal.selectedPicturesCount && internal.selectedPictures.indexOf(display) !== -1

            width: grid.cellWidth
            height: grid.cellHeight

            Rectangle {
                anchors {
                    fill: parent
                    margins: 2
                }

                Image {
                    id: image

                    //this method is used to load list items in usual order
                    //but not in reverse order
                    //take a look at QTBUG-35502
                    function reloadImage() {
                        Utils.delayCall(index * 40, function() {
                            image.source = display;
                        });
                    }

                    anchors.fill: parent
                    cache: false
                    asynchronous: true
                    fillMode: Image.PreserveAspectCrop
                    sourceSize {
                        //if to use smaller size then images can be blurry
                        //we use images smaller than original to reduce memory usage
                        width: internal.cellSize * 2
                        height: internal.cellSize * 2
                    }
                    opacity: status === Image.Ready ? 1 : 0
                    Behavior on opacity {
                        NumberAnimation { duration: 300 }
                    }

                    Connections {
                        target: root

                        onWidthChanged: {
                            image.source = "";
                            image.reloadImage();
                        }
                    }

                    Loader {
                        id: selectionLoader

                        anchors.fill: parent
                        active: delegateItem.selected
                        sourceComponent: Qt.createComponent("qrc:/qml/Components/ListSelector.qml")
                    }

                    Component.onCompleted: reloadImage()
                }
            }

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    if (internal.editMode) {
                        var itemIndex = internal.selectedPictures.indexOf(display);
                        if (itemIndex !== -1) {
                            internal.selectedPictures.splice(itemIndex, 1);
                        } else {
                            internal.selectedPictures.push(display);
                        }

                        internal.updateSelectedPicturesCount();
                    } else {
                        AnalyticsManager.sendEvent("MAIN", "OPEN_PIC");
                        var orientation = ImageHelper.getImageOrientation(display);
                        root.Stack.view.push({ item: "qrc:/qml/Pages/SharePage.qml",
                                                 properties:{ pictureOrientation: orientation, pictureFilePath: display } })
                    }
                }
            }
        }
    }

    Loader {
        id: limitationLoader

        anchors.centerIn: parent

        active: false
        sourceComponent: gridFooterComponent
    }

    Components.FloatingActionButton {
        anchors {
            right: parent.right
            bottom: parent.bottom
            rightMargin: {
                if (Qt.platform.os === "ios") {
                    if (AiosHelper.isCurrentDeviceIsIPhoneX()) {
                        return 45;
                    }
                }

                return 24;
            }

            bottomMargin: 24
        }

        text: "+"

        onClicked: {
            internal.editMode = false;

            var imagesCount = AppSettings.getCustomProperty("saved_images_count")
            if (!internal.applyLimitations || PurchaseManager.hasPurchase || imagesCount === undefined || imagesCount < 10) {
                AnalyticsManager.sendEvent("MAIN", "NEW_PIC");
                root.Stack.view.push({ item: "qrc:/qml/Pages/CanvasPage.qml" });
            } else {
                if (!PurchaseManager.billingInitialized) {
                    DialogManager.showErrorDialog(Qt.platform.os == "android" ?
                                                      qsTr("Something wrong with Google Play. Check if it is working.")
                                                    : qsTr("App Store is not available right now. Try again later."));
                } else {
                    var popup = Qt.createComponent("qrc:/qml/Popups/PurchasePopup.qml").createObject();
                    popupManager.showPopup(popup, false);
                }
            }
        }
    }

    Component {
        id: gridFooterComponent

        Item {
            width: grid.width
            height: StyleManager.styles.homePage.gridFooterHeight

            Label {
                id: titleLabel
                anchors {
                    top: parent.top
                    topMargin: StyleManager.styles.homePage.footerTitleTopMargin
                    horizontalCenter: parent.horizontalCenter
                }
                width: parent.width * 0.7
                font {
                    bold: true
                    pixelSize: StyleManager.styles.homePage.footerTitleFontSize
                }
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignHCenter
                color: "#B1B1B1"
                text: qsTr("Free drawings available:")
            }

            Label {
                id: countLabel

                function update() {
                    var imagesCount = AppSettings.getCustomProperty("saved_images_count")
                    if (imagesCount === undefined) {
                        imagesCount = 0
                    }

                    countLabel.text = 10 - imagesCount
                }

                anchors {
                    top: titleLabel.bottom
                    topMargin: StyleManager.styles.homePage.footerCountTopMargin
                    horizontalCenter: parent.horizontalCenter
                }
                font {
                    bold: true
                    pixelSize: StyleManager.styles.homePage.footerCountFontSize
                }
                color: StyleManager.styles.homePage.footerTextColor

                Component.onCompleted: update()

                Connections {
                    target: grid

                    onCountChanged: countLabel.update()
                }
            }

            Label {
                id: premiumTitleLabel
                anchors {
                    top: countLabel.bottom
                    topMargin: StyleManager.styles.homePage.footerPremiumTitleTopMargin
                    horizontalCenter: parent.horizontalCenter
                }
                width: parent.width * (AiosHelper.isTablet() ? 0.5 : 0.7)
                font {
                    bold: true
                    pixelSize: StyleManager.styles.homePage.footerTitleFontSize
                }
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignHCenter
                color: StyleManager.styles.homePage.footerTextColor
                text: qsTr("Get Premium for unlimited drawings")
            }

            Components.TextButton {
                id: premiumButton

                anchors {
                    top: premiumTitleLabel.bottom
                    topMargin: StyleManager.styles.homePage.premiumButtonTopMargin
                    horizontalCenter: parent.horizontalCenter
                }

                width: StyleManager.styles.homePage.premiumButtonWidth
                height: StyleManager.styles.homePage.premiumButtonHeight
                color: StyleManager.styles.homePage.premiumButtonBgColor
                border.width: 0
                radius: StyleManager.styles.purchasePopup.buttonRadius
                text: qsTr("Get Premium")
                textColor: StyleManager.styles.homePage.footerTextColor
                fontSize: StyleManager.styles.homePage.premiumButtonFontSize
                fontBold: true

                onClicked: internal.showPurchaseDialog()
            }
        }
    }
}
