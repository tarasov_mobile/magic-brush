import QtQuick 2.7
import QtQuick.Controls 1.4
import "." as Components

Components.Button {
    id: root

    property alias text: label.text
    property alias textColor: label.color
    property alias fontSize: label.font.pixelSize
    property alias fontBold: label.font.bold
    property alias fontCapitalization: label.font.capitalization
    property alias textPaintedWidth: label.paintedWidth

    Label {
        id: label

        anchors.centerIn: parent

        font {
            pixelSize: 14
            capitalization: Font.AllUppercase
        }

        color: "#828283"
        scale: paintedWidth > root.width ? (root.width / paintedWidth * 0.9) : 1
        antialiasing: true
    }
}
