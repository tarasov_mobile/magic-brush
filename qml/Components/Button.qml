import QtQuick 2.7

Rectangle {
    id: root

    property bool round: false
    property int mouseAreaMargin: 0

    signal clicked()

    width: 45
    height: 45

    border {
        width: 1
        color: "#DFDFDF"
    }
    radius: round ? 45 : 0
    opacity: mouseArea.pressed ? 0.7 : 1.0

    MouseArea {
        id: mouseArea

        anchors {
            fill: parent
            margins: root.mouseAreaMargin
        }

        onClicked: root.clicked()
    }
}
