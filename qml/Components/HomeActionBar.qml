import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Window 2.2

Item {
    id: root

    signal leftButtonClicked()
    signal rightButtonClicked()

    property color barColor: DefaultAppColor
    property alias titleText: actionBar.titleText
    property alias titleFontBold: actionBar.titleFontBold
    property alias titleFontPixelSize: actionBar.titleFontPixelSize
    property alias showExtraTitle: extraTitle.visible
    property alias showExtraBar: extraBar.visible
    property alias leftButtonIcon: actionBar.leftButtonIcon
    property alias rightButtonIcon: actionBar.rightButtonIcon
    property alias rightButtonEnabled: actionBar.rightButtonEnabled
    property alias buttonsExtraMargin: actionBar.buttonsExtraMargin

    anchors {
        left: parent.left
        right: parent.right
        top: parent.top
    }

    height: column.height

    Column {
        id: column

        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }

        SimpleActionBar {
            id: actionBar

            disableTopAnchor: true
            color: root.barColor
            titleColor: "white"
            titleFontBold: true

            onLeftButtonClicked: root.leftButtonClicked()
            onRightButtonClicked: root.rightButtonClicked()
        }

        Rectangle {
            id: extraBar

            anchors {
                left: parent.left
                right: parent.right
            }
            height: 30
            color: root.barColor

            Label {
                id: extraTitle

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    topMargin: -5
                }

                font {
                    pixelSize: Screen.primaryOrientation === Qt.PortraitOrientation ? 17 : 15
                }

                color: "white"
                linkColor: "white"
                text: {
                    var title = qsTr("by <a href=\"%1\"><b>Chaos Control</b></a>");
                    var link = Qt.platform.os === "android" ? "https://app.appsflyer.com/com.tarasovmobile.gtd?pid=artbreak&c=%1"
                                                            : "https://app.appsflyer.com/id591766437?pid=artbreak&c=%1";
                    return title.arg(link.arg(Qt.locale().name === "ru_RU" ? "rus" : "en"));
                }
                onLinkActivated: {
                    AnalyticsManager.sendEvent("MAIN", "OPEN_LINK_CC");
                    Qt.openUrlExternally(link);
                }
            }
        }

        BorderImage {
            id: border

            anchors {
                left: parent.left
                right: parent.right
            }
            height: 9

            source: "qrc:/icons/ic_rib_home.svg"
            horizontalTileMode: BorderImage.Repeat
        }
    }
}
