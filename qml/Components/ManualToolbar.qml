import QtQuick 2.7
import QtQuick.Controls 2.1
import com.tarasovmobile.managers 1.0

Item {
    id: root

    signal buttonClicked()

    property alias showStartButton: startButton.visible
    property alias currentIndex: pageIndicator.currentIndex
    property alias pageCount: pageIndicator.count

    anchors {
        bottom: parent.bottom
        left: parent.left
        right: parent.right
    }

    height: internal.totalBarSize

    QtObject {
        id: internal

        property int totalBarSize: StyleManager.styles.manualToolbar.totalSize
        property int barSize: StyleManager.styles.manualToolbar.barSize
        property int buttonFontSize: StyleManager.styles.manualToolbar.buttonFontSize
        property int pageIndicatorSize: StyleManager.styles.manualToolbar.pageIndicatorSize
    }

    BorderImage {
        id: border

        anchors {
            bottom: barRect.top
            left: parent.left
            right: parent.right
        }
        height: root.borderSize

        source: "qrc:/icons/ic_rib.svg"
        horizontalTileMode: BorderImage.Repeat
    }

    Rectangle {
        id: barRect

        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        height: internal.barSize
        color: "white"

        PageIndicator {
            id: pageIndicator

            anchors.centerIn: parent
            visible: !startButton.visible
            delegate: Rectangle {
                implicitWidth: internal.pageIndicatorSize
                implicitHeight: internal.pageIndicatorSize

                radius: width / 2
                color: DefaultAppColor

                opacity: index === pageIndicator.currentIndex ? 1 : 0.5

                Behavior on opacity {
                    OpacityAnimator {
                        duration: 100
                    }
                }
            }
        }

        TextButton {
            id: startButton

            anchors.fill: parent
            border.width: 0
            fontCapitalization: Font.MixedCase
            fontSize: internal.buttonFontSize
            color: "transparent"
            textColor: DefaultAppColor
            text: qsTr("Let's start!")
            onClicked: root.buttonClicked()
        }
    }
}
