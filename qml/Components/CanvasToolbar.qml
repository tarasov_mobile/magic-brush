import QtQuick 2.7
import QtQuick.Window 2.2
import com.tarasovmobile.managers 1.0

Item {
    id: root

    readonly property int borderSize: 9

    signal brushButtonClicked()
    signal widthButtonClicked()
    signal toneButtonClicked()
    signal brushColorClicked()
    signal baseColorClicked()

    function show() {
        root.state = "";
    }

    function hide() {
        root.state = "hidden";
    }

    anchors {
        bottom: parent.bottom
        bottomMargin: parent == popupFog && AiosHelper.isCurrentDeviceIsIPhoneX() && Screen.primaryOrientation === Qt.PortraitOrientation ? 20 : 0
        left: parent.left
        right: parent.right
        rightMargin: parent == popupFog && AiosHelper.isCurrentDeviceIsIPhoneX() && Screen.primaryOrientation === Qt.LandscapeOrientation ? 30 : 0
    }

    height: internal.totalBarSize

    QtObject {
        id: internal

        property int totalBarSize: barSize + root.borderSize
        property int barSize: DeviceIsTablet ? 88 : 76
    }

    BorderImage {
        id: border

        anchors {
            bottom: barRect.top
            left: parent.left
            right: parent.right
        }
        height: root.borderSize

        source: "qrc:/icons/ic_rib.svg"
        horizontalTileMode: BorderImage.Repeat
    }

    Rectangle {
        id: barRect

        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        height: internal.barSize
        color: "white"

        Grid {
            id: buttonsLayout

            property bool isSW320DPLandscape: Screen.primaryOrientation == Qt.LandscapeOrientation && Screen.height <= 320
            property int labelVisible: !isSW320DPLandscape

            anchors.centerIn: parent

            spacing: 20

            columns: 5

            LabelButton {
                labelText: qsTr("Brush")
                labelVisible: buttonsLayout.labelVisible
                buttonComponent: RoundIconButton {
                    source: "qrc:/icons/ic_brush.svg"
                }

                onClicked: root.brushButtonClicked()
            }

            LabelButton {
                labelText: qsTr("Width")
                labelVisible: buttonsLayout.labelVisible
                buttonComponent: RoundIconButton {
                    source: "qrc:/icons/ic_line.svg"
                }

                onClicked: root.widthButtonClicked()
            }

            LabelButton {
                labelText: qsTr("Tone")
                labelVisible: buttonsLayout.labelVisible
                buttonComponent: RoundIconButton {
                    source: "qrc:/icons/ic_tone.svg"
                }

                onClicked: root.toneButtonClicked()
            }

            LabelButton {
                labelText: qsTr("Color")
                labelVisible: buttonsLayout.labelVisible
                buttonComponent: Button {
                    width: StyleManager.styles.roundIconButton.buttonSize
                    height: StyleManager.styles.roundIconButton.buttonSize
                    round: true
                    border.width: Qt.colorEqual(barRect.color, color) ? 1 : 0
                    color: GlobalBrushSettings.brushColor
                }

                onClicked: root.brushColorClicked()
            }

            LabelButton {
                labelText: qsTr("Base")
                labelVisible: buttonsLayout.labelVisible
                buttonComponent: Button {
                    width: StyleManager.styles.roundIconButton.buttonSize
                    height: StyleManager.styles.roundIconButton.buttonSize
                    round: true
                    border.width: Qt.colorEqual(barRect.color, color) ? 1 : 0
                    color: canvas.backgroundColor
                }

                onClicked: root.baseColorClicked()
            }
        }
    }

    states: [
        State {
            name: "hidden"
            AnchorChanges { target: root; anchors.bottom: undefined; anchors.top: parent.bottom }
        },
        State {
            name: "landscape"
            when: Screen.primaryOrientation === Qt.LandscapeOrientation

            AnchorChanges { target: root; anchors.top: parent.top; anchors.bottom: parent.bottom; anchors.right: parent.right; anchors.left: undefined }
            AnchorChanges { target: border; anchors.top: parent.top; anchors.bottom: parent.bottom; anchors.left: parent.left; anchors.right: undefined }
            AnchorChanges { target: barRect; anchors.top: parent.top; anchors.bottom: parent.bottom; anchors.right: parent.right; anchors.left: undefined }
            PropertyChanges { target: root; width: internal.totalBarSize }
            PropertyChanges { target: border; source: "qrc:/icons/ic_rib_vert.svg"; width: root.borderSize; verticalTileMode: BorderImage.Repeat }
            PropertyChanges { target: barRect; width: internal.barSize }
            PropertyChanges { target: buttonsLayout; columns: 1; spacing: buttonsLayout.isSW320DPLandscape ? 20 : 8 }
        }
    ]
}
