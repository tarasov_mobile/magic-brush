import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

Slider {
    id: root

    stepSize: 0.0001
    value: 0.5
    style: SliderStyle {
        groove: Item {
            implicitWidth: 250
            implicitHeight: 50

            LinearGradient {
                anchors.fill: parent
                start: Qt.point(0, parent.height / 2)
                end: Qt.point(parent.width, parent.height / 2)
                gradient: Gradient {
                    GradientStop {
                        position: 0.000
                        color: Qt.rgba(1, 0, 0, 1)
                    }
                    GradientStop {
                        position: 0.167
                        color: Qt.rgba(1, 1, 0, 1)
                    }
                    GradientStop {
                        position: 0.333
                        color: Qt.rgba(0, 1, 0, 1)
                    }
                    GradientStop {
                        position: 0.500
                        color: Qt.rgba(0, 1, 1, 1)
                    }
                    GradientStop {
                        position: 0.667
                        color: Qt.rgba(0, 0, 1, 1)
                    }
                    GradientStop {
                        position: 0.833
                        color: Qt.rgba(1, 0, 1, 1)
                    }
                    GradientStop {
                        position: 1.000
                        color: Qt.rgba(1, 0, 0, 1)
                    }
                }
            }
        }

        handle: Rectangle {
            implicitWidth: 3
            implicitHeight: control.height
        }
    }
}
