import QtQuick 2.7

import QtQuick 2.4

Item {
    id: root

    property int size: 0
    property alias iconSource: iconImage.source
    property alias pressed: mouseArea.pressed
    property int mouseAreaVMargin: -5
    property int mouseAreaHMargin: -5

    signal clicked()

    width: size
    height: size

    opacity: !pressed ? 1 : 0.5

    Image {
        id: iconImage

        anchors.fill: parent

        sourceSize {
            width: size
            height: size
        }
        fillMode: Image.PreserveAspectFit
    }

    MouseArea {
        id: mouseArea

        anchors {
            fill: parent
            topMargin: root.mouseAreaVMargin
            bottomMargin: root.mouseAreaVMargin
            leftMargin: root.mouseAreaHMargin
            rightMargin: root.mouseAreaHMargin
        }

        onClicked: root.clicked()
    }
}
