﻿import QtQuick 2.7
import QtQuick.Window 2.2

Item {
    id: root

    default property alias data: pageContent.data

    // used only iPhone X device
    property color customStatusBarColor: "transparent"
    property color bottomDummyBarColor: "transparent"
    property bool useMarginsOnlyInPortrait: true

    Rectangle {
        id: extraTopRect

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        height: AiosHelper.isCurrentDeviceIsIPhoneX() && !Qt.colorEqual(customStatusBarColor, "transparent") && Screen.primaryOrientation === Qt.PortraitOrientation ? 30 : 0

        color: customStatusBarColor
    }

    Rectangle {
        id: extraLeftRect

        anchors {
            top: parent.top
            left: parent.left
            bottom: parent.bottom
        }

        width: !useMarginsOnlyInPortrait && AiosHelper.isCurrentDeviceIsIPhoneX()
               && !Qt.colorEqual(customStatusBarColor, "transparent") && Screen.primaryOrientation === Qt.LandscapeOrientation ? 30 : 0
        color: customStatusBarColor
    }

    Item {
        id: pageContent

        anchors {
            top: extraTopRect.bottom
            left: extraLeftRect.right
            right: extraRightRect.left
            bottom: extraBottomRect.top
        }
    }

    Rectangle {
        id: extraRightRect

        anchors {
            top: parent.top
            right: parent.right
            bottom: parent.bottom
        }

        width: !useMarginsOnlyInPortrait && AiosHelper.isCurrentDeviceIsIPhoneX()
               && !Qt.colorEqual(bottomDummyBarColor, "transparent") && Screen.primaryOrientation === Qt.LandscapeOrientation ? 30 : 0
        color: bottomDummyBarColor
    }

    Rectangle {
        id: extraBottomRect

        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        height: AiosHelper.isCurrentDeviceIsIPhoneX() && !Qt.colorEqual(bottomDummyBarColor, "transparent") && Screen.primaryOrientation === Qt.PortraitOrientation ? 20 : 0

        color: bottomDummyBarColor
    }
}
