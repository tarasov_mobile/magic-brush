import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import com.tarasovmobile.managers 1.0

Rectangle {
    id: root

    anchors {
        left: parent.left
        right: parent.right
        top: parent.top
    }

    property var leftRowModel
    property var rightRowModel

    function show() {
        root.state = "";
    }

    function hide() {
        root.state = "hidden";
    }

    height: internal.barSize

    QtObject {
        id: internal

        property int barSize: StyleManager.styles.actionBar.height + borderSize
        property int borderSize: 9
    }

    Loader {
        id: contentLoader

        anchors.fill: parent

        sourceComponent: rowComponent
    }

    Component {
        id: rowComponent

        Item {
            anchors.fill: parent

            Row {
                id: leftRow

                anchors {
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                    leftMargin: StyleManager.styles.actionBar.sideMargin
                }
                spacing: 20

                Repeater {
                    id: leftRepeater

                    model: root.leftRowModel
                    delegate: leftModelDelegate
                }
            }

            Row {
                id: rightRow

                anchors {
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                    rightMargin: StyleManager.styles.actionBar.sideMargin
                }
                spacing: 20

                Repeater {
                    id: rightRepeater

                    model: root.rightRowModel
                    delegate: rightModelDelegate
                }
            }

            BorderImage {
                anchors {
                    bottom: parent.bottom
                    left: parent.left
                    right: parent.right
                }
                height: internal.borderSize
                transform: Rotation { origin.y: internal.borderSize; axis { x: 1; y: 0; z: 0 } angle: 180 }
                source: "qrc:/icons/ic_rib.svg"
                horizontalTileMode: BorderImage.Repeat
            }
        }
    }

    Component {
        id: columnComponent

        Item {
            anchors.fill: parent

            Column {
                id: topColumn

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    topMargin: 16
                }
                spacing: 20

                Repeater {
                    id: topRepeater

                    model: root.leftRowModel
                    delegate: leftModelDelegate
                }
            }

            Column {
                id: bottomColumn

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    bottom: parent.bottom
                    bottomMargin: 16
                }
                spacing: 20

                Repeater {
                    id: bottomRepeater

                    model: root.rightRowModel
                    delegate: rightModelDelegate
                }
            }

            BorderImage {
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                    right: parent.right
                }
                width: internal.borderSize
                transform: Rotation { origin.x: internal.borderSize; axis { x: 0; y: 1; z: 0 } angle: 180 }
                source: "qrc:/icons/ic_rib_vert.svg"
                verticalTileMode: BorderImage.Repeat
            }
        }
    }

    Component {
        id: leftModelDelegate

        IconButton {
            size: StyleManager.styles.actionBar.iconSize
            mouseAreaVMargin: -((StyleManager.styles.actionBar.height - size) / 2)
            iconSource: modelData.icon
            enabled: modelData.enabled !== undefined ? modelData.enabled : true
            onClicked: root.leftRowModel[index]["callback"]()
        }
    }

    Component {
        id: rightModelDelegate

        IconButton {
            size: StyleManager.styles.actionBar.iconSize
            mouseAreaVMargin: -((StyleManager.styles.actionBar.height - size) / 2)
            iconSource: modelData.icon
            enabled: modelData.enabled !== undefined ? modelData.enabled : true
            onClicked: root.rightRowModel[index]["callback"]()
        }
    }

    states: [
        State {
            name: "hidden"
            AnchorChanges { target: root; anchors.top: undefined; anchors.bottom: parent.top }
        },
        State {
            name: "landscape"
            when: Screen.primaryOrientation === Qt.LandscapeOrientation

            AnchorChanges { target: root; anchors.top: parent.top; anchors.bottom: parent.bottom; anchors.left: parent.left; anchors.right: undefined }
            PropertyChanges { target: root; width: internal.barSize }
            PropertyChanges { target: contentLoader; sourceComponent: columnComponent }
        }
    ]
}
