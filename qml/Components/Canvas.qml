import QtQuick 2.7
import com.tarasovmobile.drawing 1.0 as Drawing

Drawing.Canvas {
    id: root

    // WORKAROUND: this is used to emulate canvas background
    // take a look at /docs/Drawing_Performance_QtQuick_Patch.txt for more info
    Rectangle {
        id: canvasBg
        z: -1
        anchors.fill: root

        color: root.backgroundColor
    }
}
