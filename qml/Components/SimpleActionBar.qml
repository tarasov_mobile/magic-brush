import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import com.tarasovmobile.managers 1.0

Rectangle {
    id: root

    signal leftButtonClicked()
    signal rightButtonClicked()

    property bool disableTopAnchor: false
    property alias titleText: title.text
    property alias titleColor: title.color
    property alias titleFontBold: title.font.bold
    property alias titleFontPixelSize: title.font.pixelSize
    property alias leftButtonIcon: leftButton.iconSource
    property alias rightButtonIcon: rightButton.iconSource
    property alias rightButtonEnabled: rightButton.enabled
    property int buttonsExtraMargin: 0

    anchors {
        left: parent.left
        right: parent.right
        top: disableTopAnchor ? undefined : parent.top
    }

    height: {
        if (Qt.platform.os === "ios") {
            if (AiosHelper.isCurrentDeviceIsIPhoneX() && Screen.primaryOrientation === Qt.LandscapeOrientation) {
                return 60;
            }
        }

        return DeviceIsTablet ? (Screen.primaryOrientation === Qt.PortraitOrientation ? 72 : 64) : (Screen.primaryOrientation === Qt.PortraitOrientation ? 56 : 48);
    }

    IconButton {
        id: leftButton

        size: StyleManager.styles.actionBar.iconSize

        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            leftMargin: StyleManager.styles.actionBar.sideMargin + root.buttonsExtraMargin
        }
        visible: iconSource !== ""

        onClicked: root.leftButtonClicked()
    }

    Label {
        id: title

        anchors.centerIn: parent
        font {
            pixelSize: Screen.primaryOrientation === Qt.PortraitOrientation ?
                           StyleManager.styles.actionBar.portraitTitleBarFontSize * 0.9 : StyleManager.styles.actionBar.landscapeTitleBarFontSize * 0.9
        }

        color: DefaultAppColor
    }

    IconButton {
        id: rightButton

        size: StyleManager.styles.actionBar.iconSize

        anchors {
            verticalCenter: parent.verticalCenter
            right: parent.right
            rightMargin: StyleManager.styles.actionBar.sideMargin + root.buttonsExtraMargin
        }

        onClicked: root.rightButtonClicked()
    }
}
