import QtQuick 2.7
import QtGraphicalEffects 1.0

Item {
    id: root

    property color currentColor: Qt.hsva(root.hueValue, handle.centerX / root.width, 1 - handle.centerY / root.height, 1)
    property double hueValue: 0.0
    onHueValueChanged: internal.updateCurrentColor()

    function pickColor(color) {
        var saturation = ColorHelper.getSaturationFromColor(color);
        var brightness = ColorHelper.getBrightnessFromColor(color);

        var handleX = saturation * root.width;
        var handleY = (1 - brightness) * root.height;

        internal.moveHandle(handleX, handleY);
        root.currentColor = color;
    }

    clip: true

    QtObject {
        id: internal

        function moveHandle(x, y) {
            if (x !== undefined)
                handle.x = x - handle.width / 2;

            if (y !== undefined)
                handle.y = y - handle.height / 2;

            updateCurrentColor();
        }

        function updateCurrentColor()
        {
            root.currentColor = Qt.hsva(root.hueValue, handle.centerX / root.width, 1 - handle.centerY / root.height, 1);
        }
    }

    LinearGradient {
        anchors.fill: parent
        start: Qt.point(0, parent.height / 2)
        end: Qt.point(parent.width, parent.height / 2)
        gradient: Gradient {
            GradientStop {
                position: 0.0; color: "#FFFFFFFF"
            }
            GradientStop {
                position: 1.0; color: Qt.hsva(root.hueValue, 1, 1, 1)
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        gradient: Gradient {
            GradientStop {
                position: 1.0; color: "#FF000000"
            }
            GradientStop {
                position: 0.0; color: "#00000000"
            }
        }
    }

    Rectangle {
        id: handle

        property int centerX: handle.x + handle.width / 2
        property int centerY: handle.y + handle.height / 2

        width: 25
        height: 25
        radius: 100
        color: "transparent"
        border.color: "white"
    }

    MouseArea {
        anchors.fill: parent
        onPressed: internal.moveHandle(mouse.x, mouse.y)
        onPositionChanged: {
            var xOutOfRange = mouse.x < 0 || mouse.x > root.width;
            var yOutOfRange = mouse.y < 0 || mouse.y > root.height;

            internal.moveHandle(xOutOfRange ? undefined : mouse.x, yOutOfRange ? undefined : mouse.y);
        }
    }
}
