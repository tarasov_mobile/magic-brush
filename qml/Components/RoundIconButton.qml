import QtQuick 2.7
import com.tarasovmobile.managers 1.0

Button {
    id: root

    property alias source: image.source

    round: true
    width: StyleManager.styles.roundIconButton.buttonSize
    height: StyleManager.styles.roundIconButton.buttonSize

    Image {
        id: image

        anchors.centerIn: parent
        width: StyleManager.styles.roundIconButton.iconSize
        height: StyleManager.styles.roundIconButton.iconSize

        sourceSize {
            width: image.width
            height: image.width
        }
        fillMode: Image.PreserveAspectFit
    }
}
