import QtQuick 2.7
import QtQuick.Controls 1.4

Column {
    id: root

    property alias buttonComponent: buttonLoader.sourceComponent
    property alias labelText: label.text
    property alias labelVisible: label.visible

    signal clicked()

    width: buttonLoader.width
    spacing: 6

    Loader {
        id: buttonLoader

        anchors.horizontalCenter: parent.horizontalCenter

        onLoaded: item.clicked.connect(root.clicked)
    }

    Label {
        id: label

        anchors.horizontalCenter: parent.horizontalCenter
        color: DefaultAppColor
        font {
            pixelSize: DeviceIsTablet ? 14 : 11
        }
    }
}
