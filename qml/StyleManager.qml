pragma Singleton
import QtQuick 2.7

QtObject {
    id: root

    property var styles: _stylesLoader.item

    readonly property var _stylesLoader: Loader {
        source: DeviceIsTablet ? "qrc:/styles/TabletStyles.qml" : "qrc:/styles/PhoneStyles.qml"
    }
}
