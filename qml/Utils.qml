pragma Singleton
import QtQuick 2.7

QtObject {
    id: root

    function delayCall(delayTime, callback) {
        var timer = Qt.createQmlObject("import QtQuick 2.5; Timer {}", root);
        timer.interval = delayTime;
        timer.triggered.connect(function() {
            callback();
            timer.destroy();
        });
        timer.start();
    }
}
