#ifndef BRUSHFACTORY_H
#define BRUSHFACTORY_H

#include <QObject>

class AbstractBrush;
class BrushFactoryPrivate;

class BrushFactory : public QObject
{
    Q_OBJECT
public:
    explicit BrushFactory(QObject *parent = 0);
    ~BrushFactory();

public:
    AbstractBrush* getBrushByName(const QString &name);

private:
    BrushFactoryPrivate *d;
};

#endif // BRUSHFACTORY_H
