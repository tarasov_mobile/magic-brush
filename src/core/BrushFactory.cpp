#include "BrushFactory.h"
#include <QHash>
#include "../drawing/brushes/AbstractBrush.h"
#include "../drawing/brushes/FurBrush.h"
#include "../drawing/brushes/LongFurBrush.h"
#include "../drawing/brushes/WebBrush.h"
#include "../drawing/brushes/SquaresBrush.h"
#include "../drawing/brushes/SketchyBrush.h"
#include "../drawing/brushes/ShadedBrush.h"
#include "../drawing/brushes/ChromeBrush.h"
#include "../drawing/brushes/GridBrush.h"
#include "../drawing/brushes/RibbonBrush.h"
#include "../drawing/brushes/Eraser.h"
#include "../drawing/brushes/MirrorBrush.h"
#include "../drawing/brushes/SymmetryBrush.h"
#include "../drawing/brushes/SimpleBrush.h"

class BrushFactoryPrivate
{
public:
    BrushFactoryPrivate()
    {

    }

    ~BrushFactoryPrivate()
    {
        qDeleteAll(brushHash);
    }

    AbstractBrush *getBrush(const QString &name)
    {
        if (!brushHash.contains(name)) {
            AbstractBrush *brush = nullptr;
            if (name == "fur")
                brush = new FurBrush;
            else if (name == "longfur")
                brush = new LongFurBrush;
            else if (name == "web")
                brush = new WebBrush;
            else if (name == "squares")
                brush = new SquaresBrush;
            else if (name == "sketchy")
                brush = new SketchyBrush;
            else if (name == "shaded")
                brush = new ShadedBrush;
            else if (name == "chrome")
                brush = new ChromeBrush;
            else if (name == "grid")
                brush = new GridBrush;
            else if (name == "ribbon")
                brush = new RibbonBrush;
            else if (name == "eraser")
                brush = new Eraser;
            else if (name == "simple")
                brush = new SimpleBrush;

            if (brush)
                brushHash.insert(name, brush);

            return brush;
        }

        return brushHash.value(name);
    }

    QHash<QString, AbstractBrush *> brushHash;
};

BrushFactory::BrushFactory(QObject *parent)
    : QObject(parent)
    , d(new BrushFactoryPrivate)

{

}

BrushFactory::~BrushFactory()
{
    delete d;
}

AbstractBrush* BrushFactory::getBrushByName(const QString &name)
{
    AbstractBrush *brush = nullptr;

    if (name.contains("mirror")) {
        QStringList splitted = name.split("_", QString::SkipEmptyParts);
        if (splitted.count() == 3) {
            MirrorBrush *mirror = nullptr;
            if (d->brushHash.contains("mirror")) {
                mirror = static_cast<MirrorBrush *>(d->brushHash.value("mirror"));
            } else {
                mirror = new MirrorBrush;
                d->brushHash.insert("mirror", mirror);
            }

            mirror->setSourceBrush(d->getBrush(splitted.at(1)));
            mirror->setBrushType(splitted.at(2));
            brush = mirror;
        }
    } else if (name.contains("symmetry")) {
        QStringList splitted = name.split("_", QString::SkipEmptyParts);
        if (splitted.count() == 2) {
            SymmetryBrush *symmetry = nullptr;
            if (d->brushHash.contains("symmetry")) {
                symmetry = static_cast<SymmetryBrush *>(d->brushHash.value("symmetry"));
            } else {
                symmetry = new SymmetryBrush;
                d->brushHash.insert("symmetry", symmetry);
            }

            symmetry->setSourceBrush(d->getBrush(splitted.at(1)));
            brush = symmetry;
        }
    } else {
        brush = d->getBrush(name);
    }

    return brush;
}
