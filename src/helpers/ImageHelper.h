#ifndef IMAGEHELPER_H
#define IMAGEHELPER_H

#include <QObject>
#include <QUrl>

class ImageHelper : public QObject
{
    Q_OBJECT

public:
    explicit ImageHelper(QObject *parent = 0);
    ~ImageHelper();

    Q_INVOKABLE Qt::ScreenOrientation getImageOrientation(const QUrl &filePath) const;
};

#endif // IMAGEHELPER_H
