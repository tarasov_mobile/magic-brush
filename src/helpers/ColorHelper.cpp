#include "ColorHelper.h"

ColorHelper::ColorHelper(QObject *parent)
    : QObject(parent)
{

}

ColorHelper::~ColorHelper()
{

}

qreal ColorHelper::getHueFromColor(const QColor &color)
{
    return color.hueF();
}

qreal ColorHelper::getSaturationFromColor(const QColor &color)
{
    return color.saturationF();
}

qreal ColorHelper::getBrightnessFromColor(const QColor &color)
{
    return color.valueF();
}

QString ColorHelper::colorToName(const QColor &color)
{
    return color.name();
}
