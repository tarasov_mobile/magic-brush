#ifndef COLORHELPER_H
#define COLORHELPER_H

#include <QColor>
#include <QObject>

class ColorHelper : public QObject
{
    Q_OBJECT
public:
    explicit ColorHelper(QObject *parent = 0);
    ~ColorHelper();

    Q_INVOKABLE qreal getHueFromColor(const QColor &color);
    Q_INVOKABLE qreal getSaturationFromColor(const QColor &color);
    Q_INVOKABLE qreal getBrightnessFromColor(const QColor &color);
    Q_INVOKABLE QString colorToName(const QColor &color);
};

#endif // COLORHELPER_H
