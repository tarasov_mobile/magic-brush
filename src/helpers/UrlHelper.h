#ifndef URLHELPER_H
#define URLHELPER_H

#include <QObject>

class UrlHelper : public QObject
{
    Q_OBJECT
public:
    explicit UrlHelper(QObject *parent = 0);
    ~UrlHelper();

public slots:
    void openInstagramUserPage(const QString &userId);
    void openInstagramTagPage(const QString &tagName);

private:
    bool openInstagramPage(const QString &urlString);
};

#endif // URLHELPER_H
