#ifndef ANDROIDHELPER_H
#define ANDROIDHELPER_H

#include <QObject>
#include <QHash>
#include <QUrl>

class AndroidHelper : public QObject
{
    Q_OBJECT
public:
    ~AndroidHelper();

    static AndroidHelper* instance();

    QString getCurrentAppVersion() const;

public slots:
    bool openInstagramApp(const QString &url);
    void sendAnalyticsEvent(const QString &categoryName, const QString &eventName);
    void updateImageInGallery(const QUrl &filePath);
    void showToast(const QString &messageText);
    void shareImage(const QString &appName, const QString &filePath);
    bool isTablet() const;

    void requestScreenOrientation(Qt::ScreenOrientation orientation);
    void enableScreenOrientationChange();
    void disableScreenOrientationChange();

    void hideSplashScreen();

    QString getCurrentScreenDensity() const;

signals:
    void pictureAddedToGallery();

private:
    explicit AndroidHelper(QObject *parent = 0);
    AndroidHelper(const AndroidHelper &)    ;
    AndroidHelper& operator=(const AndroidHelper &);

    static AndroidHelper* m_instance;
    QHash<QString /*appName*/, QString /*appId*/> m_shareAppsHash;
};

#endif // ANDROIDHELPER_H
