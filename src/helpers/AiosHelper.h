#ifndef AIOSHELPER_H
#define AIOSHELPER_H

#include <QObject>

class AiosHelper : public QObject
{
    Q_OBJECT
public:
    ~AiosHelper();

    static AiosHelper* instance();

    QString getCurrentAppVersion() const;

    Q_INVOKABLE bool isAppleReviewDialogSupported() const;

    Q_INVOKABLE bool isCurrentDeviceIsIPhoneX() const;

public slots:
    void requestScreenOrientation(Qt::ScreenOrientation orientation);
    void enableScreenOrientationChange();
    void disableScreenOrientationChange();

    bool isTablet() const;

    void shareImage(const QString &appName, const QString &filePath);

    void sendAnalyticsEvent(const QString &categoryName, const QString &eventName);

    void requestReviewDialog();

signals:
    void shareCompleted();
    void pictureAddedToGallery();

private:
    explicit AiosHelper(QObject *parent = 0);
    AiosHelper(const AiosHelper &);
    AiosHelper& operator=(const AiosHelper &);

    int convertQtOrientationToIosMask(Qt::ScreenOrientation orientation);
    int convertQtOrientationToIos(Qt::ScreenOrientation orientation);

    static AiosHelper* m_instance;
};

#endif // AIOSHELPER_H
