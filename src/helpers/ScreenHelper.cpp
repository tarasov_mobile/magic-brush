#include "ScreenHelper.h"

#ifdef Q_OS_ANDROID
#include "AndroidHelper.h"
#elif defined(Q_OS_IOS)
#include "AiosHelper.h"
#endif

ScreenHelper::ScreenHelper(QObject *parent)
    : QObject(parent)
{

}

ScreenHelper::~ScreenHelper()
{

}

void ScreenHelper::requestScreenOrientation(Qt::ScreenOrientation orientation)
{
#ifdef Q_OS_ANDROID
    AndroidHelper::instance()->requestScreenOrientation(orientation);
#elif defined(Q_OS_IOS)
    AiosHelper::instance()->requestScreenOrientation(orientation);
#endif
}

void ScreenHelper::enableScreenOrientationChange()
{
#ifdef Q_OS_ANDROID
    AndroidHelper::instance()->enableScreenOrientationChange();
#elif defined(Q_OS_IOS)
    AiosHelper::instance()->enableScreenOrientationChange();
#endif
}

void ScreenHelper::disableScreenOrientationChange()
{
#ifdef Q_OS_ANDROID
    AndroidHelper::instance()->disableScreenOrientationChange();
#elif defined(Q_OS_IOS)
    AiosHelper::instance()->disableScreenOrientationChange();
#endif
}
