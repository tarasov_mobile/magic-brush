#ifndef SCREENHELPER_H
#define SCREENHELPER_H

#include <QObject>

class ScreenHelper : public QObject
{
    Q_OBJECT
public:
    explicit ScreenHelper(QObject *parent = 0);
    ~ScreenHelper();

public slots:
    void requestScreenOrientation(Qt::ScreenOrientation orientation);
    void enableScreenOrientationChange();
    void disableScreenOrientationChange();
};

#endif // SCREENHELPER_H
