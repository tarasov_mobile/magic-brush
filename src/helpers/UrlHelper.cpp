#include "UrlHelper.h"

#include <QDesktopServices>
#include <QUrl>

#ifdef Q_OS_ANDROID
#include "AndroidHelper.h"
#endif

namespace {
const QString INSTAGRAM_USER_WEB_LINK = "https://www.instagram.com/_u/%1";
const QString INSTAGRAM_TAG_WEB_LINK = "https://www.instagram.com/explore/tags/%1";

#ifdef Q_OS_IOS
const QString INSTAGRAM_USER_IOS_LINK = "instagram://user?username=%1";
const QString INSTAGRAM_TAG_IOS_LINK = "instagram://tag?name=%1";
#endif
}

UrlHelper::UrlHelper(QObject *parent)
    : QObject(parent)
{

}

UrlHelper::~UrlHelper()
{

}

void UrlHelper::openInstagramUserPage(const QString &userId)
{
#ifdef Q_OS_IOS
    if (openInstagramPage(INSTAGRAM_USER_IOS_LINK.arg(userId)))
        return;
#endif

    openInstagramPage(INSTAGRAM_USER_WEB_LINK.arg(userId));
}

void UrlHelper::openInstagramTagPage(const QString &tagName)
{
#ifdef Q_OS_IOS
    if (openInstagramPage(INSTAGRAM_TAG_IOS_LINK.arg(tagName)))
        return;
#endif

    openInstagramPage(INSTAGRAM_TAG_WEB_LINK.arg(tagName));
}

bool UrlHelper::openInstagramPage(const QString &urlString)
{
#ifdef Q_OS_ANDROID
    if (AndroidHelper::instance()->openInstagramApp(urlString))
        return true;
#endif

    return QDesktopServices::openUrl(urlString);
}
