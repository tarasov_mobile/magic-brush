#include "ImageHelper.h"

#include <QImage>

ImageHelper::ImageHelper(QObject *parent)
    : QObject(parent)
{

}

ImageHelper::~ImageHelper()
{

}

Qt::ScreenOrientation ImageHelper::getImageOrientation(const QUrl &filePath) const
{
    QImage image(filePath.toLocalFile());

    return image.width() > image.height() ? Qt::LandscapeOrientation : Qt::PortraitOrientation;
}
