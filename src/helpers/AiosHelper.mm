#include "AiosHelper.h"
#include <QDesktopServices>
#include <QGuiApplication>
#include <QScreen>

#import "UIKit/UIApplication.h"
#import <StoreKit/StoreKit.h>
#import "../../ios/src/AppDelegate.h"

#define APP_DELEGATE ((QIOSApplicationDelegate*)[UIApplication sharedApplication].delegate)

AiosHelper* AiosHelper::m_instance = nullptr;

AiosHelper::~AiosHelper()
{

}

AiosHelper *AiosHelper::instance()
{
    if (!m_instance)
        m_instance = new AiosHelper;

    return m_instance;
}

QString AiosHelper::getCurrentAppVersion() const
{
    NSString *version = [APP_DELEGATE getCurrentAppVersion];
    return QString::fromNSString(version);
}

bool AiosHelper::isAppleReviewDialogSupported() const
{
    return [[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){.majorVersion = 10, .minorVersion = 3, .patchVersion = 0}];
}

bool AiosHelper::isCurrentDeviceIsIPhoneX() const
{
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        if ((int)[[UIScreen mainScreen] nativeBounds].size.height == 2436)
            return true;
    }

    return false;
}

void AiosHelper::requestScreenOrientation(Qt::ScreenOrientation orientation)
{
    ScreenOrientationLocker::ALLOWED_SCREEN_ORIENTATION = convertQtOrientationToIosMask(orientation);

    //rotate if current device orientation is not the same as 'orientation'
    [APP_DELEGATE forceActivateOrientation:convertQtOrientationToIos(orientation)];
}

void AiosHelper::enableScreenOrientationChange()
{
    ScreenOrientationLocker::ALLOWED_SCREEN_ORIENTATION = UIInterfaceOrientationMaskAll;
}

void AiosHelper::disableScreenOrientationChange()
{
    ScreenOrientationLocker::ALLOWED_SCREEN_ORIENTATION = convertQtOrientationToIosMask(qApp->primaryScreen()->orientation());
}

bool AiosHelper::isTablet() const
{
    return [APP_DELEGATE isTablet];
}

AiosHelper::AiosHelper(QObject *parent)
    : QObject(parent)
{

}

int AiosHelper::convertQtOrientationToIosMask(Qt::ScreenOrientation orientation)
{
    switch (orientation) {
    case Qt::PortraitOrientation:
        return UIInterfaceOrientationMaskPortrait;

    case Qt::InvertedPortraitOrientation:
        return UIInterfaceOrientationMaskPortraitUpsideDown;

    case Qt::LandscapeOrientation:
        return UIInterfaceOrientationMaskLandscapeRight;

    case Qt::InvertedLandscapeOrientation:
        return UIInterfaceOrientationMaskLandscapeLeft;

    default:
        return UIInterfaceOrientationMaskAll;
    }
}

int AiosHelper::convertQtOrientationToIos(Qt::ScreenOrientation orientation)
{
    switch (orientation) {
    case Qt::PortraitOrientation:
        return UIInterfaceOrientationPortrait;

    case Qt::InvertedPortraitOrientation:
        return UIInterfaceOrientationPortraitUpsideDown;

    case Qt::LandscapeOrientation:
        return UIInterfaceOrientationLandscapeRight;

    case Qt::InvertedLandscapeOrientation:
        return UIInterfaceOrientationLandscapeLeft;

    default:
        return UIInterfaceOrientationUnknown;
    }
}

void AiosHelper::shareImage(const QString &appName, const QString &filePath)
{
    if (appName == "facebook")
        [APP_DELEGATE shareToFacebook: filePath.toNSString() completion:^() {
            emit shareCompleted();
        }];
    else if (appName == "instagram")
        [APP_DELEGATE shareToInstagram: filePath.toNSString()];
    else if (appName == "twitter")
        [APP_DELEGATE shareToTwitter: filePath.toNSString() completion:^() {
            emit shareCompleted();
        }];
    else if (appName == "photo_library")
        [APP_DELEGATE saveToPhotoLibrary: filePath.toNSString() completion:^() {
            emit pictureAddedToGallery();
        }];
    else
        [APP_DELEGATE showShareDialog: filePath.toNSString() completion:^() {
            emit shareCompleted();
        }];
}

void AiosHelper::sendAnalyticsEvent(const QString &categoryName, const QString &eventName)
{
    [APP_DELEGATE sendAnalyticsEventWithCategory:categoryName.toNSString() andAction:eventName.toNSString()];
}

void AiosHelper::requestReviewDialog()
{
    [SKStoreReviewController requestReview];
}
