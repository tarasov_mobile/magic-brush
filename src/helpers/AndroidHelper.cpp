#include "AndroidHelper.h"

#include <QGuiApplication>
#include <QScreen>
#include <QtAndroid>

AndroidHelper* AndroidHelper::m_instance = nullptr;

AndroidHelper::AndroidHelper(QObject *parent)
    : QObject(parent)
{
    m_shareAppsHash["facebook"] = "com.facebook.katana";
    m_shareAppsHash["vk"] = "com.vkontakte.android";
    m_shareAppsHash["instagram"] = "com.instagram.android";
    m_shareAppsHash["pinterest"] = "com.pinterest";
}

AndroidHelper::~AndroidHelper()
{

}

AndroidHelper *AndroidHelper::instance()
{
    if (!m_instance)
        m_instance = new AndroidHelper;

    return m_instance;
}

bool AndroidHelper::openInstagramApp(const QString &url)
{
    return QtAndroid::androidActivity().callMethod<jboolean>("openInstagramApp", "(Ljava/lang/String;)Z",
                                                  QAndroidJniObject::fromString(url).object<jstring>());
}

void AndroidHelper::sendAnalyticsEvent(const QString &categoryName, const QString &eventName)
{
    QtAndroid::androidActivity().callMethod<void>("sendAnalyticsEvent", "(Ljava/lang/String;Ljava/lang/String;)V",
                                                  QAndroidJniObject::fromString(categoryName).object<jstring>(),
                                                  QAndroidJniObject::fromString(eventName).object<jstring>());
}

void AndroidHelper::updateImageInGallery(const QUrl &filePath)
{
    QtAndroid::androidActivity().callMethod<void>("updateImageInGallery", "(Ljava/lang/String;)V",
                                                  QAndroidJniObject::fromString(filePath.toLocalFile()).object<jstring>());

    emit pictureAddedToGallery();
}

void AndroidHelper::showToast(const QString &messageText)
{
    QtAndroid::runOnAndroidThread([messageText] {
        QAndroidJniObject javaString = QAndroidJniObject::fromString(messageText);
        QAndroidJniObject toast = QAndroidJniObject::callStaticObjectMethod("android/widget/Toast", "makeText",
                                                                            "(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;",
                                                                            QtAndroid::androidActivity().object(),
                                                                            javaString.object(),
                                                                            jint(1));
        toast.callMethod<void>("show");
    });
}

void AndroidHelper::shareImage(const QString &appName, const QString &filePath)
{
    QString appId = m_shareAppsHash.value(appName);

    QtAndroid::runOnAndroidThread([this, appId, filePath] {
        QtAndroid::androidActivity().callMethod<void>("shareImage",
                                                      "(Ljava/lang/String;Ljava/lang/String;)V",
                                                      QAndroidJniObject::fromString(appId).object<jstring>(),
                                                      QAndroidJniObject::fromString(filePath).object<jstring>());
    });
}

bool AndroidHelper::isTablet() const
{
    return QtAndroid::androidActivity().callMethod<jboolean>("isTablet");
}

void AndroidHelper::requestScreenOrientation(Qt::ScreenOrientation orientation)
{
    //see ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    //and ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    int androidOrientation = (orientation == Qt::LandscapeOrientation ? 0x00000000 : 0x00000001);
    QtAndroid::androidActivity().callMethod<void>("requestScreenOrientation", "(I)V", androidOrientation);
}

void AndroidHelper::enableScreenOrientationChange()
{
    QtAndroid::androidActivity().callMethod<void>("enableScreenOrientationChange");
}

void AndroidHelper::disableScreenOrientationChange()
{
    QtAndroid::androidActivity().callMethod<void>("disableScreenOrientationChange");
}

void AndroidHelper::hideSplashScreen()
{
    QtAndroid::hideSplashScreen();
}

QString AndroidHelper::getCurrentScreenDensity() const
{
    qreal ratio = qApp->primaryScreen()->devicePixelRatio();

    QString prefix;
    if (isTablet()) {
        QSize screenSize = qApp->primaryScreen()->size();
        if (screenSize.width() >= 720 && screenSize.width() >= 720)
            prefix = "sw720dp-%1";
        else
            prefix = "sw600dp-%1";
    } else {
        prefix = "%1";
    }

    if (ratio == 1.5)
        return prefix.arg("hdpi");
    else if (ratio == 2.0)
        return prefix.arg("xhdpi");
    else if (ratio == 3.0)
        return prefix.arg("xxhdpi");
    else if (ratio == 4.0)
        return prefix.arg("xxxhdpi");

    return prefix.arg("mdpi");
}

QString AndroidHelper::getCurrentAppVersion() const
{
    return QtAndroid::androidActivity().callObjectMethod("getCurrentAppVersion", "()Ljava/lang/String;").toString();
}
