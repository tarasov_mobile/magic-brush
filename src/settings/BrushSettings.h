#ifndef BRUSHSETTINGS_H
#define BRUSHSETTINGS_H

#include <QColor>

class PathSettings
{
public:
    PathSettings(int brushWidth, const QColor &brushColor, Qt::ScreenOrientation pathOrientation);
    ~PathSettings();

    void setBrushColor(const QColor &color);
    QColor brushColor() const;

    void setBrushWidth(int width);
    int brushWidth() const;

    void setPathOrientation(Qt::ScreenOrientation orientation);
    Qt::ScreenOrientation pathOrientation() const;

private:
    QColor m_brushColor;
    int m_brushWidth;
    Qt::ScreenOrientation m_pathOrientation;
};

#endif // BRUSHSETTINGS_H
