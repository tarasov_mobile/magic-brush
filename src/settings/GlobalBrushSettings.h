#ifndef GLOBALBRUSHSETTINGS_H
#define GLOBALBRUSHSETTINGS_H

#include <QObject>
#include <QColor>

class GlobalBrushSettingsPrivate;

class GlobalBrushSettings : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QColor brushColor READ brushColor WRITE setBrushColor RESET unsetBrushColor NOTIFY brushColorChanged)

    Q_PROPERTY(double minBrushWidth READ minBrushWidth WRITE setMinBrushWidth NOTIFY minBrushWidthChanged)
    Q_PROPERTY(double maxBrushWidth READ maxBrushWidth WRITE setMaxBrushWidth NOTIFY maxBrushWidthChanged)
    Q_PROPERTY(double brushWidth READ brushWidth WRITE setBrushWidth NOTIFY brushWidthChanged)

    Q_PROPERTY(double minBrushTransparency READ minBrushTransparency WRITE setMinBrushTransparency NOTIFY minBrushTransparencyChanged)
    Q_PROPERTY(double maxBrushTransparency READ maxBrushTransparency WRITE setMaxBrushTransparency NOTIFY maxBrushTransparencyChanged)
    Q_PROPERTY(double brushTransparency READ brushTransparency WRITE setBrushTransparency NOTIFY brushTransparencyChanged)
public:
    ~GlobalBrushSettings();
    static GlobalBrushSettings* instance();

    QColor brushColor() const;
    double brushWidth() const;
    double brushTransparency() const;
    double minBrushWidth() const;
    double maxBrushWidth() const;
    double minBrushTransparency() const;
    double maxBrushTransparency() const;

public slots:
    void setBrushColor(const QColor &brushColor);
    void unsetBrushColor();
    void setBrushWidth(double brushWidth);
    void setBrushTransparency(double brushTransparency);
    void setMinBrushWidth(double minBrushWidth);
    void setMaxBrushWidth(double maxBrushWidth);
    void setMinBrushTransparency(double minBrushTransparency);
    void setMaxBrushTransparency(double maxBrushTransparency);

signals:
    void brushColorChanged(const QColor &brushColor);
    void brushWidthChanged(double brushWidth);
    void brushTransparencyChanged(double brushTransparency);
    void minBrushWidthChanged(double minBrushWidth);
    void maxBrushWidthChanged(double maxBrushWidth);
    void minBrushTransparencyChanged(double minBrushTransparency);
    void maxBrushTransparencyChanged(double maxBrushTransparency);

private:
    explicit GlobalBrushSettings(QObject *parent = 0);
    GlobalBrushSettings(const GlobalBrushSettings &)    ;
    GlobalBrushSettings& operator=(const GlobalBrushSettings &);

    static GlobalBrushSettings* m_instance;
    GlobalBrushSettingsPrivate *d;
};

#endif // GLOBALBRUSHSETTINGS_H
