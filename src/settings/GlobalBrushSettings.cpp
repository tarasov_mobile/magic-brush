#include "GlobalBrushSettings.h"

namespace {
const QColor DEFAULT_BRUSH_COLOR = QColor("#6417ba");
}

class GlobalBrushSettingsPrivate
{
public:
    GlobalBrushSettingsPrivate()
    {
        brushColor = DEFAULT_BRUSH_COLOR;
        brushWidth = 1.0;
        brushTransparency = 0.5;
        minBrushWidth = 0.25;
        maxBrushWidth = 1.0;
        minBrushTransparency = 0.02;
        maxBrushTransparency = 0.5;
    }

    ~GlobalBrushSettingsPrivate()
    {

    }

    QColor brushColor;
    double brushWidth;
    double brushTransparency;
    double minBrushWidth;
    double maxBrushWidth;
    double minBrushTransparency;
    double maxBrushTransparency;
};

GlobalBrushSettings* GlobalBrushSettings::m_instance = nullptr;

GlobalBrushSettings::GlobalBrushSettings(QObject *parent)
    : QObject(parent)
    , d(new GlobalBrushSettingsPrivate)
{

}

GlobalBrushSettings::~GlobalBrushSettings()
{
    delete d;
}

GlobalBrushSettings *GlobalBrushSettings::instance()
{
    if (!m_instance)
        m_instance = new GlobalBrushSettings;

    return m_instance;
}

QColor GlobalBrushSettings::brushColor() const
{
    return d->brushColor;
}

double GlobalBrushSettings::brushWidth() const
{
    return d->brushWidth;
}

double GlobalBrushSettings::brushTransparency() const
{
    return d->brushTransparency;
}

double GlobalBrushSettings::minBrushWidth() const
{
    return d->minBrushWidth;
}

double GlobalBrushSettings::maxBrushWidth() const
{
    return d->maxBrushWidth;
}

double GlobalBrushSettings::minBrushTransparency() const
{
    return d->minBrushTransparency;
}

double GlobalBrushSettings::maxBrushTransparency() const
{
    return d->maxBrushTransparency;
}

void GlobalBrushSettings::setBrushColor(const QColor &brushColor)
{
    if (d->brushColor == brushColor)
        return;

    d->brushColor = brushColor;
    emit brushColorChanged(brushColor);
}

void GlobalBrushSettings::unsetBrushColor()
{
    d->brushColor = DEFAULT_BRUSH_COLOR;

    emit brushColorChanged(DEFAULT_BRUSH_COLOR);
}

void GlobalBrushSettings::setBrushWidth(double brushWidth)
{
    if (d->brushWidth == brushWidth)
        return;

    d->brushWidth = brushWidth;
    emit brushWidthChanged(brushWidth);
}

void GlobalBrushSettings::setBrushTransparency(double brushTransparency)
{
    if (d->brushTransparency == brushTransparency)
        return;

    d->brushTransparency = brushTransparency;
    emit brushTransparencyChanged(brushTransparency);
}

void GlobalBrushSettings::setMinBrushWidth(double minBrushWidth)
{
    if (d->minBrushWidth == minBrushWidth)
        return;

    d->minBrushWidth = minBrushWidth;
    emit minBrushWidthChanged(minBrushWidth);
}

void GlobalBrushSettings::setMaxBrushWidth(double maxBrushWidth)
{
    if (d->maxBrushWidth == maxBrushWidth)
        return;

    d->maxBrushWidth = maxBrushWidth;
    emit maxBrushWidthChanged(maxBrushWidth);
}

void GlobalBrushSettings::setMinBrushTransparency(double minBrushTransparency)
{
    if (d->minBrushTransparency == minBrushTransparency)
        return;

    d->minBrushTransparency = minBrushTransparency;
    emit minBrushTransparencyChanged(minBrushTransparency);
}

void GlobalBrushSettings::setMaxBrushTransparency(double maxBrushTransparency)
{
    if (d->maxBrushTransparency == maxBrushTransparency)
        return;

    d->maxBrushTransparency = maxBrushTransparency;
    emit maxBrushTransparencyChanged(maxBrushTransparency);
}
