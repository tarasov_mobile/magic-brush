#include "BrushSettings.h"

PathSettings::PathSettings(int brushWidth, const QColor &brushColor, Qt::ScreenOrientation pathOrientation)
{
    m_brushWidth = brushWidth;
    m_brushColor = brushColor;
    m_pathOrientation = pathOrientation;
}

PathSettings::~PathSettings()
{

}

void PathSettings::setBrushColor(const QColor &color)
{
    m_brushColor = color;
}

QColor PathSettings::brushColor() const
{
    return m_brushColor;
}

void PathSettings::setBrushWidth(int width)
{
    m_brushWidth = width;
}

int PathSettings::brushWidth() const
{
    return m_brushWidth;
}

void PathSettings::setPathOrientation(Qt::ScreenOrientation orientation)
{
    m_pathOrientation = orientation;
}

Qt::ScreenOrientation PathSettings::pathOrientation() const
{
    return m_pathOrientation;
}
