#ifndef APPSETTINGS_H
#define APPSETTINGS_H

#include <QObject>
#include <QVariantList>
#include <QColor>

class AppSettingsPrivate;

class AppSettings : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariantList brushColorList READ brushColorList WRITE setBrushColorList NOTIFY brushColorListChanged)
    Q_PROPERTY(QVariantList baseColorList READ baseColorList WRITE setBaseColorList NOTIFY baseColorListChanged)

    Q_PROPERTY(bool showManualAtStart READ showManualAtStart WRITE setShowManualAtStart NOTIFY showManualAtStartChanged)

public:
    ~AppSettings();
    static AppSettings* instance();

    QVariantList brushColorList() const;
    QVariantList baseColorList() const;

    Q_INVOKABLE void setCustomProperty(const QString &name, const QVariant &value);
    Q_INVOKABLE QVariant getCustomProperty(const QString &name) const;

    bool showManualAtStart() const;

public slots:
    void setBrushColorList(const QVariantList &brushColorList);
    void setBaseColorList(const QVariantList &baseColorList);

    void setShowManualAtStart(bool show);

signals:
    void brushColorListChanged(const QVariantList &brushColorList);
    void baseColorListChanged(const QVariantList &baseColorList);
    void showManualAtStartChanged(bool show);

private:
    explicit AppSettings(QObject *parent = 0);
    AppSettings(const AppSettings &)    ;
    AppSettings& operator=(const AppSettings &);

    static AppSettings* m_instance;
    AppSettingsPrivate *d;
};

#endif // APPSETTINGS_H
