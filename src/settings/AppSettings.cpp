#include "AppSettings.h"

#include <QSettings>
#include <QColor>

class AppSettingsPrivate
{
public:
    AppSettingsPrivate()
    {
        settings = new QSettings;

        defaultBrushColorList << QColor("#000000") << QColor("#4a4a4a")
                         << QColor("#c0c0c0") << QColor("#a40800")
                         << QColor("#db1608") << QColor("#ee5509")
                         << QColor("#f19b1a") << QColor("#f7c217")
                         << QColor("#f2e751") << QColor("#cfdc17")
                         << QColor("#afd245") << QColor("#509b34")
                         << QColor("#09620d") << QColor("#97caca")
                         << QColor("#ffffff") << QColor("#808080")
                         << QColor("#f2eee7") << QColor("#78b2d9")
                         << QColor("#3b8ccf") << QColor("#2e5cbf")
                         << QColor("#0f3290") << QColor("#6417ba")
                         << QColor("#a848eb") << QColor("#c16ae7")
                         << QColor("#b14297") << QColor("#e05e85")
                         << QColor("#eb7e9b") << QColor("#ffc5d1");

        defaultBaseColorList << QColor("#000000") << QColor("#B7B7B7")
                         << QColor("#E6E6E6") << QColor("#DB9D99")
                         << QColor("#F1A29D") << QColor("#F9BB9D")
                         << QColor("#FAD7A4") << QColor("#FCE7A3")
                         << QColor("#FAF6BA") << QColor("#ECF1A3")
                         << QColor("#DFEDB5") << QColor("#B9D7AE")
                         << QColor("#9DC19F") << QColor("#D6EAEA")
                         << QColor("#ffffff") << QColor("#CDCDCD")
                         << QColor("#FAF9F6") << QColor("#C9E1F0")
                         << QColor("#B1D1EC") << QColor("#ACBEE6")
                         << QColor("#9FADD3") << QColor("#C1A3E4")
                         << QColor("#DDB6F7") << QColor("#E7C4F6")
                         << QColor("#E0B4D6") << QColor("#F3BFCF")
                         << QColor("#F7CCD7") << QColor("#FFE8ED");

        colorsByNameHash["brush_colors"] = defaultBrushColorList;
        colorsByNameHash["base_colors"] = defaultBaseColorList;
    }

    ~AppSettingsPrivate()
    {
        delete settings;
    }

    void setColorList(const QString &arrayName, const QVariantList &colorList)
    {
        settings->beginWriteArray(arrayName);
        for (int i = 0; i < colorList.size(); ++i) {
            settings->setArrayIndex(i);
            settings->setValue("color", colorList.at(i));
        }
        settings->endArray();
        settings->sync();
    }

    QVariantList getColorList(const QString &arrayName)
    {
        QVariantList colors;
        int size = settings->beginReadArray(arrayName);

        for (int i = 0; i < size; ++i) {
            settings->setArrayIndex(i);
            colors << settings->value("color");
        }
        settings->endArray();

        return size ? colors : colorsByNameHash[arrayName];
    }

    QSettings *settings;
    QVariantList defaultBrushColorList;
    QVariantList defaultBaseColorList;
    QHash<QString, QVariantList> colorsByNameHash;
};

AppSettings* AppSettings::m_instance = nullptr;

AppSettings::~AppSettings()
{
    delete d;
}

AppSettings *AppSettings::instance()
{
    if (!m_instance)
        m_instance = new AppSettings;

    return m_instance;
}

QVariantList AppSettings::brushColorList() const
{
    return d->getColorList("brush_colors");
}

QVariantList AppSettings::baseColorList() const
{
    return d->getColorList("base_colors");
}

void AppSettings::setCustomProperty(const QString &name, const QVariant &value)
{
    d->settings->setValue(name, value);
}

QVariant AppSettings::getCustomProperty(const QString &name) const
{
    return d->settings->value(name);
}

bool AppSettings::showManualAtStart() const
{
    return d->settings->value("show_manual_at_start", true).toBool();
}

void AppSettings::setBrushColorList(const QVariantList &brushColorList)
{
    if (!brushColorList.size())
        return;

    d->setColorList("brush_colors", brushColorList);

    emit brushColorListChanged(brushColorList);
}

void AppSettings::setBaseColorList(const QVariantList &baseColorList)
{
    if (!baseColorList.size())
        return;

    d->setColorList("base_colors", baseColorList);

    emit baseColorListChanged(baseColorList);
}

void AppSettings::setShowManualAtStart(bool show)
{
    if (showManualAtStart() == show)
        return;

    d->settings->setValue("show_manual_at_start", show);
    emit showManualAtStartChanged(show);
}

AppSettings::AppSettings(QObject *parent)
    : QObject(parent)
    , d(new AppSettingsPrivate)
{

}
