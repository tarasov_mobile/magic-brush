#ifndef SIMPLEBRUSH_H
#define SIMPLEBRUSH_H

#include "AbstractBrush.h"

class SimpleBrush : public AbstractBrush
{
public:
    explicit SimpleBrush(QObject *parent = 0);
    ~SimpleBrush();

    virtual double defaultWidth() const override;
    virtual double minWidth() const override;
    virtual double maxWidth() const override;

    virtual double defaultTransparency() const override;
    virtual double minTransparency() const override;
    virtual double maxTransparency() const override;

protected:
    DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) override;
};

#endif // SIMPLEBRUSH_H
