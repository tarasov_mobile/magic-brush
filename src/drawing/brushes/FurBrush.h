#ifndef FURBRUSH_H
#define FURBRUSH_H

#include "AbstractBrush.h"

class FurBrush : public AbstractBrush
{
public:
    explicit FurBrush(QObject *parent = 0);
    ~FurBrush();

    double defaultWidth() const override;
    double defaultTransparency() const override;

protected:
    DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) override;
};

#endif // FURBRUSH_H
