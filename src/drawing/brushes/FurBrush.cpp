#include "FurBrush.h"

#include <QDateTime>

FurBrush::FurBrush(QObject *parent)
    : AbstractBrush("fur", parent)
{
    qsrand(QDateTime::currentDateTime().toTime_t());
}

FurBrush::~FurBrush()
{

}

double FurBrush::defaultWidth() const
{
    return 1.8;
}

double FurBrush::defaultTransparency() const
{
    return 0.3;
}

DrawPathList FurBrush::calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points)
{
    PathSettings settings = globalPathSettings();

    DrawPathList paths;
    QPoint lastPoint = points.last();

    QPainterPath path;
    path.moveTo(prevPoint.x(), prevPoint.y());
    path.lineTo(lastPoint);
    paths << DrawPath(path, settings);

    double dx, dy, d;
    for (int i = 0; i < points.count(); ++i) {
        dx = points.at(i).x() - lastPoint.x();
        dy = points.at(i).y() - lastPoint.y();
        d = dx * dx + dy * dy;

        if (d < 2000 && RAND_BETWEEN_ZERO_AND_ONE > d / 2000) {
            QPainterPath path;
            path.moveTo(lastPoint.x() + (dx * 0.5), lastPoint.y() + (dy * 0.5));
            path.lineTo(lastPoint.x() - (dx * 0.5), lastPoint.y() - (dy * 0.5));
            paths << DrawPath(path, settings);
        }
    }

    return paths;
}
