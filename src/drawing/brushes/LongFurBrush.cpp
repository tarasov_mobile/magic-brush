#include "LongFurBrush.h"

#include <QDateTime>

LongFurBrush::LongFurBrush(QObject *parent)
    : AbstractBrush("longfur", parent)
{
    qsrand(QDateTime::currentDateTime().toTime_t());
}

LongFurBrush::~LongFurBrush()
{

}

double LongFurBrush::defaultWidth() const
{
    return 1.8;
}

double LongFurBrush::defaultTransparency() const
{
    return 0.2;
}

DrawPathList LongFurBrush::calculatePaths(const QPoint &, const QList<QPoint> &points)
{
    PathSettings settings = globalPathSettings();

    DrawPathList paths;

    double size = 0.0;
    double dx, dy, d;
    QPoint lastPoint = points.last();
    for (int i = 0; i < points.count(); ++i)
    {
        size = -RAND_BETWEEN_ZERO_AND_ONE;
        dx = points.at(i).x() - lastPoint.x();
        dy = points.at(i).y() - lastPoint.y();
        d = dx * dx + dy * dy;

        if (d < 4000 && RAND_BETWEEN_ZERO_AND_ONE > d / 4000)
        {
            //TODO: apply line width
            QPainterPath path;
            path.moveTo(lastPoint.x() + (dx * size), lastPoint.y() + (dy * size));
            path.lineTo(points.at(i).x() - (dx * size) + RAND_BETWEEN_ZERO_AND_ONE * 2,
                        points.at(i).y() - (dy * size) + RAND_BETWEEN_ZERO_AND_ONE * 2);
            paths << DrawPath(path, settings);
        }
    }

    return paths;
}
