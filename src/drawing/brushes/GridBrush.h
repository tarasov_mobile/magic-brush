#ifndef GRIDBRUSH_H
#define GRIDBRUSH_H

#include "AbstractBrush.h"

class GridBrush : public AbstractBrush
{
public:
    explicit GridBrush(QObject *parent = 0);
    ~GridBrush();

    double defaultTransparency() const override;

protected:
    DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) override;
};

#endif // GRIDBRUSH_H
