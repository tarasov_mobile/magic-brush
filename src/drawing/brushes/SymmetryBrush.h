#ifndef SYMMETRYBRUSH_H
#define SYMMETRYBRUSH_H

#include "AbstractProxyBrush.h"

class SymmetryBrush : public AbstractProxyBrush
{
public:
    explicit SymmetryBrush(QObject *parent = 0);
    ~SymmetryBrush();

    QString name() const override;

    double defaultWidth() const override;
    double defaultTransparency() const override;

protected:
    DrawPathList handleSourcePaths(const DrawPathList &paths) override;
};

#endif // SYMMETRYBRUSH_H
