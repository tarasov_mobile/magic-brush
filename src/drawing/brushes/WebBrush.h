#ifndef WEBBRUSH_H
#define WEBBRUSH_H

#include "AbstractBrush.h"

class WebBrush : public AbstractBrush
{
public:
    explicit WebBrush(QObject *parent = 0);
    ~WebBrush();

    double defaultWidth() const override;
    double defaultTransparency() const override;

protected:
    DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) override;
};

#endif // WEBBRUSH_H
