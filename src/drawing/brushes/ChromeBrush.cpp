#include "ChromeBrush.h"
#include <QDateTime>
#include <QtMath>

ChromeBrush::ChromeBrush(QObject *parent)
    : AbstractBrush("chrome", parent)
{
    qsrand(QDateTime::currentDateTime().toTime_t());
}

ChromeBrush::~ChromeBrush()
{

}

double ChromeBrush::defaultWidth() const
{
    return 1.8;
}

double ChromeBrush::defaultTransparency() const
{
    return 0.2;
}

DrawPathList ChromeBrush::calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points)
{
    PathSettings settings = globalPathSettings();

    DrawPathList paths;
    QPoint lastPoint = points.last();

    QPainterPath path;
    path.moveTo(prevPoint.x(), prevPoint.y());
    path.lineTo(lastPoint);
    paths << DrawPath(path, settings);

    double dx, dy, d;
    for (int i = 0; i < points.count(); ++i) {
        dx = points.at(i).x() - lastPoint.x();
        dy = points.at(i).y() - lastPoint.y();
        d = dx * dx + dy * dy;

        if (d < 1000) {
            QPainterPath path;
            path.moveTo(lastPoint.x() + (dx * 0.2), lastPoint.y() + (dy * 0.2));
            path.lineTo(points.at(i).x() - (dx * 0.2), points.at(i).y() - (dy * 0.2));

            QColor userColor = settings.brushColor();
            int newRed = qFloor(RAND_BETWEEN_ZERO_AND_ONE * qRed(userColor.rgba()));
            int newGreen = qFloor(RAND_BETWEEN_ZERO_AND_ONE * qGreen(userColor.rgba()));
            int newBlue = qFloor(RAND_BETWEEN_ZERO_AND_ONE * qBlue(userColor.rgba()));
            paths << DrawPath(path, PathSettings(settings.brushWidth(), QColor(newRed, newGreen, newBlue, userColor.alpha()), settings.pathOrientation()));
        }
    }

    return paths;
}
