#include "SymmetryBrush.h"

#include <QTransform>

SymmetryBrush::SymmetryBrush(QObject *parent)
    : AbstractProxyBrush("symmetry", parent)
{

}

SymmetryBrush::~SymmetryBrush()
{

}

QString SymmetryBrush::name() const
{
    return QString("%1_%2").arg(m_brushName).arg(m_sourceBrush ? m_sourceBrush->name() : QString());
}

double SymmetryBrush::defaultWidth() const
{
    return 1.8;
}

double SymmetryBrush::defaultTransparency() const
{
    return 0.2;
}

DrawPathList SymmetryBrush::handleSourcePaths(const DrawPathList &paths)
{
    DrawPathList newPathList;
    foreach (const DrawPath &drawPath, paths) {
        QPainterPath path = drawPath.first;
        path = path.translated(QPoint(-m_initialPoint.x(), -m_initialPoint.y()));

        QTransform transform;
        transform.translate(m_initialPoint.x(), m_initialPoint.y());

        transform.rotate(-72);
        newPathList << DrawPath(transform.map(path), drawPath.second);

        transform.rotate(-72);
        newPathList << DrawPath(transform.map(path), drawPath.second);

        transform.rotate(-72);
        newPathList << DrawPath(transform.map(path), drawPath.second);

        transform.rotate(-72);
        newPathList << DrawPath(transform.map(path), drawPath.second);
    }

    return paths + newPathList;
}
