#ifndef CHROMEBRUSH_H
#define CHROMEBRUSH_H

#include "AbstractBrush.h"

class ChromeBrush : public AbstractBrush
{
public:
    explicit ChromeBrush(QObject *parent = 0);
    ~ChromeBrush();

    double defaultWidth() const override;
    double defaultTransparency() const override;

protected:
    DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) override;
};

#endif // CHROMEBRUSH_H
