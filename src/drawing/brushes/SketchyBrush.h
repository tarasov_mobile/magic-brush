#ifndef SKETCHYBRUSH_H
#define SKETCHYBRUSH_H

#include "AbstractBrush.h"

class SketchyBrush : public AbstractBrush
{
public:
    explicit SketchyBrush(QObject *parent = 0);
    ~SketchyBrush();

    double defaultWidth() const override;
    double defaultTransparency() const override;

protected:
    DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) override;
};

#endif // SKETCHYBRUSH_H
