#ifndef ABSTRACTPROXYBRUSH_H
#define ABSTRACTPROXYBRUSH_H

#include "AbstractBrush.h"

class AbstractProxyBrush : public AbstractBrush
{
public:
    explicit AbstractProxyBrush(const QString &brushName, QObject *parent = 0);
    ~AbstractProxyBrush();

    QString name() const override;
    void setSourceBrush(AbstractBrush *brush);

    void init(const QPoint &initialPoint) override;

protected:
    DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) override;
    virtual DrawPathList handleSourcePaths(const DrawPathList &paths) = 0;

    AbstractBrush *m_sourceBrush;
};

#endif // ABSTRACTPROXYBRUSH_H
