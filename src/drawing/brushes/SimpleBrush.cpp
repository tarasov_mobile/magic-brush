#include "SimpleBrush.h"

SimpleBrush::SimpleBrush(QObject *parent)
    : AbstractBrush("simple", parent)
{

}

SimpleBrush::~SimpleBrush()
{

}

double SimpleBrush::defaultWidth() const
{
    return 2.0;
}

double SimpleBrush::minWidth() const
{
    return 0.25;
}

double SimpleBrush::maxWidth() const
{
    return 5.0;
}

double SimpleBrush::defaultTransparency() const
{
    return 0.5;
}

double SimpleBrush::minTransparency() const
{
    return 0.02;
}

double SimpleBrush::maxTransparency() const
{
    return 0.5;
}

DrawPathList SimpleBrush::calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points)
{
    PathSettings settings = globalPathSettings();

    DrawPathList paths;
    QPoint lastPoint = points.last();

    QPainterPath path;
    path.moveTo(prevPoint.x(), prevPoint.y());
    path.lineTo(lastPoint);
    paths << DrawPath(path, settings);

    return paths;
}
