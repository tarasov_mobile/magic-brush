#ifndef SHADEDBRUSH_H
#define SHADEDBRUSH_H

#include "AbstractBrush.h"

class ShadedBrush : public AbstractBrush
{
public:
    explicit ShadedBrush(QObject *parent = 0);
    ~ShadedBrush();

    double defaultWidth() const override;
    double defaultTransparency() const override;

protected:
    DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) override;
};

#endif // SHADEDBRUSH_H
