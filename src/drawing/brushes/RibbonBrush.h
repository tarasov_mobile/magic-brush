#ifndef RIBBONBRUSH_H
#define RIBBONBRUSH_H

#include "AbstractBrush.h"

class RibbonBrush : public AbstractBrush
{
public:
    explicit RibbonBrush(QObject *parent = 0);
    ~RibbonBrush();

    void init(const QPoint &initialPoint) override;

    double defaultTransparency() const override;

protected:
    DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) override;

private:
    QList<QVariantMap> m_painters;
};

#endif // RIBBONBRUSH_H
