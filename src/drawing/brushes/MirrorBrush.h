#ifndef MIRRORBRUSH_H
#define MIRRORBRUSH_H

#include "AbstractProxyBrush.h"

class MirrorBrush : public AbstractProxyBrush
{
public:
    explicit MirrorBrush(QObject *parent = 0);
    ~MirrorBrush();

    double defaultWidth() const override;
    double defaultTransparency() const override;

protected:
    DrawPathList handleSourcePaths(const DrawPathList &paths) override;
};

#endif // MIRRORBRUSH_H
