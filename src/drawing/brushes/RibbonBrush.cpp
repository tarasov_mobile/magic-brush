#include "RibbonBrush.h"

#include <QDateTime>

RibbonBrush::RibbonBrush(QObject *parent)
    : AbstractBrush("ribbon", parent)
{
    qsrand(QDateTime::currentDateTime().toTime_t());

    for (int i = 0; i < 35; ++i)
    {
        QVariantMap object;
        object["ax"] = 0;
        object["ay"] = 0;
        object["div"] = 0.15;
        object["ease"] = RAND_BETWEEN_ZERO_AND_ONE * 0.2 + 0.6;
        m_painters << object;
    }
}

RibbonBrush::~RibbonBrush()
{

}

void RibbonBrush::init(const QPoint &initialPoint)
{
    AbstractBrush::init(initialPoint);

    for (int i = 0; i < m_painters.count(); ++i)
    {
        QVariantMap object = m_painters.at(i);
        object["dx"] = initialPoint.x();
        object["dy"] = initialPoint.y();
        m_painters[i] = object;
    }
}

double RibbonBrush::defaultTransparency() const
{
    return 0.25;
}

DrawPathList RibbonBrush::calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points)
{
    Q_UNUSED(prevPoint)

    PathSettings settings = globalPathSettings();

    DrawPathList paths;
    QPoint lastPoint = points.last();

    for (int i = 0; i < m_painters.count(); ++i)
    {
        QPainterPath path;
        QVariantMap object = m_painters.at(i);
        int dx = object["dx"].toInt();
        int dy = object["dy"].toInt();
        double ax = object["ax"].toDouble();
        double ay = object["ay"].toDouble();
        double div = object["div"].toDouble();
        double ease = object["ease"].toDouble();

        path.moveTo(dx, dy);

        ax = (ax + (dx - lastPoint.x()) * div) * ease;
        object["ax"] = ax;
        dx -= ax;
        object["dx"] = dx;

        ay = (ay + (dy - lastPoint.y()) * div) * ease;
        object["ay"] = ay;
        dy -= ay;
        object["dy"] = dy;

        m_painters[i] = object;

        path.lineTo(dx, dy);

        paths << DrawPath(path, settings);
    }

    return paths;
}
