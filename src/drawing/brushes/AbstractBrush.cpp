#include "AbstractBrush.h"
#include <QtConcurrentRun>
#include <QThreadPool>
#include <QGuiApplication>
#include <QScreen>

#include "../../settings/GlobalBrushSettings.h"

AbstractBrush::AbstractBrush(const QString &brushName, QObject *parent)
    : QObject(parent)
    , m_brushName(brushName)
{
    QObject::connect(&m_watcher, &QFutureWatcher<DrawPathList>::resultReadyAt, [this] (int index) {
        emit pathsCalculated(m_watcher.resultAt(index));

        if (m_moveToQueue.size()) {
            int index = m_moveToQueue.dequeue();
            QList<QPoint> points = m_points.mid(0, index);
            startPathsCalculation(points);
            m_prevPoint = points.last();
        }
    });
}

AbstractBrush::~AbstractBrush()
{

}

QString AbstractBrush::name() const
{
    return m_brushName;
}

void AbstractBrush::init(const QPoint &initialPoint)
{
    m_prevPoint = initialPoint;
    m_initialPoint = initialPoint;
}

void AbstractBrush::moveTo(const QPoint &point)
{
    m_points << point;

    if (m_watcher.isRunning() || m_moveToQueue.size()) {
        m_moveToQueue.enqueue(m_points.size());
        return;
    }

    startPathsCalculation(m_points);
    m_prevPoint = point;
}

void AbstractBrush::finish()
{
    m_moveToQueue.clear();
    m_watcher.cancel();
    m_points.clear();
}

void AbstractBrush::setBrushType(const QString &brushType)
{
    if (m_brushType == brushType)
        return;

    m_brushType = brushType;
    emit brushTypeChanged(brushType);
}

void AbstractBrush::setCanvasSize(const QSize &size)
{
    m_canvasSize = size;
}

double AbstractBrush::defaultWidth() const
{
    return 1.0;
}

double AbstractBrush::minWidth() const
{
    return 0.25;
}

double AbstractBrush::maxWidth() const
{
    return 5.0;
}

double AbstractBrush::defaultTransparency() const
{
    return 0.1;
}

double AbstractBrush::minTransparency() const
{
    return 0.02;
}

double AbstractBrush::maxTransparency() const
{
    return 0.5;
}

QString AbstractBrush::brushType() const
{
    return m_brushType;
}

PathSettings AbstractBrush::globalPathSettings() const
{
    QColor brushColor = GlobalBrushSettings::instance()->brushColor();
    brushColor.setAlpha(GlobalBrushSettings::instance()->brushTransparency() * 255);

    return PathSettings(GlobalBrushSettings::instance()->brushWidth(), brushColor, qApp->primaryScreen()->primaryOrientation());
}

void AbstractBrush::startPathsCalculation(const QList<QPoint> &points)
{
    QFuture<DrawPathList> result = QtConcurrent::run(this, &AbstractBrush::calculatePaths, m_prevPoint, points);
    m_watcher.setFuture(result);
}
