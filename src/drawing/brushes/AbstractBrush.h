#ifndef ABSTRACTBRUSH_H
#define ABSTRACTBRUSH_H

#include <QObject>
#include <QPainterPath>
#include <QPoint>
#include <QList>
#include <QFutureWatcher>
#include <QQueue>
#include "../../settings/BrushSettings.h"

#define RAND_BETWEEN_ZERO_AND_ONE ((double)qrand() / RAND_MAX)

typedef QPair<QPainterPath, PathSettings> DrawPath;
typedef QList<DrawPath> DrawPathList;

class AbstractProxyBrush;

class AbstractBrush : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name CONSTANT)

    Q_PROPERTY(QString brushType READ brushType WRITE setBrushType NOTIFY brushTypeChanged)

    Q_PROPERTY(double defaultWidth READ defaultWidth CONSTANT)
    Q_PROPERTY(double minWidth READ minWidth CONSTANT)
    Q_PROPERTY(double maxWidth READ maxWidth CONSTANT)

    Q_PROPERTY(double defaultTransparency READ defaultTransparency CONSTANT)
    Q_PROPERTY(double minTransparency READ minTransparency CONSTANT)
    Q_PROPERTY(double maxTransparency READ maxTransparency CONSTANT)

public:
    friend class AbstractProxyBrush;
    explicit AbstractBrush(const QString &brushName, QObject *parent = 0);
    ~AbstractBrush();

    virtual QString name() const;

    QString brushType() const;

    virtual double defaultWidth() const;
    virtual double minWidth() const;
    virtual double maxWidth() const;

    virtual double defaultTransparency() const;
    virtual double minTransparency() const;
    virtual double maxTransparency() const;

public slots:
    virtual void init(const QPoint &initialPoint);
    virtual void moveTo(const QPoint &point);
    virtual void finish();

    void setBrushType(const QString &brushType);

    void setCanvasSize(const QSize &size);

signals:
    void pathsCalculated(const DrawPathList &paths);

    void brushTypeChanged(const QString &brushType);

protected:
    virtual DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) = 0;

    PathSettings globalPathSettings() const;

protected:
    QSize m_canvasSize;
    QString m_brushName;
    QString m_brushType;
    QPoint m_prevPoint;
    QPoint m_initialPoint;
    QList<QPoint> m_points;

private:
    void startPathsCalculation(const QList<QPoint> &points);

    QFutureWatcher<DrawPathList> m_watcher;
    QQueue<int> m_moveToQueue;
};

#endif // ABSTRACTBRUSH_H
