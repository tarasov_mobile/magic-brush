#ifndef SQUARESBRUSH_H
#define SQUARESBRUSH_H

#include "AbstractBrush.h"

class SquaresBrush : public AbstractBrush
{
public:
    explicit SquaresBrush(QObject *parent = 0);
    ~SquaresBrush();

    double minWidth() const override;
    double maxWidth() const override;

    double defaultTransparency() const override;
    double minTransparency() const override;
    double maxTransparency() const override;

protected:
    DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) override;
};

#endif // SQUARESBRUSH_H
