#include "ShadedBrush.h"

ShadedBrush::ShadedBrush(QObject *parent)
    : AbstractBrush("shaded", parent)
{

}

ShadedBrush::~ShadedBrush()
{

}

double ShadedBrush::defaultWidth() const
{
    return 1.8;
}

double ShadedBrush::defaultTransparency() const
{
    return 0.2;
}

DrawPathList ShadedBrush::calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points)
{
    PathSettings settings = globalPathSettings();

    DrawPathList paths;
    QPoint lastPoint = points.last();

    QPainterPath path;
    path.moveTo(prevPoint.x(), prevPoint.y());
    path.lineTo(lastPoint);
    paths << DrawPath(path, settings);

    double dx, dy, d;
    for (int i = 0; i < points.count(); ++i) {
        dx = points.at(i).x() - lastPoint.x();
        dy = points.at(i).y() - lastPoint.y();
        d = dx * dx + dy * dy;

        if (d < 1000) {
            QPainterPath path;
            path.moveTo(lastPoint.x(), lastPoint.y());
            path.lineTo(points.at(i).x(), points.at(i).y());

            QColor color = settings.brushColor();
            color.setAlpha(color.alpha() * (1 - (d / 1000)));

            paths << DrawPath(path, PathSettings(settings.brushWidth(), color, settings.pathOrientation()));
        }
    }

    return paths;
}
