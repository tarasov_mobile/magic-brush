#include "SketchyBrush.h"
#include <QDateTime>

SketchyBrush::SketchyBrush(QObject *parent)
    : AbstractBrush("sketchy", parent)
{
    qsrand(QDateTime::currentDateTime().toTime_t());
}

SketchyBrush::~SketchyBrush()
{

}

double SketchyBrush::defaultWidth() const
{
    return 1.8;
}

double SketchyBrush::defaultTransparency() const
{
    return 1.1;
}

DrawPathList SketchyBrush::calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points)
{
    PathSettings settings = globalPathSettings();

    DrawPathList paths;
    QPoint lastPoint = points.last();

    QPainterPath path;
    path.moveTo(prevPoint.x(), prevPoint.y());
    path.lineTo(lastPoint);
    paths << DrawPath(path, settings);

    double dx, dy, d;
    for (int i = 0; i < points.count(); ++i) {
        dx = points.at(i).x() - lastPoint.x();
        dy = points.at(i).y() - lastPoint.y();
        d = dx * dx + dy * dy;

        if (d < 4000 && RAND_BETWEEN_ZERO_AND_ONE > (d / 2000)) {
            QPainterPath path;
            path.moveTo(lastPoint.x() + (dx * 0.3), lastPoint.y() + (dy * 0.3));
            path.lineTo(points.at(i).x() - (dx * 0.3), points.at(i).y()- (dy * 0.3));
            paths << DrawPath(path, settings);
        }
    }

    return paths;
}
