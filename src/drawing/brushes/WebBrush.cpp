#include "WebBrush.h"

WebBrush::WebBrush(QObject *parent)
    : AbstractBrush("web", parent)
{

}

WebBrush::~WebBrush()
{

}

double WebBrush::defaultWidth() const
{
    return 1.8;
}

double WebBrush::defaultTransparency() const
{
    return 0.4;
}

DrawPathList WebBrush::calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points)
{
    QPoint lastPoint = points.last();
    DrawPathList paths;

    QPainterPath path;
    path.moveTo(prevPoint.x(), prevPoint.y());
    path.lineTo(lastPoint);
    paths << DrawPath(path, globalPathSettings());

    PathSettings settings = globalPathSettings();
    QColor color = settings.brushColor();
    color.setAlpha(color.alpha() * 0.2);
    settings.setBrushColor(color);

    double dx, dy, d;
    for (int i = 0; i < points.count(); ++i)
    {
        dx = points.at(i).x() - lastPoint.x();
        dy = points.at(i).y() - lastPoint.y();
        d = dx * dx + dy * dy;

        if (d < 2500 && RAND_BETWEEN_ZERO_AND_ONE > 0.9)
        {
            QPainterPath path;
            path.moveTo(lastPoint.x(), lastPoint.y());
            path.lineTo(points.at(i).x(), points.at(i).y());
            paths << DrawPath(path, settings);
        }
    }

    return paths;
}
