#include "AbstractProxyBrush.h"

AbstractProxyBrush::AbstractProxyBrush(const QString &brushName, QObject *parent)
    : AbstractBrush(brushName, parent)
    , m_sourceBrush(nullptr)
{

}

AbstractProxyBrush::~AbstractProxyBrush()
{

}

QString AbstractProxyBrush::name() const
{
    return QString("%1_%2_%3").arg(m_brushName).arg(m_sourceBrush ? m_sourceBrush->name() : QString()).arg(brushType());
}

void AbstractProxyBrush::setSourceBrush(AbstractBrush *brush)
{
    m_sourceBrush = brush;
}

void AbstractProxyBrush::init(const QPoint &initialPoint)
{
    AbstractBrush::init(initialPoint);

    m_sourceBrush->init(initialPoint);
}

DrawPathList AbstractProxyBrush::calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points)
{
    if (!m_sourceBrush)
        return DrawPathList();

    DrawPathList pathList = m_sourceBrush->calculatePaths(prevPoint, points);

    return handleSourcePaths(pathList);
}
