#ifndef LONGFURBRUSH_H
#define LONGFURBRUSH_H

#include "AbstractBrush.h"

class LongFurBrush : public AbstractBrush
{
public:
    explicit LongFurBrush(QObject *parent = 0);
    ~LongFurBrush();

    double defaultWidth() const override;
    double defaultTransparency() const override;

protected:
    DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) override;
};

#endif // LONGFURBRUSH_H
