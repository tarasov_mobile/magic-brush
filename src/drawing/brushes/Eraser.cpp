#include "Eraser.h"

Eraser::Eraser(QObject *parent)
    : AbstractBrush("eraser", parent)
{

}

Eraser::~Eraser()
{

}

double Eraser::defaultWidth() const
{
    return 16.0;
}

double Eraser::minWidth() const
{
    return 5.0;
}

double Eraser::maxWidth() const
{
    return 30.0;
}

double Eraser::defaultTransparency() const
{
    return 1.0;
}

double Eraser::minTransparency() const
{
    return 1.0;
}

double Eraser::maxTransparency() const
{
    return 1.0;
}

DrawPathList Eraser::calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points)
{
    PathSettings settings = globalPathSettings();
    settings.setBrushColor(Qt::transparent);

    DrawPathList paths;
    QPoint lastPoint = points.last();

    QPainterPath path;
    path.moveTo(prevPoint.x(), prevPoint.y());
    path.lineTo(lastPoint);
    paths << DrawPath(path, settings);

    return paths;
}
