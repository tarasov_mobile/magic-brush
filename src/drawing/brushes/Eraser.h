#ifndef ERASER_H
#define ERASER_H

#include "AbstractBrush.h"

class Eraser : public AbstractBrush
{
public:
    explicit Eraser(QObject *parent = 0);
    ~Eraser();

    virtual double defaultWidth() const override;
    virtual double minWidth() const override;
    virtual double maxWidth() const override;

    virtual double defaultTransparency() const override;
    virtual double minTransparency() const override;
    virtual double maxTransparency() const override;

protected:
    DrawPathList calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points) override;
};

#endif // ERASER_H
