#include "GridBrush.h"

#include <QDateTime>
#include <QtMath>

GridBrush::GridBrush(QObject *parent)
    : AbstractBrush("grid", parent)
{
    qsrand(QDateTime::currentDateTime().toTime_t());
}

GridBrush::~GridBrush()
{

}

double GridBrush::defaultTransparency() const
{
    return minTransparency();
}

DrawPathList GridBrush::calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points)
{
    Q_UNUSED(prevPoint)

    PathSettings settings = globalPathSettings();

    DrawPathList paths;
    QPoint lastPoint = points.last();

    double cx, cy, dx, dy;
    //NOTE: here we can change default cell's size (by default it is equal to 90)
    cx = qRound(lastPoint.x() / 90.0) * 90;
    cy = qRound(lastPoint.y() / 90.0) * 90;

    dx = (cx - lastPoint.x()) * 10;
    dy = (cy - lastPoint.y()) * 10;

    for (int i = 0; i < 50; ++i)
    {
        QPainterPath path;
        path.moveTo(cx, cy);
        path.quadTo(lastPoint.x() + RAND_BETWEEN_ZERO_AND_ONE * dx,
                    lastPoint.y() + RAND_BETWEEN_ZERO_AND_ONE * dy, cx, cy);

        paths << DrawPath(path, settings);
    }

    return paths;
}
