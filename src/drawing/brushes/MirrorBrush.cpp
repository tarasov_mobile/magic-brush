#include "MirrorBrush.h"

#include <QTransform>
#include "../surfaces/Canvas.h"

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
#include <QGuiApplication>
#include <QScreen>
#endif

MirrorBrush::MirrorBrush(QObject *parent)
    : AbstractProxyBrush("mirror", parent)
{
    setBrushType("vertical");
}

MirrorBrush::~MirrorBrush()
{

}

double MirrorBrush::defaultWidth() const
{
    return 1.9;
}

double MirrorBrush::defaultTransparency() const
{
    return 0.2;
}

DrawPathList MirrorBrush::handleSourcePaths(const DrawPathList &paths)
{
    DrawPathList newPathList;
    foreach (const DrawPath &drawPath, paths) {
        QPainterPath path = drawPath.first;
        if (brushType() == "vertical" || brushType() == "corners") {
            QTransform transform;
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
            transform.translate(m_canvasSize.width(), 0);
#else
            transform.translate(1280, 0);
#endif
            transform.scale(-1, 1);

            newPathList << DrawPath(transform.map(path), drawPath.second);
        }

        if (brushType() == "horizontal" || brushType() == "corners") {
            QTransform transform;
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
            transform.translate(0, m_canvasSize.height());
#else
            transform.translate(0, 800);
#endif
            transform.scale(1, -1);

            newPathList << DrawPath(transform.map(path), drawPath.second);
        }

        if (brushType() == "corners") {
            QTransform transform;
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
            transform.translate(m_canvasSize.width(), m_canvasSize.height());
#else
            transform.translate(1280, 800);
#endif
            transform.scale(-1, -1);

            newPathList << DrawPath(transform.map(path), drawPath.second);
        }
    }

    return paths + newPathList;
}
