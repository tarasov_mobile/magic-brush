#include "SquaresBrush.h"

#include <QtMath>

SquaresBrush::SquaresBrush(QObject *parent)
    : AbstractBrush("squares", parent)
{

}

SquaresBrush::~SquaresBrush()
{

}

DrawPathList SquaresBrush::calculatePaths(const QPoint &prevPoint, const QList<QPoint> &points)
{
    DrawPathList paths;
    double dx, dy, angle, px, py;
    QPoint lastPoint = points.last();

    dx = lastPoint.x() - prevPoint.x();
    dy = lastPoint.y() - prevPoint.y();
    angle = 1.57079633;
    px = qCos(angle) * dx - qSin(angle) * dy;
    py = qSin(angle) * dx + qCos(angle) * dy;

    QPainterPath path;
    path.moveTo(prevPoint.x() - px, prevPoint.y() - py);
    path.lineTo(prevPoint.x() + px, prevPoint.y() + py);
    path.lineTo(lastPoint.x() + px, lastPoint.y() + py);
    path.lineTo(lastPoint.x() - px, lastPoint.y() - py);
    path.lineTo(prevPoint.x() - px, prevPoint.y() - py);
    paths << DrawPath(path, globalPathSettings());

    return paths;
}

double SquaresBrush::minWidth() const
{
    return 1.0;
}

double SquaresBrush::maxWidth() const
{
    return 1.0;
}

double SquaresBrush::defaultTransparency() const
{
    return 1.0;
}

double SquaresBrush::minTransparency() const
{
    return 1.0;
}

double SquaresBrush::maxTransparency() const
{
    return 1.0;
}
