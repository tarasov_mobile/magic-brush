﻿#ifndef CANVAS_H
#define CANVAS_H

#include <QQuickPaintedItem>
#include <QPainterPath>
#include <QPixmap>

class CanvasPrivate;
class AbstractBrush;

class Canvas : public QQuickPaintedItem
{
    Q_OBJECT

    Q_PROPERTY(QString brushName READ brushName WRITE setBrushName NOTIFY brushNameChanged)
    Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor NOTIFY backgroundColorChanged)
    Q_PROPERTY(QPixmap pixmap READ pixmap CONSTANT)

    Q_PROPERTY(bool canUndo READ canUndo NOTIFY canUndoChanged)
    Q_PROPERTY(bool canRedo READ canRedo NOTIFY canRedoChanged)

    Q_PROPERTY(bool drawingActive READ drawingActive NOTIFY drawingActiveChanged)

    friend class CanvasPrivate;
public:
    explicit Canvas(QQuickItem *parent = 0);
    ~Canvas();

    void paint(QPainter *painter) override;

    QString brushName() const;
    QColor backgroundColor() const;
    QPixmap pixmap() const;

    bool canUndo() const;
    bool canRedo() const;

    bool drawingActive() const;

public slots:
    void setBrushName(const QString &brushName);
    void setBackgroundColor(const QColor &backgroundColor);

    void redo();
    void undo();
    void clear();

signals:
    void clicked();//emmited when user only clicked the canvas(without painting)

    void brushNameChanged(const QString &brushName);
    void backgroundColorChanged(const QColor &backgroundColor);
    void canUndoChanged(bool canUndo);
    void canRedoChanged(bool canRedo);

    void drawingActiveChanged(bool active);

    void imageSaveError(const QString &errorText);

protected:
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private:
    CanvasPrivate *d;
};

#endif // CANVAS_H
