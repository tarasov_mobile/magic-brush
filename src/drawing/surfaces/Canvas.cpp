#include "Canvas.h"

#include <QList>
#include <QPainterPath>
#include <QPainter>
#include <QTimer>
#include <QGuiApplication>
#include <QScreen>
#include <QStack>
#include <QStandardPaths>
#include <QDir>
#include <QDateTime>
#include <QUndoCommand>
#include <QMutex>
#include <QThread>
#include <QMetaType>

#include "../../core/BrushFactory.h"
#include "../../settings/GlobalBrushSettings.h"
#include "../brushes/AbstractBrush.h"
#include "../brushes/SquaresBrush.h"
#include "../../settings/AppSettings.h"

typedef QPair<QPointF /*topLeft*/, QImage> CommandPair;

namespace {
const int CANVAS_UPDATE_INTERVAL = 15;
const QColor DEFAULT_BACKGROUND_COLOR = QColor("#ffffff");

const int MOUSE_MOVE_UPDATE_INTERVAL =
        #ifdef Q_OS_ANDROID
        8
        #else
        0
        #endif
        ;

void clearPixmap(QPixmap *pixmap)
{
    QPainter p(pixmap);
    p.setCompositionMode(QPainter::CompositionMode_Clear);
    p.fillRect(pixmap->rect(), Qt::transparent);
    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
}
}

struct PixmapInfo
{
    PixmapInfo()
    {
        orientation = Qt::PortraitOrientation;
        compositionMode = QPainter::CompositionMode_SourceOver;
        pixmap = nullptr;
    }

    QPointF topLeft;
    Qt::ScreenOrientation orientation;//shows in which screen orientation content of the pixmap was painted by user
    QPainter::CompositionMode compositionMode;//this is needed to undo/redo Eraser
    QPixmap *pixmap;
};

class CommandPixmapRenderer : public QObject
{
    Q_OBJECT

public:
    enum RefineDirection
    {
        Backward = 0,
        Forward
    };

    explicit CommandPixmapRenderer(QObject *parent = 0)
        : QObject(parent)
    {

    }
    ~CommandPixmapRenderer()
    {

    }

public slots:
    CommandPair getCommandInfo() const
    {
        QMutexLocker locker(&m_mutex);

        QRectF rect = QRectF(m_currentCommandRect.topLeft() * m_image.devicePixelRatio(),
                           m_currentCommandRect.bottomRight() * m_image.devicePixelRatio());

        // refine command bounding rect
        // this is needed because different brushes have different width
        // and sometimes we get command image cropped
        do {
            QRect newRect;
            newRect.setLeft(refineEdge(rect.toRect(), Qt::LeftEdge));
            newRect.setTop(refineEdge(rect.toRect(), Qt::TopEdge));
            newRect.setRight(refineEdge(rect.toRect(), Qt::RightEdge));
            newRect.setBottom(refineEdge(rect.toRect(), Qt::BottomEdge));

            if (QRectF(newRect) != rect)
                rect = QRectF(newRect);
            else
                break;
        } while (true);

        return CommandPair(rect.topLeft() / m_image.devicePixelRatio(), m_image.copy(rect.toRect()));
    }

    void recreateImage(const QSize &size, const QColor &bgColor)
    {
        QMutexLocker locker(&m_mutex);

        m_image = QImage(size, QImage::Format_ARGB32);
        m_image.fill(bgColor);
        m_bgColor = bgColor;
        m_image.setDevicePixelRatio(qApp->devicePixelRatio());

        m_currentCommandRect = QRectF(m_image.rect().bottomRight(), m_image.rect().topLeft());
    }

    void addNewPaths(const DrawPathList &paths)
    {
        QMutexLocker locker(&m_mutex);

        QPainter p(&m_image);

        bool useAntialiasing =
        #ifdef Q_OS_IOS
                true;
        #elif defined(Q_OS_ANDROID)
            AppSettings::instance()->getCustomProperty("use_antialiasing").toBool();
        #endif

        if (useAntialiasing)
            p.setRenderHint(QPainter::Antialiasing);

        foreach (const DrawPath &path, paths) {
            PathSettings settings = path.second;
            QPen pen(settings.brushColor(), settings.brushWidth());
            pen.setJoinStyle(Qt::RoundJoin);
            pen.setCapStyle(Qt::RoundCap);
            p.setPen(pen);
            p.save();

            p.setCompositionMode(settings.brushColor() == Qt::transparent ?
                                     QPainter::CompositionMode_Clear
                                   : QPainter::CompositionMode_SourceOver);

            p.drawPath(path.first);
            p.restore();

            updateCurrentCommandRect(path);
        }
    }

private:
    int refineEdge(const QRect &rect, Qt::Edge edge) const
    {
        int extraValueFirst = (edge == Qt::TopEdge || edge == Qt::BottomEdge) ? rect.left() : rect.top();
        int extraValueLast = (edge == Qt::TopEdge || edge == Qt::BottomEdge) ? rect.right() : rect.bottom();

        int currentValue = 0;
        int lastValue = 0;
        switch (edge) {
        case Qt::LeftEdge:
            currentValue = rect.left();
            break;

        case Qt::RightEdge:
            currentValue = rect.right();
            lastValue = m_image.width() - 1;
            break;

        case Qt::TopEdge:
            currentValue = rect.top();
            break;

        case Qt::BottomEdge:
            currentValue = rect.bottom();
            lastValue = m_image.height() - 1;
            break;

        default:
            break;
        }

        if (currentValue == 0 || currentValue == m_image.width() - 1 || currentValue == m_image.height() -1)
            return currentValue;

        RefineDirection direction = (edge == Qt::LeftEdge || edge == Qt::TopEdge)
                ? CommandPixmapRenderer::Backward : CommandPixmapRenderer::Forward;

        bool pixelFound = false;
        for (int i = currentValue + (direction == CommandPixmapRenderer::Backward ? -1 : 1);
             (direction == CommandPixmapRenderer::Backward ? i > lastValue : i < lastValue);
             direction == CommandPixmapRenderer::Backward ? --i : ++i)
        {
            for (int j = extraValueFirst; j < extraValueLast; ++j)
            {
                int xCoor = edge == Qt::LeftEdge || edge == Qt::RightEdge ? i : j;
                int yCoor = edge == Qt::LeftEdge || edge == Qt::RightEdge ? j : i;
                if (m_image.pixelColor(xCoor, yCoor) != m_bgColor) {
                    pixelFound = true;
                    break;
                }
            }

            if (!pixelFound)
                return (direction == CommandPixmapRenderer::Backward ? i + 1 : i - 1);

            pixelFound = false;
        }

        return (direction == CommandPixmapRenderer::Backward ? 0 : lastValue);
    }

    void updateCurrentCommandRect(const DrawPath &drawPath)
    {
        QRectF rectF = drawPath.first.controlPointRect();

        int imageWidth = m_image.width() / m_image.devicePixelRatio();
        int imageHeight = m_image.height() / m_image.devicePixelRatio();

        if (m_currentCommandRect.left() > rectF.left() && rectF.left() >= 0) {
            m_currentCommandRect.setLeft(rectF.left());
        }

        if (m_currentCommandRect.right() < rectF.right() && rectF.right() <= imageWidth - 1) {
            m_currentCommandRect.setRight(rectF.right());
        }

        if (m_currentCommandRect.top() > rectF.top() && rectF.top() >= 0) {
            m_currentCommandRect.setTop(rectF.top());
        }

        if (m_currentCommandRect.bottom() < rectF.bottom() && rectF.bottom() <= imageHeight - 1) {
            m_currentCommandRect.setBottom(rectF.bottom());
        }
    }

    QRectF m_currentCommandRect;
    QImage m_image;
    QColor m_bgColor;
    mutable QMutex m_mutex;
};

//Describes Undo/Redo command
class DrawPixmapCommand : public QUndoCommand
{
public:
    explicit DrawPixmapCommand(QPixmap *canvasPixmap, const QList<PixmapInfo> &pixmapInfoList, QUndoCommand *parent = nullptr)
        : QUndoCommand(parent)
    {
        m_canvasPixmap = canvasPixmap;
        m_pixmapInfoList = pixmapInfoList;
        m_disableRedo = true;
    }

    ~DrawPixmapCommand() override
    {
        if (m_pixmapInfoList.count())
            delete m_pixmapInfoList.last().pixmap;
    }

    QList<PixmapInfo> pixmapInfoList() const
    {
        return m_pixmapInfoList;
    }

    virtual void undo() override
    {
        //clear the pixmap at first
        clearPixmap(m_canvasPixmap);

        QPainter p(m_canvasPixmap);
        //then draw all previous pixmaps
        for (int i = 0; i < m_pixmapInfoList.count() - 1; ++i) {
            const PixmapInfo &pixmapInfo = m_pixmapInfoList.at(i);
            p.save();
            rotateCoorSystem(pixmapInfo.orientation, p);
            p.setCompositionMode(pixmapInfo.compositionMode);
            p.drawPixmap(pixmapInfo.topLeft, *pixmapInfo.pixmap);
            p.restore();
        }
    }

    virtual void redo() override
    {
        //this condition is used to disable first call of redo()
        //when we push the command into QUndoStack
        //see QUndoStack::push(), it calls redo() automatically
        if (m_disableRedo) {
            m_disableRedo = false;
            return;
        }

        QPainter p(m_canvasPixmap);
        const PixmapInfo &pixmapInfo = m_pixmapInfoList.last();
        rotateCoorSystem(pixmapInfo.orientation, p);
        p.setCompositionMode(pixmapInfo.compositionMode);
        p.drawPixmap(pixmapInfo.topLeft, *pixmapInfo.pixmap);
    }

private:
    void rotateCoorSystem(Qt::ScreenOrientation pixmapOrientation, QPainter &painter)
    {
        bool needRotate = pixmapOrientation != qApp->primaryScreen()->orientation();
#ifdef Q_OS_IOS
      if (qApp->primaryScreen()->primaryOrientation() == Qt::PortraitOrientation) {
          needRotate &= (pixmapOrientation != Qt::PortraitOrientation && pixmapOrientation != Qt::InvertedPortraitOrientation)
                  //this is needed to handle inverted orientations
                  || (pixmapOrientation == Qt::PortraitOrientation && qApp->primaryScreen()->orientation() == Qt::InvertedPortraitOrientation)
                  || (pixmapOrientation == Qt::InvertedPortraitOrientation && qApp->primaryScreen()->orientation() == Qt::PortraitOrientation);
      } else {
          needRotate &= (pixmapOrientation != Qt::LandscapeOrientation && pixmapOrientation != Qt::InvertedLandscapeOrientation)
                  //this is needed to handle inverted orientations
                  || (pixmapOrientation == Qt::LandscapeOrientation && qApp->primaryScreen()->orientation() == Qt::InvertedLandscapeOrientation)
                  || (pixmapOrientation == Qt::InvertedLandscapeOrientation && qApp->primaryScreen()->orientation() == Qt::LandscapeOrientation);
      }
#endif

        if (needRotate) {
            int canvasWidth = m_canvasPixmap->width() / m_canvasPixmap->devicePixelRatio();
            int canvasHeight = m_canvasPixmap->height() / m_canvasPixmap->devicePixelRatio();
            int angle = qApp->primaryScreen()->angleBetween(pixmapOrientation, qApp->primaryScreen()->orientation());

            switch (angle) {
            case 90:
                painter.translate(canvasWidth, 0);
                break;

            case 180:
                painter.translate(canvasWidth, canvasHeight);
                break;

            case 270:
                painter.translate(0, canvasHeight);
                break;

            default:
                break;
            }

            painter.rotate(angle);
        }
    }

    QPixmap *m_canvasPixmap;
    QList<PixmapInfo> m_pixmapInfoList;
    bool m_disableRedo;
};

class CanvasPrivate
{
public:
    CanvasPrivate(Canvas *q)
    {
        this->q = q;

        pixmapOrientation = qApp->primaryScreen()->orientation();

        currentBrush = nullptr;
        mousePressed = false;
        drawingActive = false;
        backgroundColor = DEFAULT_BACKGROUND_COLOR;

        mouseMoveTimer.setSingleShot(true);

        rendererThread = new QThread;
        currentCommandRenderer = new CommandPixmapRenderer;
        currentCommandRenderer->moveToThread(rendererThread);
        rendererThread->start();

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
        pixmapInitialized = false;
#endif
    }

    ~CanvasPrivate()
    {
        rendererThread->quit();
        rendererThread->wait();
        rendererThread->deleteLater();
        currentCommandRenderer->deleteLater();
    }

    void setDrawingActive(bool active)
    {
        if (drawingActive == active)
            return;

        drawingActive = active;
        emit q->drawingActiveChanged(active);
    }

    void resizePixmap(const QSize &newSize)
    {
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
        if (!pixmapInitialized)
            pixmapInitialized = true;
#endif

        QPixmap newPixmap(newSize * QGuiApplication::primaryScreen()->devicePixelRatio());
        newPixmap.setDevicePixelRatio(QGuiApplication::primaryScreen()->devicePixelRatio());
        newPixmap.fill(Qt::transparent);

        if (!pixmap.isNull()) {
            QPainter p(&newPixmap);
            p.drawPixmap(0, 0, pixmap);
        }

        pixmap = newPixmap;
    }

    QPixmap flattenedPixmap()
    {
        QPixmap newPixmap(pixmap.size());
        newPixmap.setDevicePixelRatio(pixmap.devicePixelRatio());
        newPixmap.fill(backgroundColor);
        QPainter p(&newPixmap);
        p.drawPixmap(0, 0, pixmap);

        return newPixmap;
    }

    PixmapInfo createCurrentCommandPixmapInfo(QPixmap *pixmap, const QPointF &topLeft)
    {
        PixmapInfo pixmapInfo;
        pixmapInfo.topLeft = topLeft;
        pixmapInfo.pixmap = pixmap;
        pixmapInfo.orientation = pixmapOrientation;
        if (q->brushName() == "eraser")
            pixmapInfo.compositionMode = QPainter::CompositionMode_DestinationIn;

        return pixmapInfo;
    }

    void appendUndoCommand(const PixmapInfo &pixmapInfo)
    {
        QList<PixmapInfo> pixmapList;
        if (undoStack.count()) {
            //index() - 1 because we need last Undo command but not Redo command
            const DrawPixmapCommand *lastUndoCommand = static_cast<const DrawPixmapCommand *>(undoStack.command(undoStack.index() - 1));
            if (lastUndoCommand)
                pixmapList << lastUndoCommand->pixmapInfoList();
        }
        pixmapList << pixmapInfo;

        undoStack.push(new DrawPixmapCommand(&pixmap, pixmapList));
    }

    void changePixmapOrientation()
    {
        QPoint center = pixmap.rect().center();
        QMatrix matrix;
        matrix.translate(center.x(), center.y());
        matrix.rotate(qApp->primaryScreen()->angleBetween(pixmapOrientation, qApp->primaryScreen()->orientation()));
        pixmap = pixmap.transformed(matrix);
        pixmap.setDevicePixelRatio(qApp->primaryScreen()->devicePixelRatio());

        pixmapOrientation = qApp->primaryScreen()->orientation();
    }

    QUndoStack undoStack;

    QMetaObject::Connection orientationConnection;

    Qt::ScreenOrientation pixmapOrientation;
    DrawPathList pathsToDraw;
    QTimer canvasUpdateTimer;
    QPixmap pixmap;
    AbstractBrush *currentBrush;
    QMetaObject::Connection lastBrushConnection;
    bool mousePressed;
    BrushFactory brushFactory;
    QColor backgroundColor;
    bool drawingActive;
    QTimer mouseMoveTimer;
    QThread *rendererThread;
    CommandPixmapRenderer *currentCommandRenderer;

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
    bool pixmapInitialized;
#endif

    Canvas *q;
};

Canvas::Canvas(QQuickItem *parent)
    : QQuickPaintedItem(parent)
    , d(new CanvasPrivate(this))
{
    setAcceptedMouseButtons(Qt::LeftButton);
#ifdef Q_OS_ANDROID
    setRenderTarget(QQuickPaintedItem::FramebufferObject);
#endif

    setAntialiasing(
            #ifdef Q_OS_IOS
                true
            #elif defined(Q_OS_ANDROID)
                AppSettings::instance()->getCustomProperty("use_antialiasing").toBool()
            #endif
                );

    qRegisterMetaType<DrawPathList>("DrawPathList");
    qRegisterMetaType<CommandPair>("CommandPair");

    d->canvasUpdateTimer.setInterval(CANVAS_UPDATE_INTERVAL);
    QObject::connect(&d->canvasUpdateTimer, &QTimer::timeout, [this] () {
        if (!d->mousePressed && !d->pathsToDraw.count()) {
            d->canvasUpdateTimer.stop();
        }

        this->update();
    });

    QObject::connect(&d->undoStack, &QUndoStack::canUndoChanged, this, &Canvas::canUndoChanged);
    QObject::connect(&d->undoStack, &QUndoStack::canRedoChanged, this, &Canvas::canRedoChanged);

    // this is needed to catch transition between Inverted and not Inverted orientations
    // for Android take a look at /docs/Android_Inverted_Orientations_Fix.txt for more info
    d->orientationConnection = QObject::connect(qApp->primaryScreen(), &QScreen::orientationChanged, [this] () {
        QTimer::singleShot(0, this, SLOT(update()));
    });
}

Canvas::~Canvas()
{
    QObject::disconnect(d->orientationConnection);

    delete d;
}

void Canvas::paint(QPainter *painter)
{
    // if drawing is active then we paint directly onto QQuickPaintedItem's paint device (by default it is a QImage)
    if (!d->pathsToDraw.isEmpty()) {
        foreach (const DrawPath &path, d->pathsToDraw) {
            PathSettings settings = path.second;
            QPen pen(settings.brushColor(), settings.brushWidth());
            pen.setJoinStyle(Qt::RoundJoin);
            pen.setCapStyle(Qt::RoundCap);
            painter->setPen(pen);
            painter->save();

            painter->setCompositionMode(settings.brushColor() == Qt::transparent ?
                                     QPainter::CompositionMode_Clear
                                   : QPainter::CompositionMode_SourceOver);

            painter->drawPath(path.first);
            painter->restore();
        }

        d->pathsToDraw.clear();
    }

#ifdef Q_OS_ANDROID
    if (!drawingActive() && d->pixmapOrientation != qApp->primaryScreen()->orientation())
        d->changePixmapOrientation();
#endif

#ifdef Q_OS_IOS
    // this is a workaround for a bug when pixmap orientation changed even when the whole app UI stays untouch
    // we just compare orientation and primaryOrientation as they can be different for some reason

    bool needRotate = !drawingActive() && d->pixmapOrientation != qApp->primaryScreen()->orientation();

    if (qApp->primaryScreen()->primaryOrientation() == Qt::PortraitOrientation) {
        needRotate &= (d->pixmapOrientation != Qt::PortraitOrientation && d->pixmapOrientation != Qt::InvertedPortraitOrientation)
                //this is needed to handle inverted orientations
                || (d->pixmapOrientation == Qt::PortraitOrientation && qApp->primaryScreen()->orientation() == Qt::InvertedPortraitOrientation)
                || (d->pixmapOrientation == Qt::InvertedPortraitOrientation && qApp->primaryScreen()->orientation() == Qt::PortraitOrientation);
    } else {
        needRotate &= (d->pixmapOrientation != Qt::LandscapeOrientation && d->pixmapOrientation != Qt::InvertedLandscapeOrientation)
                //this is needed to handle inverted orientations
                || (d->pixmapOrientation == Qt::LandscapeOrientation && qApp->primaryScreen()->orientation() == Qt::InvertedLandscapeOrientation)
                || (d->pixmapOrientation == Qt::InvertedLandscapeOrientation && qApp->primaryScreen()->orientation() == Qt::LandscapeOrientation);
    }

    if (needRotate)
        d->changePixmapOrientation();
#endif

    // if drawing is not active then we paint our pixmap
    if (!drawingActive()) {
        painter->setCompositionMode(QPainter::CompositionMode_Source);
        painter->drawPixmap(0, 0, d->pixmap);
        painter->setCompositionMode(QPainter::CompositionMode_SourceOver);
    }
}

QString Canvas::brushName() const
{
    return d->currentBrush->name();
}

QColor Canvas::backgroundColor() const
{
    return d->backgroundColor;
}

QPixmap Canvas::pixmap() const
{
    return d->flattenedPixmap();
}

bool Canvas::canUndo() const
{
    return d->undoStack.canUndo();
}

bool Canvas::canRedo() const
{
    return d->undoStack.canRedo();
}

bool Canvas::drawingActive() const
{
    return d->drawingActive;
}

void Canvas::setBrushName(const QString &brushName)
{
    if (d->currentBrush && d->currentBrush->name() == brushName)
        return;

    AbstractBrush *brush = d->brushFactory.getBrushByName(brushName);
    if (!brush)
        return;

    d->currentBrush = brush;
    QObject::disconnect(d->lastBrushConnection);
    d->lastBrushConnection = QObject::connect(d->currentBrush, &AbstractBrush::pathsCalculated, [this] (const DrawPathList &paths) {
        if (!d->mousePressed)
            return;

        //QTimer::singleShot acts as a replacement of Qt::QueuedConnection
        QTimer::singleShot(0, [this, paths] () {
            QMetaObject::invokeMethod(d->currentCommandRenderer, "addNewPaths", Qt::QueuedConnection, Q_ARG(const DrawPathList&, paths));
            d->pathsToDraw << paths;
        });
    });
    emit brushNameChanged(brushName);

    // set brush width
    GlobalBrushSettings::instance()->setMinBrushWidth(d->currentBrush->minWidth());
    GlobalBrushSettings::instance()->setMaxBrushWidth(d->currentBrush->maxWidth());

    QVariant brushWidth = AppSettings::instance()->getCustomProperty(QString("%1_width").arg(brushName));
    GlobalBrushSettings::instance()->setBrushWidth(brushWidth.isValid() ? brushWidth.toDouble() : d->currentBrush->defaultWidth());

    // set brush transparency
    GlobalBrushSettings::instance()->setMinBrushTransparency(d->currentBrush->minTransparency());
    GlobalBrushSettings::instance()->setMaxBrushTransparency(d->currentBrush->maxTransparency());

    QVariant brushTransparency = AppSettings::instance()->getCustomProperty(QString("%1_transparency").arg(brushName));
    GlobalBrushSettings::instance()->setBrushTransparency(
                brushTransparency.isValid() ? brushTransparency.toDouble() : d->currentBrush->defaultTransparency());
}

void Canvas::setBackgroundColor(const QColor &backgroundColor)
{
    if (d->backgroundColor != backgroundColor) {
        d->backgroundColor = backgroundColor;

        emit backgroundColorChanged(backgroundColor);
    }
}

void Canvas::redo()
{
    d->undoStack.redo();

    update();
}

void Canvas::undo()
{
    d->undoStack.undo();

    update();
}

void Canvas::clear()
{
    d->undoStack.clear();
    clearPixmap(&d->pixmap);

    update();
}

void Canvas::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickPaintedItem::geometryChanged(newGeometry, oldGeometry);

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
    if (d->pixmapInitialized)
        return;
#endif

    if (newGeometry.isValid() && newGeometry.size() != oldGeometry.size())
        d->resizePixmap(newGeometry.size().toSize());
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
    d->mousePressed = true;

    QMetaObject::invokeMethod(d->currentCommandRenderer, "recreateImage", Qt::QueuedConnection,
                              Q_ARG(QSize, d->pixmap.size())
                              , Q_ARG(QColor, (brushName() == "eraser" ? Qt::white : Qt::transparent)));

    d->canvasUpdateTimer.start();

    d->currentBrush->setCanvasSize(d->pixmap.size() / qApp->primaryScreen()->devicePixelRatio());
    d->currentBrush->init(event->pos());
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
    if (!drawingActive())
        d->setDrawingActive(true);

    //this is needed to avoid freezes when
    // using S-Pen stylus
    if (d->mouseMoveTimer.isActive())
        return;

    d->mouseMoveTimer.start(MOUSE_MOVE_UPDATE_INTERVAL);

    d->currentBrush->moveTo(event->pos());
}

void Canvas::mouseReleaseEvent(QMouseEvent *)
{
    d->mousePressed = false;

    if (!d->drawingActive) {
        emit clicked();
    } else {
        CommandPair commandInfo;
        QMetaObject::invokeMethod(d->currentCommandRenderer, "getCommandInfo", Qt::BlockingQueuedConnection, Q_RETURN_ARG(CommandPair, commandInfo));
        QPixmap *pixmap = new QPixmap(QPixmap::fromImage(commandInfo.second));
        pixmap->setDevicePixelRatio(qApp->devicePixelRatio());

        PixmapInfo pixmapInfo = d->createCurrentCommandPixmapInfo(pixmap, commandInfo.first);
        d->appendUndoCommand(pixmapInfo);

        d->currentBrush->finish();

        d->setDrawingActive(false);

        QPainter p(&d->pixmap);
        p.setCompositionMode(pixmapInfo.compositionMode);
        p.drawPixmap(pixmapInfo.topLeft, *pixmapInfo.pixmap);
    }
}

#include "Canvas.moc"
