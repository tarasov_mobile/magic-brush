#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QString>

namespace {
const QString MONTH_SUBSCRIPTION_SKU = "ab1subscription";
const QString YEAR_SUBSCRIPTION_SKU = "absub1year";
const QString FOREVER_PURCHASE_SKU = "abfull";
}

#endif // CONSTANTS_H
