#ifndef ANALYTICSMANAGER_H
#define ANALYTICSMANAGER_H

#include <QObject>

class AnalyticsManager : public QObject
{
    Q_OBJECT
public:
    ~AnalyticsManager();

    static AnalyticsManager* instance();

public slots:
    void sendEvent(const QString &categoryName, const QString &eventName);

private:
    explicit AnalyticsManager(QObject *parent = 0);
    AnalyticsManager(const AnalyticsManager &)    ;
    AnalyticsManager& operator=(const AnalyticsManager &);

    static AnalyticsManager* m_instance;
};

#endif // ANALYTICSMANAGER_H
