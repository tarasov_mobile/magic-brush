#ifndef PURCHASEMANAGER_H
#define PURCHASEMANAGER_H

#include <QObject>

class PurchaseManagerPrivate;

class PurchaseManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool billingInitialized READ billingInitialized NOTIFY billingInitializedChanged)

    Q_PROPERTY(QString monthPrice READ monthPrice NOTIFY monthPriceChanged)
    Q_PROPERTY(QString yearPrice READ yearPrice NOTIFY yearPriceChanged)
    Q_PROPERTY(QString foreverPrice READ foreverPrice NOTIFY foreverPriceChanged)

    Q_PROPERTY(bool hasPurchase READ hasPurchase NOTIFY hasPurchaseChanged)

    Q_PROPERTY(PurchaseManager::State state READ state NOTIFY stateChanged)

    friend class PurchaseManagerPrivate;
public:
    enum State {
        Inactive = 0,
        Active // means that there is work in progress
    };
    Q_ENUMS(State)

    ~PurchaseManager();

    static PurchaseManager *instance();

    void initInAppBilling();

    QString monthPrice() const;
    QString yearPrice() const;
    QString foreverPrice() const;
    bool hasPurchase() const;

    PurchaseManager::State state() const;

    bool billingInitialized() const;

public slots:
    void purchaseMonthSubscription();
    void purchaseYearSubscription();
    void purchaseAppForever();

    void restoreProducts();// used only on iOS

    void requestUpdateData();

signals:
    void dataUpdateError();

    void purchaseCompleted();

    void monthPriceChanged(const QString &monthPrice);
    void yearPriceChanged(const QString &yearPrice);
    void foreverPriceChanged(const QString &foreverPrice);
    void hasPurchaseChanged(bool hasPurchase);
    void billingInitializedChanged(bool billingInitialized);

    void stateChanged(PurchaseManager::State state);

private:
    explicit PurchaseManager(QObject *parent = 0);

    void setMonthPrice(const QString &price);
    void setYearPrice(const QString &price);
    void setForeversPrice(const QString &price);

    void setHasPurchase(bool hasPurchase);
    void setBillingInitialized(bool initialized);

    void setState(PurchaseManager::State state);

    PurchaseManagerPrivate *d;

    QString m_monthPrice;
    QString m_yearPrice;
    QString m_foreverPrice;
    bool m_hasPurchase;
    bool m_billingInitialized;
    PurchaseManager::State m_state;

    static PurchaseManager* m_instance;
};

#endif // PURCHASEMANAGER_H
