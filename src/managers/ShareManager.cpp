﻿#include "ShareManager.h"

#ifdef Q_OS_ANDROID
#include "../helpers/AndroidHelper.h"
#endif

#ifdef Q_OS_IOS
#include "../helpers/AiosHelper.h"
#endif

ShareManager::ShareManager(QObject *parent)
    : QObject(parent)
{

}

void ShareManager::shareImage(const QUrl &filePath, const QString &appName)
{
#ifdef Q_OS_ANDROID
    AndroidHelper::instance()->shareImage(appName, filePath.toLocalFile());
#else
    AiosHelper::instance()->shareImage(appName, filePath.toLocalFile());
#endif
}
