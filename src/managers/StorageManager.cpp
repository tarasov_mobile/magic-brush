﻿#include "StorageManager.h"

#include <QStandardPaths>
#include <QDateTime>
#include <QDir>
#include <QPainter>
#include <QFileSelector>
#include "../models/StorageModel.h"

#ifdef Q_OS_ANDROID
#include "../helpers/AndroidHelper.h"
#endif

#ifdef Q_OS_IOS
#include "../helpers/AiosHelper.h"
#endif

namespace {
QStandardPaths::StandardLocation PLATFORM_LOCATION =
#ifdef Q_OS_IOS
QStandardPaths::DocumentsLocation
#else
QStandardPaths::AppDataLocation
#endif
;
}

StorageManager* StorageManager::m_instance = nullptr;

class StorageManagerPrivate
{
public:
    StorageManagerPrivate(StorageManager *q)
        : isDrawWatermark(true)
    {
        this->q = q;

        QDir dir = getPlatformPicturesLocation();
        if (!dir.exists("ArtBreak")) {
            dir.mkdir("ArtBreak");
        }
        dir.cd("ArtBreak");

        storageModel = new StorageModel(dir.absolutePath());
    }

    ~StorageManagerPrivate()
    {
        delete storageModel;
    }

    void replacePircuresWithOriginal(const QDir &dir)
    {
        QStringList fileNames = dir.entryList(QStringList() << "*.jpg", QDir::Files);
        foreach (const QString &fileName, fileNames) {
            QString originalFilePath = generateFilePath(PLATFORM_LOCATION, fileName, "ArtBreakOriginal");
            if (QFile::exists(originalFilePath)) {
                QString currentFilePath = dir.absoluteFilePath(fileName);
                QString backupFilePath = currentFilePath + ".bak";
                if (QFile::rename(currentFilePath, backupFilePath)) {
                    if (QFile::copy(originalFilePath, currentFilePath)) {
                        // remove original and backup JPG as they useless now
                        QFile::remove(originalFilePath);
                        QFile::remove(backupFilePath);
                    }
                }
            }
        }
    }

    QString generateFileName() const
    {
        return QDateTime::currentDateTime().toString("ddMMyy-hhmmsszzz") + ".jpg";
    }

    QDir getPlatformPicturesLocation() const
    {
        return QDir(QStandardPaths::writableLocation(PLATFORM_LOCATION));
    }

    QString generateFilePath(QStandardPaths::StandardLocation location, const QString &fileName, const QString &folderName = QString())
    {
        QDir dir(QStandardPaths::writableLocation(location));
        if (!folderName.isEmpty()) {
            if (!dir.exists(folderName)) {
                if (!dir.mkdir(folderName)) {
                    emit q->error(QObject::tr("Unable to create a subfolder within: %1").arg(dir.absolutePath()));
                    return QString();
                }
            }
            if (!dir.cd(folderName)) {
                emit q->error(QString(QObject::tr("Unable to open the %1 folder")).arg(folderName));
                return QString();
            }
        }

        return dir.absoluteFilePath(fileName);
    }

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
    QPixmap drawWatermark(const QPixmap &pixmap)
    {
        Qt::ScreenOrientation pixmapOrienation = pixmap.width() > pixmap.height() ? Qt::LandscapeOrientation : Qt::PortraitOrientation;

#ifdef Q_OS_ANDROID
        QFileSelector selector;
        selector.setExtraSelectors(QStringList() << AndroidHelper::instance()->getCurrentScreenDensity());
        QPixmap wPixmap(selector.select(pixmapOrienation == Qt::PortraitOrientation ?
                                            ":/images/watermark.png"
                                          : ":/images/watermark_land.png"));
#elif defined(Q_OS_IOS)
        QString fileName = QString("%1%2%3.png").arg(pixmapOrienation == Qt::PortraitOrientation ? ":/images/watermark"
                                                                                              : ":/images/watermark_land")
                .arg(AiosHelper::instance()->isTablet() ? "_ipad" : "")
                .arg(pixmap.devicePixelRatio() > 1 ? QString("@%1x").arg(pixmap.devicePixelRatio()) : "");
        QPixmap wPixmap(fileName);
#endif
        wPixmap.setDevicePixelRatio(pixmap.devicePixelRatio());

        QPixmap newPixmap = pixmap;

        QPainter p(&newPixmap);
        int pixmapWidth = pixmap.width() / pixmap.devicePixelRatio();
        int pixmapHeight = pixmap.height() / pixmap.devicePixelRatio();
        int wPixmapWidth = wPixmap.width() / wPixmap.devicePixelRatio();
        int wPixmapHeight = wPixmap.height() / wPixmap.devicePixelRatio();

        int x = (pixmapWidth - wPixmapWidth) / (pixmapOrienation == Qt::PortraitOrientation ? 1 : 2);
        int y = pixmapOrienation == Qt::PortraitOrientation ? ((pixmapHeight - wPixmapHeight) / 2) : 0;
        p.drawPixmap(x, y, wPixmap);

        return newPixmap;
    }
#endif

    bool saveToFileWithWatermark(const QString &filePath, const QString &originalFilePath, const QPixmap &pixmap)
    {
        QPixmap newPixmap = drawWatermark(pixmap);
        if (!(newPixmap.save(filePath) && pixmap.save(originalFilePath))) {
            QFile::remove(filePath);
            QFile::remove(originalFilePath);
            emit q->error(QObject::tr("Unable to save a picture: %1").arg(filePath));
            return false;
        }

        return true;
    }

    bool saveToFile(const QString &filePath, const QPixmap &pixmap)
    {
        if (!pixmap.save(filePath)) {
            emit q->error(QObject::tr("Unable to save a picture: %1").arg(filePath));
            return false;
        }

        return true;
    }

    StorageModel *storageModel;
    bool isDrawWatermark;
    StorageManager *q;
};

StorageManager::~StorageManager()
{
    delete d;
}

StorageManager *StorageManager::instance()
{
    if (!m_instance)
        m_instance = new StorageManager;

    return m_instance;
}

QUrl StorageManager::savePixmapTemporary(const QPixmap &pixmap)
{
    QString fileName = d->generateFileName();

    QString filePath = d->generateFilePath(PLATFORM_LOCATION, fileName, "ArtBreakTemp");

    if (d->isDrawWatermark) {
        QString originalFilePath = d->generateFilePath(PLATFORM_LOCATION, fileName, "ArtBreakOriginal");
        if (d->saveToFileWithWatermark(filePath, originalFilePath, pixmap)) {
            return QUrl::fromLocalFile(filePath);
        }
    } else {
        if (d->saveToFile(filePath, pixmap)) {
            return QUrl::fromLocalFile(filePath);
        }
    }

    return QUrl();
}

QUrl StorageManager::savePixmap(const QPixmap &pixmap)
{
    QString fileName = d->generateFileName();

    QString filePath = d->generateFilePath(PLATFORM_LOCATION, fileName, "ArtBreak");

    if (d->isDrawWatermark) {
        QString originalFilePath = d->generateFilePath(PLATFORM_LOCATION, fileName, "ArtBreakOriginal");
        if (d->saveToFileWithWatermark(filePath, originalFilePath, pixmap)) {
            d->storageModel->prependItem(filePath);
            return QUrl::fromLocalFile(filePath);
        }
    } else {
        if (d->saveToFile(filePath, pixmap)) {
            d->storageModel->prependItem(filePath);
            return QUrl::fromLocalFile(filePath);
        }
    }

    return QUrl();
}

QUrl StorageManager::copyPictureToExternalStorage(const QUrl &fileUrl)
{
#ifdef Q_OS_ANDROID
    QString filePath = d->generateFilePath(QStandardPaths::PicturesLocation, d->generateFileName(), "ArtBreak");

    return QFile::copy(fileUrl.toLocalFile(), filePath) ? QUrl::fromLocalFile(filePath) : QUrl();
#else
    Q_UNUSED(fileUrl)
    return QUrl();
#endif
}

void StorageManager::removePicture(const QUrl &fileUrl)
{
    QString filePath = fileUrl.toLocalFile();
    QString fileName = QFileInfo(filePath).fileName();

    // remove original file if it exists
    QFile::remove(d->generateFilePath(PLATFORM_LOCATION, fileName, "ArtBreakOriginal"));

    if (!QFile::remove(filePath)) {
        emit error(tr("Unable to remove %1").arg(filePath));
        return;
    }

    d->storageModel->removeItem(filePath);
}

void StorageManager::removeTempPictures()
{
    QDir dir = d->getPlatformPicturesLocation();

    if (!dir.cd("ArtBreakTemp"))
        return;

    QStringList fileNames = dir.entryList(QStringList() << "*.jpg", QDir::Files);

    //remove temporary files
    foreach (const QString &fileName, fileNames) {
        QFile::remove(dir.absoluteFilePath(fileName));
    }
}

void StorageManager::unlockPictures()
{
    QDir dir = d->getPlatformPicturesLocation();

    //check if there are original pictures without watermark
    if (!dir.cd("ArtBreakOriginal"))
        return;

    QStringList originalFileNames = dir.entryList(QStringList() << "*.jpg", QDir::Files);
    if (originalFileNames.isEmpty())
        return;

    // replace saved pictures at first
    if (dir.cdUp() && dir.cd("ArtBreak"))
        d->replacePircuresWithOriginal(dir);

    // replace temp pictures then
    if (dir.cdUp() && dir.cd("ArtBreakTemp"))
        d->replacePircuresWithOriginal(dir);

    // remove left useless original files if there are any
    foreach (const QString &fileName, originalFileNames) {
        QFile::remove(d->generateFilePath(PLATFORM_LOCATION, fileName, "ArtBreakOriginal"));
    }

    emit unlockCompleted();
}

QVariant StorageManager::storageModel() const
{
    return QVariant::fromValue(d->storageModel);
}

bool StorageManager::drawWatermark() const
{
    return d->isDrawWatermark;
}

void StorageManager::setDrawWatermark(bool drawWatermark)
{
    if (d->isDrawWatermark == drawWatermark)
        return;

    d->isDrawWatermark = drawWatermark;
    emit drawWatermarkChanged(drawWatermark);
}

StorageManager::StorageManager(QObject *parent)
    : QObject(parent)
    , d(new StorageManagerPrivate(this))
{

}
