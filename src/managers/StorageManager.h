#ifndef STORAGEMANAGER_H
#define STORAGEMANAGER_H

#include <QObject>
#include <QPixmap>
#include <QUrl>
#include <QVariant>

class StorageManagerPrivate;

class StorageManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariant storageModel READ storageModel CONSTANT)
    Q_PROPERTY(bool drawWatermark READ drawWatermark WRITE setDrawWatermark NOTIFY drawWatermarkChanged)
public:
    ~StorageManager();

    static StorageManager* instance();

    Q_INVOKABLE QUrl savePixmapTemporary(const QPixmap &pixmap);
    Q_INVOKABLE QUrl savePixmap(const QPixmap &pixmap);
    // On Android saves the pixmap to the external Pictures folder
    // On iOS does nothing and returns an invalid URL
    Q_INVOKABLE QUrl copyPictureToExternalStorage(const QUrl &fileUrl);

    Q_INVOKABLE void removePicture(const QUrl &fileUrl);

    Q_INVOKABLE void removeTempPictures();

    Q_INVOKABLE void unlockPictures(); //removes watermark from existing pictures

    QVariant storageModel() const;

    bool drawWatermark() const;

public slots:
    void setDrawWatermark(bool drawWatermark);

signals:
    void error(const QString &errorText);
    void drawWatermarkChanged(bool drawWatermark);

    void unlockCompleted();

private:
    explicit StorageManager(QObject *parent = 0);

    static StorageManager* m_instance;

    StorageManagerPrivate *d;
};

#endif // STORAGEMANAGER_H
