#include "PurchaseManager_p.h"

#include <jni.h>
#include <QtAndroid>
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>

#define PURCHASE_MANAGER PurchaseManager::instance()

namespace {
PurchaseManagerPrivate *PURCHASE_MANAGER_PRIVATE = nullptr;
}

class NativeHandler {
public:
    static void updateItemPrice(JNIEnv *env, jobject thiz, jstring itemId, jstring price)
    {
        Q_UNUSED(env)
        Q_UNUSED(thiz)

        QString productName = QAndroidJniObject(itemId).toString();
        QString priceValue = QAndroidJniObject(price).toString();
        // WORKAROUND: there is a weird issue with russian ruble sign on QML side
        // therefore we replace it with a simple string in russian version
        priceValue.replace("₽", "РУБ");
        PURCHASE_MANAGER_PRIVATE->updateItemPrice(productName, priceValue);
    }

    static void updateHasPurchase(JNIEnv *env, jobject thiz, jboolean hasPurchase)
    {
        Q_UNUSED(env)
        Q_UNUSED(thiz)

        PURCHASE_MANAGER_PRIVATE->updateHasPurchase(hasPurchase);
    }

    static void purchaseCompleted(JNIEnv *env, jobject thiz)
    {
        Q_UNUSED(env)
        Q_UNUSED(thiz)

        PURCHASE_MANAGER_PRIVATE->onPurchaseCompleted();
    }

    static void onDataUpdateError(JNIEnv *env, jobject thiz)
    {
        Q_UNUSED(env)
        Q_UNUSED(thiz)

        PURCHASE_MANAGER_PRIVATE->onDataUpdateError();
    }

    static void onBillingStatusChanged(JNIEnv *env, jobject thiz, jboolean initialized)
    {
        Q_UNUSED(env)
        Q_UNUSED(thiz)

        PURCHASE_MANAGER_PRIVATE->onBillingStatusChanged(initialized);
    }
};

JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* /*reserved*/)
{
    Q_UNUSED(vm)

    JNINativeMethod methods[] {
        {"updateItemPrice", "(Ljava/lang/String;Ljava/lang/String;)V", reinterpret_cast<void *>(NativeHandler::updateItemPrice)},
        {"updateHasPurchase", "(Z)V", reinterpret_cast<void *>(NativeHandler::updateHasPurchase)},
        {"purchaseCompleted", "()V", reinterpret_cast<void *>(NativeHandler::purchaseCompleted)},
        {"onDataUpdateError", "()V", reinterpret_cast<void *>(NativeHandler::onDataUpdateError)},
        {"onBillingStatusChanged", "(Z)V", reinterpret_cast<void *>(NativeHandler::onBillingStatusChanged)}
    };

    QAndroidJniObject javaClass("com/tarasovmobile/artbreak/MainActivity");
    QAndroidJniEnvironment env;
    jclass objectClass = env->GetObjectClass(javaClass.object<jobject>());
    env->RegisterNatives(objectClass,
                         methods,
                         sizeof(methods) / sizeof(methods[0]));
    env->DeleteLocalRef(objectClass);

    return JNI_VERSION_1_6;
}

PurchaseManagerPrivate::PurchaseManagerPrivate()
{
    PURCHASE_MANAGER_PRIVATE = this;
}

void PurchaseManagerPrivate::initInAppBilling()
{
    QtAndroid::androidActivity().callMethod<void>("initInAppBilling");
}

void PurchaseManagerPrivate::purchaseMonthSubscription()
{
    QtAndroid::androidActivity().callMethod<void>("purchaseSubscription", "(Ljava/lang/String;)V",
                                                      QAndroidJniObject::fromString(MONTH_SUBSCRIPTION_SKU).object<jstring>());
}

void PurchaseManagerPrivate::purchaseYearSubscription()
{
    QtAndroid::androidActivity().callMethod<void>("purchaseSubscription", "(Ljava/lang/String;)V",
                                                      QAndroidJniObject::fromString(YEAR_SUBSCRIPTION_SKU).object<jstring>());
}

void PurchaseManagerPrivate::purchaseAppForever()
{
    QtAndroid::androidActivity().callMethod<void>("purchaseItem", "(Ljava/lang/String;)V",
                                                  QAndroidJniObject::fromString(FOREVER_PURCHASE_SKU).object<jstring>());
}

void PurchaseManagerPrivate::restoreProducts()
{

}

void PurchaseManagerPrivate::requestUpdateData()
{
    QtAndroid::runOnAndroidThread([] () {
        QtAndroid::androidActivity().callMethod<void>("requestUpdateData");
    });
}

void PurchaseManagerPrivate::onBillingStatusChanged(bool initialized)
{
    PURCHASE_MANAGER->setBillingInitialized(initialized);
}
