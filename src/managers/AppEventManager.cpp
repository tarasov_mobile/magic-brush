#include "AppEventManager.h"

#include <QGuiApplication>

#ifdef Q_OS_ANDROID
#include "../helpers/AndroidHelper.h"
#endif

#ifdef Q_OS_IOS
#include "../helpers/AiosHelper.h"
#endif

AppEventManager::AppEventManager(QObject *parent)
    : QObject(parent)
{
    QObject::connect(qApp, &QGuiApplication::applicationStateChanged, [this] (Qt::ApplicationState state) {
        if (state == Qt::ApplicationActive)
            emit appDidBecomeActive();
    });

#ifdef Q_OS_ANDROID
    QObject::connect(AndroidHelper::instance(), &AndroidHelper::pictureAddedToGallery, [this] () {
        emit pictureAddedToGallery();
    });
#endif

#ifdef Q_OS_IOS
    QObject::connect(AiosHelper::instance(), &AiosHelper::shareCompleted, [this] () {
        emit appDidBecomeActive();
    });
    QObject::connect(AiosHelper::instance(), &AiosHelper::pictureAddedToGallery, [this] () {
        emit pictureAddedToGallery();
    });
#endif



}

AppEventManager::~AppEventManager()
{

}
