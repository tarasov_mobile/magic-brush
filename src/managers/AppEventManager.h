#ifndef APPEVENTMANAGER_H
#define APPEVENTMANAGER_H

#include <QObject>

class AppEventManager : public QObject
{
    Q_OBJECT
public:
    explicit AppEventManager(QObject *parent = 0);
    ~AppEventManager();

signals:
    void appDidBecomeActive();
    void pictureAddedToGallery();
};

#endif // APPEVENTMANAGER_H
