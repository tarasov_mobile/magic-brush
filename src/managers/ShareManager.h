#ifndef SHAREMANAGER_H
#define SHAREMANAGER_H

#include <QObject>
#include <QPixmap>
#include <QUrl>

class ShareManager : public QObject
{
    Q_OBJECT
public:
    explicit ShareManager(QObject *parent = 0);

    Q_INVOKABLE void shareImage(const QUrl &filePath, const QString &appName = QString());
};

#endif // SHAREMANAGER_H
