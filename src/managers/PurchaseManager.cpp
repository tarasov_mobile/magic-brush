#include "PurchaseManager.h"
#include "PurchaseManager_p.h"

PurchaseManager* PurchaseManager::m_instance = nullptr;

PurchaseManager::PurchaseManager(QObject *parent)
    : QObject(parent)
    , d(new PurchaseManagerPrivate)
    , m_hasPurchase(false)
    , m_billingInitialized(false)
    , m_state(PurchaseManager::Inactive)
{

}

PurchaseManager::~PurchaseManager()
{
    delete d;
}

PurchaseManager *PurchaseManager::instance()
{
    if (!m_instance)
        m_instance = new PurchaseManager;

    return m_instance;
}

void PurchaseManager::initInAppBilling()
{
    d->initInAppBilling();
}

QString PurchaseManager::monthPrice() const
{
    return m_monthPrice;
}

QString PurchaseManager::yearPrice() const
{
    return m_yearPrice;
}

QString PurchaseManager::foreverPrice() const
{
    return m_foreverPrice;
}

bool PurchaseManager::hasPurchase() const
{
    return m_hasPurchase;
}

PurchaseManager::State PurchaseManager::state() const
{
    return m_state;
}

bool PurchaseManager::billingInitialized() const
{
    return m_billingInitialized;
}

void PurchaseManager::purchaseMonthSubscription()
{
    if (!billingInitialized() || state() == PurchaseManager::Active)
        return;

    d->purchaseMonthSubscription();
}

void PurchaseManager::purchaseYearSubscription()
{
    if (!billingInitialized() || state() == PurchaseManager::Active)
        return;

    d->purchaseYearSubscription();
}

void PurchaseManager::purchaseAppForever()
{
    if (!billingInitialized() || state() == PurchaseManager::Active)
        return;

    d->purchaseAppForever();
}

void PurchaseManager::restoreProducts()
{
    if (!billingInitialized() || state() == PurchaseManager::Active)
        return;

    d->restoreProducts();
}

void PurchaseManager::requestUpdateData()
{
    if (!billingInitialized() || state() == PurchaseManager::Active)
        return;

    d->requestUpdateData();
}

void PurchaseManager::setMonthPrice(const QString &price)
{
    if (m_monthPrice != price) {
        m_monthPrice = price;

        emit monthPriceChanged(price);
    }
}

void PurchaseManager::setYearPrice(const QString &price)
{
    if (m_yearPrice != price) {
        m_yearPrice = price;

        emit yearPriceChanged(price);
    }
}

void PurchaseManager::setForeversPrice(const QString &price)
{
    if (m_foreverPrice != price) {
        m_foreverPrice = price;

        emit foreverPriceChanged(price);
    }
}

void PurchaseManager::setHasPurchase(bool hasPurchase)
{
    if (m_hasPurchase != hasPurchase) {
        m_hasPurchase = hasPurchase;

        emit hasPurchaseChanged(hasPurchase);
    }
}

void PurchaseManager::setBillingInitialized(bool initialized)
{
    if (m_billingInitialized != initialized) {
        m_billingInitialized = initialized;

        emit billingInitializedChanged(initialized);
    }
}

void PurchaseManager::setState(PurchaseManager::State state)
{
    if (m_state != state) {
        m_state = state;

        emit stateChanged(state);
    }
}
