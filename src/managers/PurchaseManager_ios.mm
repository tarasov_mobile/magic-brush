#include "PurchaseManager_p.h"

#import "UIKit/UIKit.h"
#import <StoreKit/StoreKit.h>
#import "../../ios/src/IAPHelper/IAPHelper.h"
#import "../../ios/src/IAPHelper/IAPShare.h"
#import "../../ios/src/AppDelegate.h"

#include <QSettings>
#include <QDateTime>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QTimer>
#include <QGuiApplication>

#include <AppsFlyerTracker.h>

#define APP_DELEGATE ((QIOSApplicationDelegate*)[UIApplication sharedApplication].delegate)

namespace
{
QSettings *PURCHASE_SETTINGS = nullptr;
PurchaseManagerPrivate *PURCHASE_MANAGER_GLOBAL = nullptr;
QHash<QString, SKProduct *> PRODUCTS_HASH = QHash<QString, SKProduct *>();
const QString SHARED_SECRET = "8291347f81f542c98961a88dce6a9aa5";
bool RESTORE_IN_PROGRESS = false;

void (^receiptHandler)(NSString *response, NSError *error) = ^(NSString *response, NSError *error) {
    Q_UNUSED(error)

    QJsonDocument doc = QJsonDocument::fromJson(QString::fromNSString(response).toUtf8());
    if (!doc.isEmpty()) {
        QJsonObject obj = doc.object();

        if(obj.value("status").toInt() == 0) {
            QJsonArray receipts = obj.value("latest_receipt_info").toArray();
            if (receipts.count()) {
                QString currentProductId = PURCHASE_SETTINGS->value("purchased_product").toString();
                QJsonObject lastObject = receipts.last().toObject();
                QString productId = lastObject.value("product_id").toString();

                if (currentProductId != productId) {
                    if (productId == FOREVER_PURCHASE_SKU)
                        PURCHASE_MANAGER_GLOBAL->updateHasPurchase(true);

                    PURCHASE_SETTINGS->setValue("purchased_product", productId);
                    PURCHASE_SETTINGS->sync();
                }

                if (lastObject.contains("expires_date_ms")) {
                    QString timestamp = lastObject.value("expires_date_ms").toString();
                    PURCHASE_SETTINGS->setValue("expires_date", timestamp);
                    PURCHASE_SETTINGS->sync();
                }
            }
        } else if (obj.value("status").toInt() == 21006) {// expired subscription
            PURCHASE_SETTINGS->remove("expires_date");
            PURCHASE_SETTINGS->sync();
        }
    }

    // if we have expiration date time then compare it with the current date time
    if (!PURCHASE_SETTINGS->value("expires_date").isNull()) {
        QDateTime expiresDate = QDateTime::fromMSecsSinceEpoch(PURCHASE_SETTINGS->value("expires_date").toLongLong());
        bool subscriptionActive = QDateTime::currentDateTime() < expiresDate;
        PURCHASE_MANAGER_GLOBAL->updateHasPurchase(subscriptionActive);

        if (!subscriptionActive) {
            // reset subscription data
            PURCHASE_SETTINGS->setValue("purchased_product", QVariant());
            PURCHASE_SETTINGS->setValue("expires_date", QVariant());
            PURCHASE_SETTINGS->sync();
        }
    }

    if (RESTORE_IN_PROGRESS) {
        if (!PURCHASE_SETTINGS->value("purchased_product").toString().isEmpty()) {
            PURCHASE_MANAGER_GLOBAL->onPurchaseCompleted();
        } else {
            [APP_DELEGATE showNotificationMessage:QObject::tr("You have no purchases").toNSString()];
        }
        RESTORE_IN_PROGRESS = false;
    }

    PURCHASE_MANAGER_GLOBAL->updateState(PurchaseManager::Inactive);
};

void (^purchaseHandler)(SKPaymentTransaction* trans) = ^(SKPaymentTransaction* trans) {

    if(trans.error || trans.transactionState == SKPaymentTransactionStateFailed)
    {
        [APP_DELEGATE showAlertWithTitle:QObject::tr("Error").toNSString() withMessage: [trans.error localizedDescription]];
        PURCHASE_MANAGER_GLOBAL->updateState(PurchaseManager::Inactive);
    }
    else if(trans.transactionState == SKPaymentTransactionStatePurchased) {
        QString productId = QString::fromNSString(trans.payment.productIdentifier);
        QString productCategory = (productId == FOREVER_PURCHASE_SKU ? "forever" : "subscription");

        [[AppsFlyerTracker sharedTracker] trackEvent:AFEventPurchase withValues: @{
            AFEventParamContentId: productId.toNSString(),
            AFEventParamContentType : productCategory.toNSString(),
            AFEventParamRevenue: PRODUCTS_HASH.value(productId).price,
            AFEventParamCurrency:[PRODUCTS_HASH.value(productId).priceLocale objectForKey:NSLocaleCurrencyCode]}];

        PURCHASE_MANAGER_GLOBAL->onPurchaseCompleted();

        PURCHASE_SETTINGS->setValue("purchased_product", productId);
        PURCHASE_SETTINGS->sync();
        // check receipt only when user purchased a subscription
        if (productId != FOREVER_PURCHASE_SKU) {
            [[IAPShare sharedHelper].iap checkReceipt:[NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]]
                                                       AndSharedSecret:SHARED_SECRET.toNSString() onCompletion: receiptHandler];
        } else {
            PURCHASE_MANAGER_GLOBAL->updateState(PurchaseManager::Inactive);
        }
    }
};
}

PurchaseManagerPrivate::PurchaseManagerPrivate()
{
    PURCHASE_SETTINGS = new QSettings;
    PURCHASE_MANAGER_GLOBAL = this;

    if(![IAPShare sharedHelper].iap) {

        NSSet* dataSet = [[NSSet alloc] initWithObjects: MONTH_SUBSCRIPTION_SKU.toNSString(), YEAR_SUBSCRIPTION_SKU.toNSString(), FOREVER_PURCHASE_SKU.toNSString(), nil];

        [IAPShare sharedHelper].iap = [[IAPHelper alloc] initWithProductIdentifiers:dataSet];
        [IAPShare sharedHelper].iap.production = YES;
    }

    // check purchases after window appeared
    QObject::connect((QGuiApplication *)QGuiApplication::instance(), &QGuiApplication::applicationStateChanged, [] (Qt::ApplicationState state) {
        if (state == Qt::ApplicationActive) {
            QString productId = PURCHASE_SETTINGS->value("purchased_product").toString();
            // check receipt on start only when user has a subscription
            if (!productId.isEmpty() && productId != FOREVER_PURCHASE_SKU) {
                [[IAPShare sharedHelper].iap checkReceipt:[NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]]
                                                           AndSharedSecret:SHARED_SECRET.toNSString() onCompletion: receiptHandler];
            } else if (!productId.isEmpty()) {
                QTimer::singleShot(0, [] () { PURCHASE_MANAGER_GLOBAL->updateHasPurchase(true); });
            }
        }
    });
}

void PurchaseManagerPrivate::initInAppBilling()
{

    onBillingStatusChanged([SKPaymentQueue canMakePayments]);
}

void PurchaseManagerPrivate::purchaseMonthSubscription()
{
    if (!PRODUCTS_HASH.contains(MONTH_SUBSCRIPTION_SKU))
        return;

    updateState(PurchaseManager::Active);
    [[IAPShare sharedHelper].iap buyProduct:PRODUCTS_HASH.value(MONTH_SUBSCRIPTION_SKU)
                                      onCompletion:purchaseHandler];
}

void PurchaseManagerPrivate::purchaseYearSubscription()
{
    if (!PRODUCTS_HASH.contains(YEAR_SUBSCRIPTION_SKU))
        return;

    updateState(PurchaseManager::Active);
    [[IAPShare sharedHelper].iap buyProduct:PRODUCTS_HASH.value(YEAR_SUBSCRIPTION_SKU)
                                      onCompletion:purchaseHandler];
}

void PurchaseManagerPrivate::purchaseAppForever()
{
    if (!PRODUCTS_HASH.contains(FOREVER_PURCHASE_SKU))
        return;

    updateState(PurchaseManager::Active);
    [[IAPShare sharedHelper].iap buyProduct:PRODUCTS_HASH.value(FOREVER_PURCHASE_SKU)
                                      onCompletion:purchaseHandler];
}

void PurchaseManagerPrivate::restoreProducts()
{
    updateState(PurchaseManager::Active);
    RESTORE_IN_PROGRESS = true;
    [[IAPShare sharedHelper].iap restoreProductsWithCompletion:^(SKPaymentQueue *payment, NSError *error) {
            if (error) {
                [APP_DELEGATE showAlertWithTitle:QObject::tr("Error").toNSString() withMessage: error.localizedDescription];
                PURCHASE_MANAGER_GLOBAL->updateState(PurchaseManager::Inactive);
                RESTORE_IN_PROGRESS = false;
                return;
            }

            int numberOfTransactions = payment.transactions.count;
            if (numberOfTransactions) {
                // if we have 'abfull' product purchased then handle it without checking receipt
                for (SKPaymentTransaction *transaction in payment.transactions) {
                    QString productId = QString::fromNSString(transaction.payment.productIdentifier);
                    if (productId == FOREVER_PURCHASE_SKU) {
                        PURCHASE_MANAGER_GLOBAL->updateHasPurchase(true);

                        PURCHASE_SETTINGS->setValue("purchased_product", productId);
                        PURCHASE_SETTINGS->sync();

                        PURCHASE_MANAGER_GLOBAL->onPurchaseCompleted();
                        return;
                    }
                }

                [[IAPShare sharedHelper].iap checkReceipt:[NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]]
                                                           AndSharedSecret:SHARED_SECRET.toNSString() onCompletion: receiptHandler];
            } else {
                [APP_DELEGATE showNotificationMessage:QObject::tr("You have no purchases").toNSString()];
                RESTORE_IN_PROGRESS = false;
                PURCHASE_MANAGER_GLOBAL->updateState(PurchaseManager::Inactive);
            }
    }];
}

void PurchaseManagerPrivate::requestUpdateData()
{
    updateState(PurchaseManager::Active);
    [[IAPShare sharedHelper].iap requestProductsWithCompletion:^(SKProductsRequest *request, SKProductsResponse *response, NSError *error)
    {
        Q_UNUSED(request)
        PURCHASE_MANAGER_GLOBAL->updateState(PurchaseManager::Inactive);

        if (error) {
            PURCHASE_MANAGER_GLOBAL->onDataUpdateError();
            return;
        }

        for (SKProduct *product in response.products) {
            PRODUCTS_HASH.insert(QString::fromNSString(product.productIdentifier), product);
            PURCHASE_MANAGER_GLOBAL->updateItemPrice(QString::fromNSString(product.productIdentifier), QString::fromNSString([[IAPShare sharedHelper].iap getLocalePrice: product]));
        }
    }];
}

void PurchaseManagerPrivate::onBillingStatusChanged(bool initialized)
{
    PURCHASE_MANAGER->setBillingInitialized(initialized);
}
