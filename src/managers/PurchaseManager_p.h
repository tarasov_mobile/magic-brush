#ifndef PURCHASEMANAGER_P_H
#define PURCHASEMANAGER_P_H

#include <QString>

#include "PurchaseManager.h"
#include "../common/Constants.h"

#define PURCHASE_MANAGER PurchaseManager::instance()

class PurchaseManager;

class PurchaseManagerPrivate
{
public:
    PurchaseManagerPrivate();

    void initInAppBilling();

    void purchaseMonthSubscription();
    void purchaseYearSubscription();
    void purchaseAppForever();

    void restoreProducts();

    void requestUpdateData();

    void updateItemPrice(const QString &itemId, const QString &price)
    {
        if (itemId == MONTH_SUBSCRIPTION_SKU) {
            PURCHASE_MANAGER->setMonthPrice(price);
        } else if (itemId == YEAR_SUBSCRIPTION_SKU) {
            PURCHASE_MANAGER->setYearPrice(price);
        } else if (itemId == FOREVER_PURCHASE_SKU) {
            PURCHASE_MANAGER->setForeversPrice(price);
        }
    }

    void updateHasPurchase(bool hasPurchase)
    {
        PURCHASE_MANAGER->setHasPurchase(hasPurchase);
    }

    void onPurchaseCompleted()
    {
        PURCHASE_MANAGER->setHasPurchase(true);
        emit PURCHASE_MANAGER->purchaseCompleted();
    }

    void onDataUpdateError()
    {
        PURCHASE_MANAGER->dataUpdateError();
    }

    void updateState(PurchaseManager::State state)
    {
        PURCHASE_MANAGER->setState(state);
    }

    void onBillingStatusChanged(bool initialized);
};

#endif // PURCHASEMANAGER_P_H
