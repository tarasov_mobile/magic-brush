#include "AnalyticsManager.h"

#ifdef Q_OS_ANDROID
#include "../helpers/AndroidHelper.h"
#elif defined(Q_OS_IOS)
#include "../helpers/AiosHelper.h"
#endif

AnalyticsManager* AnalyticsManager::m_instance = nullptr;

AnalyticsManager::AnalyticsManager(QObject *parent)
    : QObject(parent)
{

}

AnalyticsManager::~AnalyticsManager()
{

}

AnalyticsManager *AnalyticsManager::instance()
{
    if (!m_instance)
        m_instance = new AnalyticsManager;

    return m_instance;
}

void AnalyticsManager::sendEvent(const QString &categoryName, const QString &eventName)
{
#ifdef Q_OS_ANDROID
    AndroidHelper::instance()->sendAnalyticsEvent(categoryName, eventName);
#elif defined(Q_OS_IOS)
    AiosHelper::instance()->sendAnalyticsEvent(categoryName, eventName);
#endif
}
