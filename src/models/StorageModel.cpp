#include "StorageModel.h"

#include <QDir>
#include <QStringList>
#include <QUrl>

class StorageModelPrivate
{
public:
    StorageModelPrivate(const QString &dirPath)
    {
        dirToTrack = QDir(dirPath);
        fileNameList = dirToTrack.entryList(QStringList() << "*.jpg", QDir::Files, QDir::Time);
    }

    ~StorageModelPrivate()
    {

    }

    QDir dirToTrack;
    QStringList fileNameList;
};

StorageModel::StorageModel(const QString &dirPath, QObject *parent)
    : QAbstractListModel(parent)
    , d(new StorageModelPrivate(dirPath))
{

}

StorageModel::~StorageModel()
{
    delete d;
}

QVariant StorageModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() > (d->fileNameList.count() - 1))
        return QVariant();

    if (role == Qt::DisplayRole) {
        QString fileName = d->fileNameList.at(index.row());
        QString filePath = d->dirToTrack.absoluteFilePath(fileName);
        return QUrl::fromLocalFile(filePath);
    }

    return QVariant();
}

int StorageModel::rowCount(const QModelIndex &) const
{
    return d->fileNameList.count();
}

void StorageModel::prependItem(const QString &filePath)
{
    QFileInfo fileInfo(filePath);

    beginInsertRows(QModelIndex(), 0, 0);
    d->fileNameList.prepend(fileInfo.fileName());
    endInsertRows();
}

void StorageModel::removeItem(const QString &filePath)
{
    QFileInfo fileInfo(filePath);

    int index = d->fileNameList.indexOf(fileInfo.fileName());
    if (index != -1) {
        beginRemoveRows(QModelIndex(), index, index);
        d->fileNameList.removeAt(index);
        endRemoveRows();
    }
}
