#ifndef STORAGEMODEL_H
#define STORAGEMODEL_H

#include <QAbstractListModel>

class StorageModelPrivate;

class StorageModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum ListRoles {
        FilePathRole = Qt::UserRole + 1
    };

    explicit StorageModel(const QString &dirPath, QObject *parent = 0);
    ~StorageModel();

    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    void prependItem(const QString &filePath);
    void removeItem(const QString &filePath);

private:
    StorageModelPrivate *d;
};

#endif // STORAGEMODEL_H
