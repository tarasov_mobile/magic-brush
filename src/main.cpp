#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QScreen>
#include <QtMath>
#include <QQuickStyle>
#include <QTimer>
#include "drawing/surfaces/Canvas.h"
#include "settings/GlobalBrushSettings.h"
#include "settings/AppSettings.h"
#include "managers/ShareManager.h"
#include "managers/AnalyticsManager.h"
#include "managers/StorageManager.h"
#include "managers/PurchaseManager.h"
#include "managers/AppEventManager.h"
#include "helpers/ColorHelper.h"
#include "helpers/ImageHelper.h"
#include "helpers/ScreenHelper.h"
#include "helpers/UrlHelper.h"
#include <QTranslator>
#include <QDateTime>
#include <QQuickImageProvider>
#include <QSvgRenderer>
#include <QPainter>
#include <QQmlFileSelector>

#ifdef Q_OS_ANDROID
#include "helpers/AndroidHelper.h"
#endif

#ifdef Q_OS_IOS
#include "helpers/AiosHelper.h"
#endif

//this provider is needed because there is a bug with images in HTML code
//https://bugreports.qt.io/browse/QTBUG-59252 (QML Text does not support devicePixelRation for images)
//also look at the /docs directory where you'll find
//a text file with description on how to patch QtQuick5 module
//in order to get this provider working properly
class EmojiImageProvider : public QQuickImageProvider
{
public:
    EmojiImageProvider()
        : QQuickImageProvider(QQuickImageProvider::Pixmap)
    {
    }

    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize)
    {
        Q_UNUSED(size)
        QSvgRenderer renderer(":/icons/emojis/" + id);

        QPixmap p(requestedSize);
        p.fill(Qt::transparent);

        QPainter painter(&p);
        renderer.render(&painter);

        return p;
    }
};

void loadTranslations()
{
    QString locale = QLocale::system().name();
    QTranslator *qtTranslator = new QTranslator(qApp);
    qtTranslator->load(QString(":/translations/qtbase_%1.qm").arg(locale));
    qApp->installTranslator(qtTranslator);

    QTranslator *appTranslator = new QTranslator(qApp);
    appTranslator->load(QString(":/translations/magic_brush_%1.qm").arg(locale));
    qApp->installTranslator(appTranslator);
}

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication::setOrganizationName("TarasovMobile");
    QGuiApplication::setApplicationName("ArtBreak");

#if defined(Q_OS_ANDROID)
    QGuiApplication::setFont(QFont("Roboto"));
    QGuiApplication::setApplicationVersion(AndroidHelper::instance()->getCurrentAppVersion());
#elif defined(Q_OS_IOS)
    QGuiApplication::setApplicationVersion(AiosHelper::instance()->getCurrentAppVersion());
#endif

    QGuiApplication app(argc, argv);

    loadTranslations();

    qmlRegisterType<Canvas>("com.tarasovmobile.drawing", 1, 0, "Canvas");
    qmlRegisterUncreatableType<PurchaseManager>("com.tarasovmobile.managers", 1, 0, "PurchaseManagerEnums", "");
    qmlRegisterSingletonType(QUrl("qrc:/qml/Utils.qml"), "com.tarasovmobile.utils", 1, 0, "Utils");
    qmlRegisterSingletonType(QUrl("qrc:/qml/StyleManager.qml"), "com.tarasovmobile.managers", 1, 0, "StyleManager");
    qmlRegisterSingletonType(QUrl("qrc:/qml/DialogManager.qml"), "com.tarasovmobile.managers", 1, 0, "DialogManager");

    QQmlApplicationEngine engine;

#ifdef Q_OS_ANDROID
    QQuickStyle::setStyle("Material");
    QQmlFileSelector *selector = QQmlFileSelector::get(&engine);
    selector->setExtraSelectors(QStringList() << AndroidHelper::instance()->getCurrentScreenDensity());
#endif

    QGuiApplication::primaryScreen()->setOrientationUpdateMask(Qt::LandscapeOrientation|Qt::PortraitOrientation
                                                               |Qt::InvertedLandscapeOrientation|Qt::InvertedPortraitOrientation);

    engine.addImageProvider(QLatin1String("emoji"), new EmojiImageProvider);
    engine.rootContext()->setContextProperty("DefaultAppColor", QColor("#7E57C2"));
    engine.rootContext()->setContextProperty("GlobalBrushSettings", GlobalBrushSettings::instance());
    engine.rootContext()->setContextProperty("AppSettings", AppSettings::instance());
    engine.rootContext()->setContextProperty("ShareManager", new ShareManager(&app));
    engine.rootContext()->setContextProperty("AnalyticsManager", AnalyticsManager::instance());
    engine.rootContext()->setContextProperty("StorageManager", StorageManager::instance());
    engine.rootContext()->setContextProperty("PurchaseManager", PurchaseManager::instance());
    engine.rootContext()->setContextProperty("AppEventManager", new AppEventManager(&app));
    engine.rootContext()->setContextProperty("ColorHelper", new ColorHelper(&app));
    engine.rootContext()->setContextProperty("ImageHelper", new ImageHelper(&app));
    engine.rootContext()->setContextProperty("ScreenHelper", new ScreenHelper(&app));
    engine.rootContext()->setContextProperty("UrlHelper", new UrlHelper(&app));
#ifdef Q_OS_ANDROID
    engine.rootContext()->setContextProperty("AndroidHelper", AndroidHelper::instance());
    engine.rootContext()->setContextProperty("DeviceIsTablet", AndroidHelper::instance()->isTablet());
    engine.rootContext()->setContextProperty("AiosHelper", QVariant());
#elif defined(Q_OS_IOS)
    engine.rootContext()->setContextProperty("AndroidHelper", QVariant());
    engine.rootContext()->setContextProperty("DeviceIsTablet", AiosHelper::instance()->isTablet());
    engine.rootContext()->setContextProperty("AiosHelper", AiosHelper::instance());
#endif

    // remove watermark from existing pictures when purchase completed
    // we use hasPurchaseChanged instead of purchaseCompleted for unexpected cases when
    // pictures weren't unlocked immediately after first purchase
    QObject::connect(PurchaseManager::instance(), &PurchaseManager::hasPurchaseChanged, [] (bool hasPurchase) {
        if (hasPurchase)
            StorageManager::instance()->unlockPictures();
    });

    PurchaseManager::instance()->initInAppBilling();

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
#ifdef Q_OS_ANDROID
    QTimer::singleShot(300, AndroidHelper::instance(), &AndroidHelper::hideSplashScreen);
#endif

    AppSettings::instance()->setCustomProperty("last_installed_app_version", app.applicationVersion());

    return app.exec();
}
