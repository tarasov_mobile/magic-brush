# Supported platforms #
* Android v4.1 (API level 16) and greater
* iOS 9 and greater

# Requirements #
* Qt 5.8 or greater with some patches (see below)

# Building #
Before building you need to copy patched Qt binaries from ***'qtbinaries'*** folder into appropriate places. There are patched binaries only for Qt 5.8 in the repo.
If you have different Qt version then take a look at the ***'docs'*** directory where you'll find instructions on how to patch Qt's source code.
After that you can just build the project in Qt Creator.

# Necessary patches in Qt #
There are several TXT files in the 'docs' directory and here is simple description of what they contain:

* **Android_Splash_Screen_Fix.txt** - the patch to get rid of black flicker on app start
* **Android_Inverted_Orientations_Fix.txt** - the patch to support detection of inverted orientations (*Qt::InvertedPortraitOrientation* and *Qt::InvertedLandscapeOrientation*)
* **QtQuick5_Emoji_Fix.txt** - the patch to fix blurry SVG images when using <img> tag in QQuickText on high-dpi devices
* **Drawing_Performance_QtQuick_Patch.txt** - the patch to improve drawing performance (especially on some Android devices)